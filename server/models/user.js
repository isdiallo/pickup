'use strict';

const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
const crypto = require('crypto');
const config = require('../config');
const validator = require('validator');

const Schema = mongoose.Schema;

var User = new Schema({
    username: {
      type: String,
      required: true
    },
    mail: {
      type: String,
      validate: {
        validator: function (v) {
          return validator.isEmail(v);
        },
        message: '{VALUE} is not a valid email address.'
      }
    },
    firstname: {
      type: String,
      required: true
    },
    lastname: {
      type: String,
      required: true
    },
    phone: {
      type: String,
      validate: {
        validator: function(v) {
          return config.phonePattern.test(v);
        },
        message: '{VALUE} is not a valid phone number. A valid phone number should contain 9 digits.'
      }
    },
    address: {
      type: String,
      required: true
    },
    city: {
      type: String,
      required: true
    },
    town: {
      type: String,
      required: true
    },
    // picture: {
    //   type: String,
    //   default: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALgAAABCCAIAAABrWIvvAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAn2SURBVHhe7ZtLbFTXGce7qJRFF110wSKLLrLoAqmVsmCRRRcsskiVqHIlKxAViahyRBSEGgk1jUoEdbGcEBBOHWOHwXGNjad+xDbmYXewGeLnMHaNPRgTg41tbIOBGogTQ3As9RfO0e3tHWZ87mM8c8fnr28x93XuefzP9/2/c8/8aF5DQwGaKBpK0ETRUIImioYSNFE0lKCJoqEETRQNJWiiaChBE0VDCZooGkrQRPkBj75blr80EiCbiXJr4eu+0elgeOjD2gs7j7RsLfzn5vcCv9zxyQvbP/7p7/7649/8JZFxwy/+cIibeeSPZaeLmrqbekZik7eXv1+RRa8/ZA9R7j78prV/7GBD55uHGjbtKklOBWf23KsfwLNtB+qgTnRsZl3xxsdEIV50XZ6EGTn5VT/f9pFlUNfA4OIreypwVzgbWafshc+IAjnODV7/8+dtL71b+pPf7rWMXBqNUEWQ6h+bkRXNOviDKExZJu7L75dnFDmeaRvfOkxVkUey6tmCzCXK4tJjJGReUWNawopLg9BImUvjc7Ix/kfGEYW5GGiNvra30pnzQG+KhAVJu6cyhOqsCA2Ehyew0ek7N24vWOza7D1xldu4maBGpkNc2/B6gaVkZ4YXDA9NyLb5GZlCFAYMj/3r3Z9ZOjq5/Sz3bwzq28XNjDEpD4XI4rwAaRRjTMn4BshnebUtQ/P63bukmSiD43PMe3JOS88mMiY6Oc6+6nai0s27D2QpawJcXTA8hKN6/o1CS61UDFe3o7j5/uKSLM5vSANRlr9fIXPZeaRFUXy8+E4xCQWDRKSQRaQbpOVUyYF42rCloL7rsizFV1g7ojAj0QFbC4MqS2GCHEQTJK18PiOBuEHT2JVTuQU1xDVZhE+QWqIwzAw2CnHTrhJLZ8UbggMaQSbf5ZZUeF9Vu62QhDfqHZ2Wz/sBHhPl0XfLyA7SFiILXoHAbOmgeKPLcB4EI7+viNN2lK86XeicY61R+XDGwzlR0JLkKXgL9B3ul4TFVswmWsMPgr0sLlsAXfAu6sFod+CsfDKz4cqjxCZvM96Wlic3ejB3f82pyNXs/qLGLMrJr7a0PZExYeRjGQy3oaf0dMTS7ET2wvaPDzZ0+k7EuQEJzsa8wzR8VSs+2SufyVS4JQqy30KIeNv8p0BTzxX5gIY/4ZYo+2vOW2hhNgLNzpKTJD59o9O+y2U0zHBFFCIxOa2FHEmMm19+vxxuxW5k/waOLINzoqBGX3q31EIFdeNZJK0sSyPj4Zwovz9QZxn7Z9pzr36AoHttb2VeUeO+6nYM4RZojYrfiRadiFPRsZl1pXwXlx6LD9rkkuKDdrwNjs+Je+QzawiHREkuTSAHIebD2gvw4JHTDe79YzPiewpJgdjnvPNIC9yqCA009YzQa/SXL3QP9WSAzw1ep+bkfTThzUMNuQUnFHd6JzKeMjZUMBz1nbGU7sh0QhRChqXShm186/CnLb0eegICXHhoAsbQoZZ3GYb04eqmXSX0Wk5+FR33dnGz8Fh4L4YHo87GvIzfmKKyqGPMeMOMqR8MD/EKJsaeyhBvpw7UhCrZEnCeGOwhx9xX1U6nebtSZZso12bvPXMG/GrHJ6nOgRngoqZuopizKbjeDJpuO1CHJ5Pd5w72iAJJX3yn2FIhhg0v4i1/k4N34cwhzdbCoMstRevB8G30lWMNIGCPKDhzSyVw+DhheTlNuL+4xLxJ6T96ssCef6Ow7HTE8Xy2QRSCjuVb15bCoEuepgg37z6AOqWnI8YeWAebjFJhjBbzG6+MiBE61KyoMOosRJVhhroScths4hE0PoWQPTBJVm0m9zjTvDaIsv1Qg/mVvviUZQEEMhIQvLHRy0J+YiINMSzeOW14vcB8A/eLB1/ZUyFGHWpSrBDRrf1jDDATDKe7ZqGZF/FGXk0D84oa4zd7MNupm7xbGapE4d3m90EaecEmnjx5Mj4+0dsXOXWmNVhbX1FZhVUeP9HY3BK+0PnV2DVukLdqeASxfWx34KxZz5GgyctqUCUKrzHeQQJmd34sLy9DAthQXFL2909Lk1jpZ4EvO7sfP87oHZD+Rf/YzI7iZiEhbHFFlShG8NuwpcDWMhdDHolEj5X/w0KI5Hb0WMX09E1ZhIbXQP6L3VXqMUiJKMR1wRJMfRP5yspK7PIIQ24hgaLhe65dH5dlacSB7n348Gth33z7rTxrByi23P0nRqfvyOOkUCIK0kywBMkmT62GhYX7tfVfWMa+pPRoQ2MzAoUwdOvWbaOdMzOzQ8OxtlC7hVWEIcqRJWqgNhYXh2MjrW0hhJ25o0Rf1Tc0XYwO0J/ybjXgBeSvpFAiCqmBIIri392ujF6l3kYb8A1IVzTsqkIVKcOzaFvj2S8am+W1dQycB72HwjO6JbmdOdtmly6rQokouQUnYElOfrU8TgyahBQ1aowL4ZB5IC+rAbqQARmF4Hvkhf8HtKM7uIqagV6Dl4YiF6PtHefbQueglwODzaFzHd5ad08ftVI3XMLS0iPZwqcg/ppnjqIxUekTWYQXUCLK5vcCEGXVHfOwBC6LiuJF2jvCzmKnAAMviurs6pGn7AARDY1m5+Ymp6ZMNArjt+FETbAO771qCrbGVl5xfH7+f4qB+ifxImVHy8XiQpKG0GpZlmsoESUnv4oUXB4kgJklx6uDidyALXSEL1BasLZeHqcAeK+nXvoOOunq1a9isRH4hD/DGcAnZBbDYA6jqTM6zTyvkHGW98IGFF7/wL9hf/zywcLCAvMBp2ghzeSUN38zUyJKXlHjqjl3x/kfBhVjdOl9edYdCC7k1UwdeZxWwKd79/4Dn4gFDAl86urugU8tp85AKfh09NjnxvDYNQtLCFjmqxQ+MHhJ0T0T6BkCgy54KU8WpZSIcrChM/n/Hy+PXKFOVM7buAhEAJIHfgDDCaXg08SNSXoDB9DbdxE+4W7h0/HqGkZODKFhZpaYHTPGzRTCSXFVHXgdY+2KOsizLqBElL7R6SRLsWSwiFb85M2ZWXnKO6DsjpQF5EEWQUgoocTNrqIt1C5Gl1nX2xdx45spFgpSFA5JnnIBJaIkB4EclszO3ZLHXqO17V/yV7ajLxIVLCHNMatax0C4ELgp0P1ylFui4BjhfkqX29fJ+iyqU7Ck+eQpT1SFgFAF6HR57BSuiELshPtDwzF5nBo4iNC+A8wQq9IIFM/bWxOsQ3rLA6dwRRRSuLNt9r5WazwT7R3hFLEEDD/N+eWBU7giCmLeQye5bnH/wQPCNxEnRb6ThCCdRIEi+uuuJ8CdEMFTvGPLrTR2RRT5S8MF6Man+b8HOU5K4Sr0aLgHaWOXo49ZawxNlDTjy85uX2wT1kRJMyanpuSvzIYmSprhl1UiTRQNJWiiaChBE0VDCZooGgqYn/8vs11kIVs+TjMAAAAASUVORK5CYII='
    // }
}, {
  timestamps: true,
  collection: 'users',
  discriminatorKey: '_type'
});

User.methods.getName = function () {
  return this.firstname + " " + this.lastname;
};

User.methods.comparePwds = function(candidatePwd, salt, hash, cb){
  crypto.pbkdf2(candidatePwd, salt, 25000, 512, function(err, key){
    if (err) throw err;
    cb(null, hash === key.toString('hex'));
  });
};

User.plugin(passportLocalMongoose);

module.exports = {
  schema: User,
  model: mongoose.model('User', User)
};
