'use strict';

const mongoose = require('mongoose');

var EndUserSchema = new mongoose.Schema({
    firstname: {
      type: String,
      required: true
    },
    lastname: {
      type: String,
      required: true
    },
    phone: {
      type: String,
      required: true
    },
    address: {
      type: String
    },
    city: {
      type: String
    },
    town: {
      type: String
    }
});

EndUserSchema.statics.findOneOrCreate = function findOneOrCreate (condition, document, callback) {
  var self = this;
  self.findOne(condition, function (err, result) {
    return result 
      ? callback (err, result)
      : self.create(document, function(err, result){
          return callback(err, result);
        });
  });
};

module.exports = mongoose.model('EndUser', EndUserSchema);
