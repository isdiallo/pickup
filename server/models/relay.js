'use strict';

const mongoose = require('mongoose');

var relaySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    address: {
      type: String,
      required: true
    },
    city: {
      type: String,
      required: true
    },
    town: {
      type: String,
      required: true
    },
    longitude: {
      type: Number
    },
    latitude: {
      type: Number
    },
    openingHours: {
      type: [String]
      // required: true
    },
    holiday: {
      type: Boolean,
      default: false
    }
  },
  {
    timestamps: true,
    versionKey: false
  }
);

relaySchema.statics.findOneOrCreate = function findOneOrCreate(
  document,
  callback
) {
  var self = this;
  self.findOne(document, function(err, result) {
    return result
      ? callback(err, result)
      : self.create(document, function(err, result) {
          return callback(err, result);
        });
  });
};

module.exports = mongoose.model('Relay', relaySchema);
