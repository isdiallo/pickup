'use strict';

const mongoose = require('mongoose'),
      extend = require('mongoose-schema-extend');

const User = require('./user');
const Schema = mongoose.Schema;

var Agent = User.schema.extend({});

module.exports = mongoose.model('Agent', Agent);
