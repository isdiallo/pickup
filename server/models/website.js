'use strict';
// Not used form now
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var websiteSchema = new Schema({
    autoAccept: {
      type: Boolean
    },
    sendNotif: {
      type: Boolean
    },
    webSiteName: {
      type: String
    },
    webSiteUrl: {
      type: String
    }
});

module.exports = mongoose.model('websiteSchema', Website);
