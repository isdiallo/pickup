"use strict";

const mongoose = require("mongoose");

var SingleMessageSchema = new mongoose.Schema(
  {
    from: {
      // not required as the app can send code
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    content: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);

var MessagesSchema = new mongoose.Schema(
  {
    to: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true
    },
    messages: {
      type: [SingleMessageSchema]
    }
  },
  {
    timestamps: true
  }
);

MessagesSchema.statics.findOneOrCreate = function findOneOrCreate(
  id,
  callback
) {
  var self = this;
  self.findOne({ to: id }, function(err, result) {
    return result
      ? callback(err, result)
      : self.create({ to: id, messages: [] }, function(err, result) {
          return callback(err, result);
        });
  });
};

module.exports = mongoose.model("Messages", MessagesSchema);
