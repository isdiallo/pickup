'use strict';

const mongoose = require('mongoose'),
        extend = require('mongoose-schema-extend');

const User = require('./user');
const Schema = mongoose.Schema;

var AdminSchema = User.schema.extend({});

module.exports = mongoose.model('Admin', AdminSchema);
