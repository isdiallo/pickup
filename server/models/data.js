// This is a collection of fake datas used fro manual testing purpose
user = {
    "firstname": "Test",
    "lastname": "LAstname",
    "username": "user5",
    "phone": "03652452",
    "mail": "test@user.com",
    "type": "AGENT4",
    "password": "password"
};

user = {
    "firstname": "Test",
    "lastname": "LAstname",
    "username": "user",
    "phone": "03652452",
    "mail": "test@user.com",
    "type": "MERCHANT",
    "password": "password",
    "websiteName": "test website",
    "websiteUrl": "test.com",
    "city": "Conakry",
    "town": "Ratoma",
    "address": "No Where"
}

admin = {
  "username": "admin",
  "password": "adminPwd"
}

assign = {
  "pickupId": "5a00e9201d9f0a6c14a1276b",
  "to": "5a00d62b244b9a4b78db5898"
}

relay = {
  "name": "Test Relay",
  "address": "Relay address",
  "city": "Conakry",
  "town": "Ratoma"
}
pickup = {
  "fromId": "5a00d5b9244b9a4b78db5894",
  "toId": "5a00d5e7244b9a4b78db5896",
  "endUser": {
    "firstname": "test first",
    "lastname": "test last",
    "phone": "012452145",
    "address": "No Where",
    "city": "Ckry",
    "town": "Ratoma"
  },
  "packages": [{
    "name": "Pack1",
    "height": 15,
    "lenght": 16,
    "width": 17,
    "weight": 18,
    "fragile": true
  }],
  "isFree": false,
  "cost": 25000
}

newPickup = {
  "packages": {
      "XL": {"fragile": true, "quantity": 2},
      "L": {"fragile": true, "quantity": 3},
      "M": {"fragile": false, "quantity": 1},
      "S": {"fragile": true, "quantity": 2}
  },
  "fromId": "5a00d5b9244b9a4b78db5894",
  "destination": "Cosa",
  "charge": 15000,
  "isFree": false,
  "useRelay": false,
  "toId": "5a00d5e7244b9a4b78db5896",
  "endUser": {
      "firstname": "end user first",
      "lastname": "end user last",
      "phone": "123456789",
      "address": "Address end user",
      "city": "City end user",
      "town": "Town end user"
  }
}



mutation{
  updateRelay(data: {
    name: "Test",
    address: "test 2 ade",
    city: "adr 3 city",
    town: "adr 2 town",
    longitude: 12.02145,
    latitude: 15.25623,
    openingHours: ["Test 1", "test 2"],
    holiday: false
  }) {
    name
    address
    city
    town
    longitude
    latitude
    holiday
  }
}

mutation{
  addPickup(data: {
    fromId: "5992db328c560719d0459943",
    toId: "5992db328c560719d0459943",
    state: "PENDING",
    endUser: {
      town: "Ratoma",
      city: "Conakry",
      address: "adress Conakry",
      phone: "0123456789",
      lastName: "end user lastName"
      firstName: "end user firstName"
    },
    packages: [],
    cost: 15.25623,
    isFree: false
  })
}
