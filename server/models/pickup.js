"use strict";

const mongoose = require("mongoose");
const _ = require("lodash");
const cc = require("coupon-code");

const config = require("../config");

var packageSchema = require("./package");

var stateSchema = {
  name: {
    type: String,
    enum: config.pickupStates,
    required: true
  },
  owner: {
    // Also the code sender
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  code: {
    type: String,
    required: function() {
      // Is it really necessary to put constaint on code?
      return _.indexOf(config.stateWithCode, this.name) > 0;
    }
  }
};

var assignationSchema = new mongoose.Schema(
  {
    to: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true
    },
    by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true
    }
  },
  {
    _id: false
  }
);

var actionSchema = new mongoose.Schema(
  {
    who: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true
    },
    name: {
      type: String,
      enum: config.actionsList,
      required: true
    },
    state: stateSchema
  },
  { timestamps: true }
);

var pickupSchema = new mongoose.Schema(
  {
    fromId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true
    },
    toId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    endUserId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "EndUser",
      required: true
    },
    assignation: assignationSchema,
    state: stateSchema,
    packages: [packageSchema],
    cost: {
      type: Number,
      required: function() {
        return !this.isFree;
      }
    },
    history: [actionSchema],
    isFree: {
      type: Boolean,
      default: false
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Pickup", pickupSchema);
