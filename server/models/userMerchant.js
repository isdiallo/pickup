'use strict';

const mongoose = require('mongoose'),
  extend = require('mongoose-schema-extend');

const User = require('./user');

var Merchant = User.schema.extend({
  autoAccept: {
    // Whether or not a merchant user automatically accept a pickup.
    type: Boolean,
    default: false
  },
  sendNotif: {
    // Whether or not a merchant user send notification to the final customer after each step from the pickup process.
    type: Boolean,
    default: false
  },
  useRelay: {
    // Whether or not a merchant user will use a relay during the pickup process
    type: Boolean,
    default: true
  },
  websiteName: {
    type: String
  },
  websiteUrl: {
    type: String,
    required: function() {
      return this.websiteName !== undefined && this.websiteName !== '';
    }
  }
});

module.exports = mongoose.model('Merchant', Merchant);
