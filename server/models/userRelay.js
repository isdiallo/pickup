'use strict';

const mongoose = require('mongoose'),
      extend = require('mongoose-schema-extend');

const User = require('./user');
const Schema = mongoose.Schema;

var UserRelay = User.schema.extend({
  relayId: {
    type: Schema.Types.ObjectId,
    ref: 'Relay'
  }
});

module.exports = mongoose.model('UserRelay', UserRelay);
