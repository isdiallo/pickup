"use strict";

const mongoose = require("mongoose");

var packageSchema = new mongoose.Schema({
  name: {
    type: String
    // required: true
  },
  height: {
    type: Number,
    required: true
  },
  length: {
    type: Number,
    required: true
  },
  width: {
    type: Number,
    required: true
  },
  weight: {
    type: Number,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  fragile: {
    type: Boolean,
    default: false
  }
});

module.exports = packageSchema;
