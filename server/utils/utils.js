"use strict";
const Promise = require("promise");

const config = require("../config");
const Messages = require("../models/message");

const sendCode = function(code, from, to) {
  console.log("sending code");
  console.log(code);
  console.log("from", from);
  console.log("to", to);
  saveMsg(
    {
      from: from,
      content: config.validationCodeMsg + " " + code
    },
    to
  );
};

const computePackages = function(packages) {
  return Object.keys(packages).map(function(packType) {
    var returnedPack = config.pickupSizeMapper[packType];
    return {
      height: config.pickupSizeMapper[packType]["height"],
      length: config.pickupSizeMapper[packType]["length"],
      width: config.pickupSizeMapper[packType]["width"],
      weight: config.pickupSizeMapper[packType]["weight"],
      type: packType,
      quantity: packages[packType]["quantity"],
      fragile: packages[packType]["fragile"]
    };
  });
};

const computePickupCharge = function(packages) {
  return 15000;
};

const saveMsg = function(message, to) {
  return new Promise(function(resolve, reject) {
    Messages.findOneOrCreate(to, function(err, msg) {
      if (err) {
        return reject(err);
      }
      msg.messages.push(message);
      msg.save(function(err, result) {
        if (err) {
          return reject(err);
        }
        return resolve(result);
      });
    });
  });
};

module.exports = {
  sendCode: sendCode,
  computePackages: computePackages,
  computePickupCharge: computePickupCharge,
  saveMsg: saveMsg
};
