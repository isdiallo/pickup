'use strict';

const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const config = require('./config');

mongoose.connect(config.mongoUrl);
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error'));
db.on('open', function() {
  console.log('Connected correctly to the server');
});

var users = require('./routes/users');
var pickups = require('./routes/pickup');
var relays = require('./routes/relay');
var messages = require('./routes/message');

var app = express();

// Secure traffic only
// Uncomment the next lines to only allow secure traffic (https)
// app.all('*', function(req, res, next){
//   if (req.secure){
//     return next();
//   }
//   res.redirect('https://'+req.hostname+':'+app.get('secPort')+req.url);
// });

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Passport config
var User = require('./models/user');
var Merchant = require('./models/userMerchant');
var UserRelay = require('./models/userRelay');
var Agent = require('./models/userAgent');

app.use(passport.initialize());

passport.use(new LocalStrategy(User.model.authenticate()));
passport.serializeUser(User.model.serializeUser());
passport.deserializeUser(User.model.deserializeUser());

app.use(express.static(path.join(__dirname, '../dist')));

app.use('/api/users', users);
app.use('/api/pickup', pickups);
app.use('/api/relay', relays);
app.use('/api/message', messages);

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../dist/index.html'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: err
  });
});

module.exports = app;
