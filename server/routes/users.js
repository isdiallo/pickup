'use strict';
const express = require('express');

const usersCtrl = require('../controllers/users');

const Verify = require('../controllers/verify');

const userRouter = express.Router();

/***
  Register a new user: Merchant / Agent / Relay
**/
userRouter.route('/register').post(usersCtrl.register);

/***
  Current user end point
**/
userRouter
  .route('/current')
  /***
   * Get current user information
   */
  .get(Verify.verifyOrdinaryUser, usersCtrl.getCurrent)
  .put(Verify.verifyOrdinaryUser, usersCtrl.updateCurrent);

/***
 * Password Update
 */
userRouter
  .route('/password')
  .put(Verify.verifyOrdinaryUser, usersCtrl.changePassword);

/***
  Get the list of agents
**/
userRouter
  .route('/agents')
  .get(Verify.verifyOrdinaryUser, Verify.verifyAdminUser, usersCtrl.getAgents);

/***
  Relay users en point
**/
userRouter
  .route('/usersrelay')
  /***
   * Get list of Relay Users
   */
  .get(
    Verify.verifyOrdinaryUser,
    Verify.verifyMerchantUser,
    usersCtrl.getRelayUsers
  );

// Update the current user
// .put(Verify.verifyOrdinaryUser, function(req, res, next){
//   // The user want to update his own profile
//   if (req.body.username === req.decoded.username && req.body._id === req.decoded._id){
//     var query = {'_id': req.decoded._id, 'username': req.decoded.username};
//     delete req.body.username;
//     delete req.body._id;
//     User.update(query, req.body, function(err, resp){
//       if (err) {
//         return next(err);
//       }
//       User.findById(req.decoded._id, function(err, user){
//         if (err){
//           return next(err);
//         }
//         // remove sensitive information
//         delete user.hash;
//         delete user.salt;
//         delete user__v;
//         // Updated req.decoded
//         req.decoded = user;
//         return res.json({'msg': 'User information has been updated successfully', 'data': user});
//       });

//     });
//   }else{
//     return res.status(403).json({
//       err: "Forbidden action"
//     });
//   }
// })

/***
  Log In an user
***/
userRouter.route('/login').post(usersCtrl.login);

/***
  Log Out an user
***/
userRouter.route('/logout').get(usersCtrl.logout);

module.exports = userRouter;
