"use strict";

const express = require("express");

const pickupCtrl = require("../controllers/pickup");
const Verify = require("../controllers/verify");

const pickupRouter = express.Router();

pickupRouter
  .route("/")

  /***
  Creation of a pickup with / without a relay
  ==> See data.js pickup for an example of a payload
***/
  .post(
    Verify.verifyOrdinaryUser,
    pickupCtrl.verifyUsersId,
    pickupCtrl.createPickup
  )

  /***
  Fetch list of pickups for a given user:
    - Merchant: will look for pickups with fromId == decode id
    - Agent: will look for pickups with assignation.to == decode id
    - Realy User: will look for pickups with toId == decode id
    - Admin User: will look for all pickups with status === 'VALIDATED'
***/
  .get(Verify.verifyOrdinaryUser, pickupCtrl.getPickups);

/***
  Fecth a given pickup
**/
pickupRouter
  .route("/:pickupId")
  .get(Verify.verifyOrdinaryUser, pickupCtrl.fetchPickup);

/***
  Fetch pickup packages
**/

pickupRouter
  .route("/:pickupId/packages")
  .get(Verify.verifyOrdinaryUser, pickupCtrl.fetchPickupPackages);

/**
 * Generate the code and send it to the matching user
 **/

pickupRouter
  .route("/:pickupId/sendcode")
  .put(
    Verify.verifyOrdinaryUser,
    Verify.verifyUserCanSendCode,
    pickupCtrl.sendPickupCode
  );

/**
 * Validate a pickup so that the process can start
 **/
pickupRouter
  .route("/:pickupId/validate")
  .put(
    Verify.verifyOrdinaryUser,
    Verify.verifyMerchantUser,
    pickupCtrl.validatePickup
  );

/**
 * Validate a pickup code
 *   - Payload: code
 **/
pickupRouter
  .route("/:pickupId/validatecode")
  .put(
    Verify.verifyOrdinaryUser,
    Verify.verifyUserCanSendCode,
    pickupCtrl.validatePickupCode
  );

/***
  Assign a pickup to an Agent
  Payload: 
    pickupId: id of the pickup
    to: Agent Id
**/
pickupRouter
  .route("/assign")
  // TODO: Check the to is an Agent and pickup state is Validated
  .post(
    Verify.verifyOrdinaryUser,
    Verify.verifyAdminUser,
    pickupCtrl.assignPickup
  );

/***
 * Simulate Pickup creation so that merchant can have the possible charges info
 */
pickupRouter
  .route("/simulate")
  .post(
    Verify.verifyOrdinaryUser,
    Verify.verifyMerchantUser,
    pickupCtrl.simulatePickupCreation
  );

module.exports = pickupRouter;
