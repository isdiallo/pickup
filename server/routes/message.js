"use strict";

const express = require("express");
const Verify = require("../controllers/verify");
const messageCtr = require("../controllers/message");

const messageRouter = express.Router();

messageRouter
  .route("/")
  .post(Verify.verifyOrdinaryUser, messageCtr.saveMessage)
  .get(Verify.verifyOrdinaryUser, messageCtr.getMessage);

module.exports = messageRouter;
