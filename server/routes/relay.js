'use strict';

const express = require('express');

const relayCtrl = require('../controllers/relay');
const Verify = require('../controllers/verify');

const relayRouter = express.Router();

relayRouter.route('/')
.post(Verify.verifyOrdinaryUser, Verify.verifyRelayUser, relayCtrl.createRelay)

module.exports = relayRouter;
