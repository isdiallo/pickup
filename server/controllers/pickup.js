'use strict';

const _ = require('lodash');
const cc = require('coupon-code');
const mongoose = require('mongoose');

const EndUser = require('../models/endUser');
const User = require('../models/user').model;
const Pickup = require('../models/pickup');
const async = require('async');

const config = require('../config');
const utils = require('../utils');

// While creating a pickup, make sure that merchant and relay users' ids are corrects
const verifyUsersId = function(req, res, next) {
  // Auth user should be the one who's creating the pickup
  if (req.decoded._id !== req.body.fromId) {
    var err = new Error(
      "The creator of a pickup is not the one who's authenticated."
    );
    err.status = 403;
    return next(err);
  }
  User.findById(req.body.fromId).exec(function(err, user) {
    if (err) {
      return next(err);
    }
    if (_.isEmpty(user) || user._type !== 'Merchant') {
      var err = new Error(
        'The creator of a pickup should be a Business (Merchant) user.'
      );
      err.status = 403;
      return next(err);
    } else if (req.body.useRelay) {
      User.findById(req.body.toId).exec(function(err, user) {
        if (err) {
          return next(err);
        }
        if (_.isEmpty(user) || user._type !== 'UserRelay') {
          var err = new Error(
            'The receiver of a pickup should be a Relay user.'
          );
          err.status = 403;
          return next(err);
        } else {
          next();
        }
      });
    } else {
      next();
    }
  });
};

const createPickup = function(req, res, next) {
  var packages = utils.computePackages(req.body.packages);
  var charge = utils.computePickupCharge(req.body.packages);
  var endUserQuery = {
    firstname: req.body.endUser.firstname,
    lastname: req.body.endUser.lastname,
    phone: req.body.endUser.phone
  };

  EndUser.findOneOrCreate(endUserQuery, req.body.endUser, function(
    err,
    endUser
  ) {
    if (err) {
      return res.status(500).json({
        error: {
          message: err.name ? err.name : 'pickup.create.error',
          description: err.message
            ? err.message
            : 'An error occurs while creating the pickup. Please contact an administrator'
        }
      });
    }
    _.set(req.body, 'endUserId', endUser._id);
    _.set(req.body, 'state', {
      name: config.pickupCreationState,
      owner: req.body.fromId,
      code: cc.generate(config.ccConfig)
    });
    _.set(req.body, 'history', [
      {
        who: req.body.fromId,
        name: 'CREATED',
        state: req.body.state
      }
    ]);
    _.set(req.body, 'packages', packages);
    _.set(req.body, 'cost', charge);
    var projection = '-history.state.code -state.code -__v';

    // Using findOneAndUpdate here for creation so that I can populate
    // the return created pickup
    Pickup.findOneAndUpdate({ _id: mongoose.Types.ObjectId() }, req.body, {
      new: true,
      upsert: true,
      runValidators: true,
      setDefaultsOnInsert: true
    })
      .populate({
        path: 'endUserId',
        model: 'EndUser',
        select: '-updatedAt -createdAt -__v -_id'
      })
      .populate({
        path: 'toId',
        model: 'User',
        select:
          '-updatedAt -createdAt -mail -username -__v -_id -city -town -address',
        populate: {
          path: 'relayId',
          model: 'Relay',
          select: '-updatedAt -createdAt -__v -_id'
        }
      })
      .populate({
        path: 'fromId',
        model: 'User',
        select:
          '-__v -_id -updatedAt -createdAt -username -useRelay -sendNotif -autoAccept'
      })
      .populate({
        path: 'state.owner',
        model: 'User',
        select:
          '-__v -_id -updatedAt -createdAt -username -useRelay -sendNotif -autoAccept -city -town -address -mail -websiteName -websiteUrl'
      })
      .populate({
        path: 'history.who',
        model: 'User',
        select: 'firstname lastname _type'
      })
      .populate({
        path: 'history.state.owner',
        model: 'User',
        select: 'firstname lastname _type'
      })
      .populate({
        path: 'assignation.to',
        model: 'Agent',
        select:
          '-updatedAt -createdAt -__v -_id -mail -_type -address -city -town -username'
      })
      .select(projection)
      .exec(function(err, pickup) {
        if (err) {
          return res.status(500).json({
            error: {
              message: err.name ? err.name : 'pickup.create.error',
              description: err.message
                ? err.message
                : 'An error occurs while creating the pickup. Please contact an administrator'
            }
          });
        }
        res.json({
          success: {
            message: 'pickup.creation.success',
            description: 'Pickup created successfuly.'
          },
          data: pickup
        });
      });
  });
};

const simulatePickupCreation = function(req, res, next) {
  return res.json({
    success: {
      message: 'pickup,simulation.success',
      description: 'Pickup creation simulated succesfully'
    },
    data: {
      charge: 15000
    }
  });
};

const getPickups = function(req, res, next) {
  var query = {};
  var projection = '-history.state.code -state.code -__v';
  switch (req.decoded._type) {
    case 'Merchant':
      query['fromId'] = req.decoded._id;
      break;
    case 'Agent':
      query['assignation.to'] = req.decoded._id;
      break;
    case 'UserRelay':
      query['toId'] = req.decoded._id;
      break;
    case 'Admin':
      query['state.name'] = 'VALIDATED';
      break;
  }
  if (_.isEmpty(query)) {
    return res.status(403).json({
      error: {
        message: 'pickup.error.forbidden',
        description: 'You are not authorized to perform this action.'
      }
    });
  }
  Pickup.find(query)
    .populate({
      path: 'endUserId',
      model: 'EndUser',
      select: '-updatedAt -createdAt -__v -_id'
    })
    .populate({
      path: 'toId',
      model: 'User',
      select:
        '-updatedAt -createdAt -mail -username -__v -_id -city -town -address',
      populate: {
        path: 'relayId',
        model: 'Relay',
        select: '-updatedAt -createdAt -__v -_id'
      }
    })
    .populate({
      path: 'fromId',
      model: 'User',
      select:
        '-__v -_id -updatedAt -createdAt -username -useRelay -sendNotif -autoAccept'
    })
    .populate({
      path: 'state.owner',
      model: 'User',
      select:
        '-__v -_id -updatedAt -createdAt -username -useRelay -sendNotif -autoAccept -city -town -address -mail -websiteName -websiteUrl'
    })
    .populate({
      path: 'history.who',
      model: 'User',
      select: 'firstname lastname _type'
    })
    .populate({
      path: 'history.state.owner',
      model: 'User',
      select: 'firstname lastname _type'
    })
    .populate({
      path: 'assignation.to',
      model: 'Agent',
      select:
        '-updatedAt -createdAt -__v -_id -mail -_type -address -city -town -username'
    })
    .select(projection)
    .exec(function(err, pickup) {
      if (err) {
        return res.status(500).json({
          error: {
            message: err.name ? err.name : 'pickup.fecth.all.error',
            description: err.message
              ? err.message
              : 'An error occurs while fetching your pickups. Please contact an administrator'
          }
        });
      }
      return res.json({
        success: {
          message: 'pickup.fetch.all.success',
          description: 'You have fetched successfuly your pickups.'
        },
        data: pickup
      });
    });
};

const assignPickup = function(req, res, next) {
  var saveStatus = {
    success: [],
    error: []
  };
  Pickup.find({ _id: { $in: req.body.pickups } }).exec(function(err, pickups) {
    if (err) {
      return res.status(500).json({
        error: {
          message: err.name ? err.name : 'pickup.assign.error',
          description: err.message
            ? err.message
            : 'An error occurs while assigning the pickup to an agent. Please contact an administrator'
        }
      });
    }

    if (_.isEmpty(pickups)) {
      return res.status(500).json({
        error: {
          message: 'pickup.assign.notFound',
          description:
            'Internal Error: We are having some trouble to locate your pickup (s).'
        }
      });
    }
    async.eachSeries(
      pickups,
      function(pickup, nextPickup) {
        pickup.state.name = 'ASSIGNED';
        pickup.assignation = {
          to: req.body.to,
          by: req.decoded._id
        };
        pickup.history.push({
          who: req.decoded._id,
          name: 'UPDATED',
          state: pickup.state
        });

        pickup.save(function(err, result) {
          if (err) {
            saveStatus.error.push(err);
          } else {
            saveStatus.success.push(result._id);
          }
          nextPickup();
        });
      },
      function() {
        if (saveStatus.success.length == 0) {
          return res.status(500).json({
            error: {
              message: 'pickup.assign.error',
              description:
                'An error occurs while assigning pickups. Please contact an administrator'
            },
            data: saveStatus.error
          });
        }
        return res.json({
          success: {
            message: 'pickup.assignation.success',
            description: 'The pickup has been successfuly assigned to an agent'
          },
          data: saveStatus
        });
      }
    );
  });
};

const validatePickup = function(req, res, next) {
  Pickup.findById(req.params.pickupId).exec(function(err, pickup) {
    if (err) {
      return res.status(500).json({
        error: {
          message: err.name ? err.name : 'pickup.validate.error',
          description: err.message
            ? err.message
            : 'An error occurs while validating your pickup. Please contact an administrator'
        }
      });
    }
    // If the pickup is not found
    if (_.isEmpty(pickup)) {
      return res.status(500).json({
        error: {
          message: 'pickup.validate.notFound',
          description:
            'Internal Error: We are having some trouble to locate your pickup.'
        }
      });
    }
    // Can only validate your own pickup
    if (req.decoded._id !== pickup.fromId.toString()) {
      return res.status(403).json({
        error: {
          message: 'pickup.validate.notAuthorized',
          description:
            'You are only authorized to validate the pickups you created.'
        }
      });
    }
    // Can only validate a pickup with  creation state
    if (pickup.state.name !== config.pickupCreationState) {
      return res.status(403).json({
        error: {
          message: 'pickup.validate.forbidden',
          description:
            'You are only authorized to validate a pickup with ' +
            config.pickupCreationState +
            ' state'
        }
      });
    } else {
      pickup.state.name = 'VALIDATED';
      // update the history
      pickup.history.push({
        who: req.decoded._id,
        name: 'UPDATED',
        state: pickup.state
      });
      pickup.save(function(err, pickup) {
        if (err) {
          return res.status(500).json({
            error: {
              message: err.name ? err.name : 'pickup.validate.error',
              description: err.message
                ? err.message
                : 'An error occurs while validating your pickup. Please contact an administrator'
            }
          });
        }
        return res.json({
          success: {
            message: 'pickup.validate.success',
            description:
              'You have validated successfuly the pickup. The process will start soon.'
          },
          data: { _id: pickup._id }
        });
      });
    }
  });
};

const sendPickupCode = function(req, res, next) {
  Pickup.findById(req.params.pickupId)
    .populate('fromId') // Why these 3 populates? if removed, then change sendCode below
    .populate('toId')
    .populate('endUserId')
    .exec(function(err, pickup) {
      if (err) {
        return res.status(500).json({
          error: {
            message: err.name ? err.name : 'pickup.sendcode.error',
            description: err.message
              ? err.message
              : 'An error occurs while sending the code for your pickup. Please contact an administrator'
          }
        });
      }
      if (_.isEmpty(pickup)) {
        return res.status(500).json({
          error: {
            message: 'pickup.sendcode.notFound',
            description:
              'Internal Error: We are having some trouble to locate your pickup.'
          }
        });
      }
      // Check if the user can send a code while in this state
      if (
        _.indexOf(
          config.userTypesWithCode[req.decoded._type],
          pickup.state.name
        ) < 0
      ) {
        return res.status(403).json({
          error: {
            message: 'pickup.sendcode.forbidden',
            description:
              'You are not authorized to perform sending code action in this pickup state.'
          }
        });
      }
      pickup.state.code = cc.generate(config.ccConfig);
      pickup.save(function(err, pickup) {
        if (err) {
          return res.status(500).json({
            error: {
              message: err.name ? err.name : 'pickup.sendcode.error',
              description: err.message
                ? err.message
                : 'An error occurs while sending the code for your pickup. Please contact an administrator'
            }
          });
        }
        switch (req.decoded._type) {
          case 'Merchant': // A merchant can only send code to an agent
            utils.sendCode(
              pickup.state.code,
              req.decoded._id,
              _.get(pickup, 'assignation.to')
            );
            break;
          case 'Agent': // An agent can send code to a relay or the end user
            if (pickup.toId) {
              console.log('Sending code to a relay');
              utils.sendCode(
                pickup.state.code,
                req.decoded._id,
                pickup.toId._id
              );
            } else {
              console.log('sending code to an end user');
              utils.sendCode(
                pickup.state.code,
                req.decoded._id,
                pickup.endUserId._id
              );
            }
            break;
          case 'UserRelay': // A relay can only send code to the end user
            utils.sendCode(
              pickup.state.code,
              req.decoded._id,
              pickup.endUserId._id
            );
            break;
        }
        return res.json({
          success: {
            message: 'pickup.sendcode.success',
            description: 'You have send successfuly a validation code.'
          },
          data: {}
        });
      });
    });
};

const validatePickupCode = function(req, res, next) {
  Pickup.findById(req.params.pickupId)
    .populate('fromId')
    .populate('toId')
    .populate('endUserId')
    .exec(function(err, pickup) {
      if (err) {
        return res.status(500).json({
          error: {
            message: err.name ? err.name : 'pickup.validatePickup.error',
            description: err.message
              ? err.message
              : 'An error occurs while validating the code of your pickup. Please contact an administrator'
          }
        });
      }
      if (_.isEmpty(pickup)) {
        return res.status(500).json({
          error: {
            message: 'pickup.validatePickup.notFound',
            description:
              'Internal Error: We are having some trouble to locate your pickup.'
          }
        });
      }
      // Check that the user is the one who send the code
      if (req.decoded._id !== pickup.state.owner.toString()) {
        return res.status(403).json({
          error: {
            message: 'pickup.validatePickup.forbidden',
            description:
              'You are not authorized to perform validation code action in this pickup state.'
          }
        });
      }
      // Check the sent code is matching with expected code
      if (req.body.code !== pickup.state.code) {
        return res.status(403).json({
          error: {
            message: 'pickup.validatePickup.mismatch',
            description: 'The given code is not matching the expected code.'
          }
        });
      }
      // Update pickup history
      pickup.history.push({
        who: req.decoded._id,
        name: 'UPDATED',
        state: pickup.state
      });
      // update the state of the pickup
      pickup.state = {
        code: cc.generate(config.ccConfig)
      };
      switch (req.decoded._type) {
        case 'Merchant':
          pickup.state.name = 'ONGOING';
          pickup.state.owner = pickup.assignation.to; // Agent user
          break;
        case 'Agent':
          if (pickup.toId) {
            pickup.state.name = 'TRANSMITTED';
            pickup.state.owner = pickup.toId; // Relay user
          } else {
            pickup.state.name = 'DELIVERED';
            pickup.state.owner = pickup.endUserId; // End user
          }
          break;
        case 'UserRelay':
          pickup.state.name = 'DELIVERED';
          pickup.state.owner = pickup.endUserId; // End user
          break;
        default:
          return res.status(500).json({
            error: {
              message: 'pickup.validatePickup.error',
              description:
                'An internal error occurs while trying to validate the pickup code. Please contact an Administrator.'
            }
          });
      }
      pickup.save(function(err, pickup) {
        if (err) {
          return res.status(500).json({
            error: {
              message: 'pickup.validatePickup.error',
              description:
                'An internal error occurs while trying to validate the pickup code. Please contact an Administrator.'
            }
          });
        }
        getPickup(req, res, next);
      });
    });
};

const fetchPickup = function(req, res, next) {
  getPickup(req, res, next);
};

const fetchPickupPackages = function(req, res, next) {
  Pickup.findById(req.params.pickupId)
    .select('packages')
    .exec(function(err, pickup) {
      if (err) {
        return res.status(500).json({
          error: {
            message: 'pickup.packages.error',
            description:
              'An internal error occurs while fetching pickup packages. Please contact an Administrator.'
          }
        });
      }
      return res.json({
        success: {
          message: 'pickup.packages.success',
          description: 'You have fetched successfuly a pickup packages.'
        },
        data: pickup
      });
    });
};

const getPickup = function(req, res, next) {
  var projection = '-history.state.code -state.code -__v';

  switch (req.decoded._type) {
    case 'Agent':
      projection += ' -isFree -cost';
      break;
    case 'UserRelay':
      projection += ' -isFree -cost';
      break;
  }
  Pickup.findById(req.params.pickupId)
    .populate({
      path: 'toId',
      select:
        '-updatedAt -createdAt -mail -username -__v -_id -city -town -address',
      populate: {
        path: 'relayId',
        model: 'Relay',
        select: '-updatedAt -createdAt -__v -_id'
      }
    })
    .populate({
      path: 'endUserId',
      select: '-updatedAt -createdAt -__v -_id'
    })
    .populate({
      path: 'fromId',
      select:
        '-__v -_id -updatedAt -createdAt -username -useRelay -sendNotif -autoAccept'
    })
    .populate({
      path: 'state.owner',
      select:
        '-__v -_id -updatedAt -createdAt -username -useRelay -sendNotif -autoAccept -city -town -address -mail -websiteName -websiteUrl'
    })
    .populate({
      path: 'history.who',
      select: 'firstname lastname _type'
    })
    .populate({
      path: 'history.state.owner',
      select: 'firstname lastname _type'
    })
    .populate({
      path: 'assignation.to',
      model: 'Agent',
      select:
        '-updatedAt -createdAt -__v -_id -mail -_type -address -city -town -username'
    })
    .select(projection)
    .exec(function(err, pickup) {
      if (err) {
        return res.status(500).json({
          error: {
            message: 'pickup.fecth.single.error',
            description:
              'An internal error occurs while trying to fetch the pickup. Please contact an Administrator.'
          }
        });
      }
      if (!pickup) {
        return res.status(404).json({
          error: {
            message: 'pickup.fetch.single.notFound',
            description: 'The pickup you are looking for doesnt exist.'
          }
        });
      }
      res.json({
        success: {
          message: 'pickup.fetch.single.success',
          description: 'You have fetched successfuly a pickup.'
        },
        data: pickup
      });
    });
};

module.exports = {
  verifyUsersId: verifyUsersId,
  createPickup: createPickup,
  simulatePickupCreation: simulatePickupCreation,
  getPickups: getPickups,
  assignPickup: assignPickup,
  validatePickup: validatePickup,
  sendPickupCode: sendPickupCode,
  validatePickupCode: validatePickupCode,
  fetchPickup: fetchPickup,
  fetchPickupPackages: fetchPickupPackages
};
