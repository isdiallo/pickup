'use strict';

var jwt = require('jsonwebtoken');

var config = require('../config');

// Return authentication token
exports.getToken = function(user) {
  // Take only common fields as most of the servers reduce http request size to 8KB
  var jwtUser = {
    username: user.username,
    firstname: user.firstname,
    lastname: user.lastname,
    phone: user.phone,
    _type: user._type,
    _id: user._id,
    address: user.address,
    city: user.city,
    town: user.town
  };
  return jwt.sign(jwtUser, config.secretKey, {
    expiresIn: 3600
  });
};

// Make sure an user is connected
exports.verifyOrdinaryUser = function(req, res, next) {
  // Check header or url params or post params for token
  var token =
    req.body.token || req.query.token || req.headers['x-access-token'];
  // decode token
  if (token) {
    // Verifies secret and checks exp
    jwt.verify(token, config.secretKey, function(err, decoded) {
      if (err) {
        var err = new Error('You are not authenticated!');
        err.status = 401;
        return next(err);
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    var err = Error('No token provided');
    err.status = 403;
    return next(err);
  }
};

exports.verifyMerchantUser = function(req, res, next) {
  if (req.decoded._type !== 'Merchant') {
    var err = new Error('The current user is not a Business (Merchant) user.');
    err.status = 403;
    return next(err);
  } else {
    next();
  }
};

exports.verifyAdminUser = function(req, res, next) {
  if (req.decoded._type !== 'Admin') {
    var err = new Error('The current user is not an administrator.');
    err.status = 403;
    return next(err);
  } else {
    next();
  }
};

exports.verifyRelayUser = function(req, res, next) {
  if (req.decoded._type !== 'UserRelay') {
    var err = new Error('The current user is not a relay user.');
    err.status = 403;
    return next(err);
  } else {
    next();
  }
};

exports.verifyAdminUser = function(req, res, next) {
  if (req.decoded._type !== 'Admin') {
    var err = new Error('The current user is not an administrator.');
    err.status = 403;
    return next(err);
  } else {
    next();
  }
};

exports.verifyUserCanSendCode = function(req, res, next) {
  if (config.codeSenders.indexOf(req.decoded._type) < 0) {
    return res.status(403).json({
      error: {
        message: 'pickup.code.sending.notAuthorized',
        description: 'You are not authorized to perform sending code action.'
      }
    });
  } else {
    next();
  }
};
