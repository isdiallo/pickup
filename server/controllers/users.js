'use strict';

const passport = require('passport');
const _ = require('lodash');
const User = require('../models/user');
const Merchant = require('../models/userMerchant');
const Agent = require('../models/userAgent');
const UserRelay = require('../models/userRelay');

const Verify = require('./verify');

const register = function(req, res, next) {
  var user, UserModule;
  switch (req.body.type) {
    case 'MERCHANT':
      user = new Merchant(req.body);
      UserModule = Merchant;
      break;
    case 'AGENT':
      user = new Agent(req.body);
      UserModule = Agent;
      break;
    case 'RELAY':
      user = new UserRelay(req.body);
      UserModule = UserRelay;
      break;
    default:
      return res.status(500).json({
        error: {
          message: 'register.type.incorrect',
          description: 'The provided user type is not known by the system'
        }
      });
  }

  UserModule.register(user, req.body.password, function(err, user) {
    if (err) {
      return res.status(500).json({
        error: {
          message: err.name ? err.name : 'register.error',
          description: err.message
            ? err.message
            : 'An error occurs during the registration process. Please contact an administrator'
        }
      });
    }
    user.save(function(err, user) {
      if (err) {
        return res.status(500).json({
          error: {
            message: err.name ? err.name : 'register.error',
            description: err.message
              ? err.message
              : 'An error occurs during the registration process. Please contact an administrator'
          }
        });
      }
      passport.authenticate('local', function(err, user, info) {
        // Log the user
        req.logIn(user, function(err) {
          if (err) {
            return res.status(500).json({
              error: {
                message: err.name ? err.name : 'register.error',
                description: err.message
                  ? err.message
                  : 'An error occurs during the registration process. Please contact an administrator'
              }
            });
          }
          var token = Verify.getToken(user);
          res.status(200).json({
            success: {
              name: 'login.success',
              message: 'You have been logged in successfully'
            },
            token: token
          });
        });
      })(req, res, next);
    });
  });
};

const getCurrent = function(req, res, next) {
  User.model
    .findById(req.decoded._id)
    .populate({
      path: 'relayId',
      model: 'Relay',
      select: '-updatedAt -createdAt -__v'
    })
    .exec(function(err, user) {
      if (err) {
        return res.status(500).json({
          error: {
            message: err.name ? err.name : 'user.current.error',
            description: err.message
              ? err.message
              : 'An error occurs while fetching your information. Please contact an administrator'
          }
        });
      }
      return res.json({
        success: {
          message: 'user.current.success',
          description: 'You have successfully fetched your user information'
        },
        data: user
      });
    });
};

const getAgents = function(req, res, next) {
  User.model
    .find({ _type: 'Agent' })
    .select('_id lastname firstname phone address city town')
    .exec(function(err, agents) {
      if (err) {
        return res.status(500).json({
          error: {
            message: err.name ? err.name : 'users.agents.error',
            description: err.message
              ? err.message
              : 'An error occurs while fetching list of agents. Please contact an administrator'
          }
        });
      }
      return res.json({
        success: {
          message: 'user.agents.success',
          description: 'You have fetched successfuly list of agents.'
        },
        data: agents
      });
    });
};

const getRelayUsers = function(req, res, next) {
  User.model
    .find({ _type: 'UserRelay', relayId: { $ne: null } })
    .populate({
      path: 'relayId',
      model: 'Relay',
      select: '-updatedAt -createdAt -__v'
    })
    .select('_id lastname firstname phone address city town relayId')
    .exec(function(err, userRelays) {
      if (err) {
        return res.status(500).json({
          error: {
            message: err.name ? err.name : 'user.relayUsers.error',
            description: err.message
              ? err.message
              : 'An error occurs while fetching the list of relay users. Please contact an administrator'
          }
        });
      }
      return res.json({
        success: {
          message: 'user.relayUsers.success',
          description: 'You have successfully fetched list of relay users.'
        },
        data: userRelays
      });
    });
};

const updateCurrent = function(req, res, next) {
  let UserModel = User.model;
  switch (req.decoded._type) {
    case 'Merchant': {
      UserModel = Merchant;
      break;
    }
    default: {
      return res.status(403).json({
        error: {
          message: 'user.current.update.forbidden',
          description:
            'You are not authorized to perfom user information update'
        }
      });
    }
  }
  UserModel.findByIdAndUpdate(req.decoded._id, req.body, { new: true })
    .populate({
      path: 'relayId',
      model: 'Relay',
      select: '-updatedAt -createdAt -__v'
    })
    .exec(function(err, updatedUser) {
      if (err) {
        return res.status(500).json({
          error: {
            message: err.name ? err.name : 'user.current.update.error',
            description: err.message
              ? err.message
              : 'An error occurs while updating your information, please contact an administrator'
          }
        });
      }
      return res.json({
        success: {
          message: 'user.current.update.success',
          description: 'You have successfully updated your information.'
        },
        data: updatedUser
      });
    });
};

const login = function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) {
      return res.status(500).json({
        error: {
          message: err.name ? err.name : 'user.login.error',
          description: err.message
            ? err.message
            : 'An error occurs while trying to login. Please contact an administrator'
        }
      });
    }
    if (!user) {
      return res.status(401).json({
        error: {
          message: info.name ? info.name : 'user.login.incorrect',
          description: info.message
            ? info.message
            : 'An error occurs while you are login, please try again later'
        }
      });
    }
    req.logIn(user, function(err) {
      if (err) {
        return res.status(500).json({
          error: {
            message: err.name ? err.name : 'user.login.error',
            description: err.message
              ? err.message
              : 'An error occurs while you are login. Please contact an administrator'
          }
        });
      }
      var token = Verify.getToken(user);
      res.status(200).json({
        success: {
          message: 'user.login.success',
          description: 'You have been logged in successfully'
        },
        token: token
      });
    });
  })(req, res, next);
};

const changePassword = function(req, res, next) {
  if (_.get(req, ['body', 'newPwd']) && _.get(req, ['body', 'currentPwd'])) {
    User.model.findById(req.decoded._id, function(err, user) {
      if (err) {
        return res.status(500).json({
          error: {
            message: err.name ? err.name : 'password.update.error',
            description: err.message
              ? err.message
              : 'An error occurs while updating your password, please contact an administrator'
          }
        });
      }
      user.changePassword(req.body.currentPwd, req.body.newPwd, function(err) {
        if (err) {
          return res.status(500).json({
            error: {
              message: err.name ? err.name : 'password.update.fail',
              description: err.message
                ? err.message
                : 'An error occurs while updating your password, please contact an administrator'
            }
          });
        }
        return res.status(200).json({
          success: {
            message: 'password.update.success',
            description: 'Your password have been changed in our system'
          }
        });
      });
    });
  } else {
    return res.status(500).json({
      error: {
        message: 'password.update.incorrect',
        description:
          'An error occurs while updating your password, please contact an administrator'
      }
    });
  }
};

const logout = function(req, res) {
  req.logout();
  res.status(200).json({
    success: {
      message: 'user.logout.success',
      description: 'You have been logged out successfully'
    }
  });
};

module.exports = {
  register: register,
  getCurrent: getCurrent,
  getAgents: getAgents,
  getRelayUsers: getRelayUsers,
  updateCurrent: updateCurrent,
  login: login,
  changePassword: changePassword,
  logout: logout
};
