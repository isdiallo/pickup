'use strict';

const Relay = require('../models/relay');
const UserRelay = require('../models/userRelay');
const _ = require('lodash');

const createRelay = function(req, res, next) {
  // If there is an Id in the body then it is just an update
  if (_.get(req, ['body', '_id'])) {
    Relay.findOneAndUpdate({ _id: _.get(req, ['body', '_id']) }, req.body, {
      new: true
    }).exec(function(err, relay) {
      if (err) {
        return res.status(500).json({
          error: {
            message: err.name ? err.name : 'relay.update.error',
            description: err.message
              ? err.message
              : 'An error occurs while updating the relay. Please contact an administrator'
          }
        });
      }
      return res.json({
        success: {
          message: 'relay.update.success',
          description: "You've successfuly updated your Relay information."
        },
        data: relay
      });
    });
  } else {
    Relay.findOneOrCreate(req.body, function(err, relay) {
      if (err) {
        return res.status(500).json({
          error: {
            message: err.name ? err.name : 'relay.create.error',
            description: err.message
              ? err.message
              : 'An error occurs while creating the relay. Please contact an administrator'
          }
        });
      }
      UserRelay.findById(req.decoded._id).exec(function(err, user) {
        if (err) {
          return res.status(500).json({
            error: {
              message: err.name ? err.name : 'relay.create.error',
              description: err.message
                ? err.message
                : 'An error occurs while creating the relay. Please contact an administrator'
            }
          });
        }
        user.relayId = relay._id;
        user.save(function(err, user) {
          if (err) {
            return res.status(500).json({
              error: {
                message: err.name ? err.name : 'relay.create.error',
                description: err.message
                  ? err.message
                  : 'An error occurs while creating the relay. Please contact an administrator'
              }
            });
          }
          return res.json({
            success: {
              message: 'relay.creation.success',
              description:
                "You've successfuly created and associated a Relay to your account."
            },
            data: relay
          });
        });
      });
    });
  }
};

module.exports = {
  createRelay: createRelay
};
