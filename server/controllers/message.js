'use strict';

const Messages = require('../models/message');
const utils = require('../utils');

const saveMessage = function(req, res, next) {
  utils.saveMsg(req.body.message, req.body.to).then(
    function(message) {
      return res.json({
        success: {
          message: 'message.save.success',
          description: "You've successfully saved your message"
        },
        data: message
      });
    },
    function(err) {
      return res.status(500).json({
        error: {
          message: err.name ? err.name : 'message.save.error',
          description: err.message
            ? err.message
            : 'An error occurs while saving messages. They may not be delivered. Please contact an administrator'
        }
      });
    }
  );
};

const getMessage = function(req, res, next) {
  Messages.findOne({ to: req.decoded._id })
    .populate({
      path: 'messages.from',
      model: 'User',
      select: 'firstname lastname -_id'
    })
    .select('-messages._id -messages.updatedAt')
    .exec(function(err, messages) {
      if (err) {
        return res.status(500).json({
          error: {
            message: err.name ? err.name : 'message.fetch.error',
            description: err.message
              ? err.message
              : 'An error occurs while fecthing your messages. Please contact an administrator'
          }
        });
      }
      if (messages) {
        messages.messages.sort(function(a, b) {
          return b.createdAt - a.createdAt;
        });
      }
      return res.json({
        success: {
          message: 'message.fetch.success',
          description: "You've successfuly fetched your messages."
        },
        data: messages ? messages : {}
      });
    });
};

module.exports = {
  saveMessage: saveMessage,
  getMessage: getMessage
};
