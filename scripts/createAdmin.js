'use strict';

const _ = require('lodash');
const Admin = require('../server/models/userAdmin');
const config = require('../server/config');

const create = function() {
  console.log('##### Create Admin Script #####');
  Admin.find(config.admin).exec(function(err, admin) {
    if (err) throw err;
    if (_.isEmpty(admin)) {
      console.log('##### Registering the Admin User #####');
      Admin.register(config.admin, config.adminPwd, function(err, admin) {
        if (err) throw err;
        admin.save(function(err, admin) {
          console.log('##### Admin created successfully #####');
        });
      });
    } else {
      console.log('##### Admin already exists, skipping creation! #####');
    }
  });
};

module.exports = {
  create: create
};
