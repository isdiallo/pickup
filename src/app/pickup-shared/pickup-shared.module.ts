import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducers, effects } from './store';
import { LoginService } from './auth/login.service';
import { AuthServerProvider } from './auth/authServerProvider.service';
import { MainService } from './auth/Main.service';
import { UserService } from './auth/userAccount.service';
import * as fromSharedServices from './services';
import {
  AuthInterceptor,
  AuthExpiredInterceptor,
  ErrorsInterceptor,
} from './interceptors';

import { HasAuthorityDirective } from './has-authority.directive';
import { AlertComponent } from './components/alert/alert.component';
import { MatButtonModule } from '@angular/material/button';
import { AlertListComponent } from './components/alert-list/alert-list.component';
import { FooterComponent } from './components/footer/footer.component';
import { RouterModule } from '@angular/router';
import { HasAuthorityStubDirective } from './testing/HasAuthorityDirective.stub';
import { HasAuthorityAgentStubDirective } from './testing/HasAuthorityAgentDirective.stub';
import { HasAuthorityMerchantStubDirective } from './testing/HasAuthorityMerchantDirective.stub';
import { HasAuthorityUserRelayStubDirective } from './testing/HasAuthorityUserRelayDirective.stub';
import { RouterLinkStubDirective } from './testing/RouterLink.stub';

@NgModule({
  declarations: [
    HasAuthorityDirective,
    HasAuthorityStubDirective,
    HasAuthorityAgentStubDirective,
    HasAuthorityMerchantStubDirective,
    HasAuthorityUserRelayStubDirective,
    RouterLinkStubDirective,
    AlertComponent,
    AlertListComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatButtonModule,
    StoreModule.forFeature('appShared', reducers),
    EffectsModule.forFeature(effects),
  ],
  providers: [
    ...fromSharedServices.sharedServices,
    LoginService,
    AuthServerProvider,
    MainService,
    UserService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthExpiredInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorsInterceptor,
      multi: true,
    },
  ],
  exports: [
    HasAuthorityDirective,
    AlertComponent,
    AlertListComponent,
    FooterComponent,
  ],
})
export class PickupSharedModule {}
