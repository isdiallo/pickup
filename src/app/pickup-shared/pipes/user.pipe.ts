import { Pipe, PipeTransform } from '@angular/core';
import { PickupUserInterface } from '../model/index';

@Pipe({
  name: 'userNamePipe'
})
export class UserNamePipe implements PipeTransform {
  transform(input: PickupUserInterface): string {
    return input.firstname + ' ' + input.lastname.charAt(0).toUpperCase() + '.';
  }
}
