import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'userTypePipe'
})
export class UserTypePipe implements PipeTransform {
  transform(input: string): string {
    let returnedValue = 'Unknown';
    switch (input) {
      case 'Merchant':
        returnedValue = 'Business Account';
        break;
      case 'UserRelay':
        returnedValue = 'Relay Account';
        break;
      case 'Agent':
        returnedValue = 'Agent Account';
        break;
      case 'Admin':
        returnedValue = 'Admininstrator Account';
        break;
    }
    return returnedValue;
  }
}
