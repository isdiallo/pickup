import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

import { PICKUP_TOKEN } from '../pickup-shared.constants';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private localStorage: LocalStorageService,
    private sessionStorage: SessionStorageService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // Get the token
    const jwtToken =
      this.localStorage.retrieve(PICKUP_TOKEN) ||
      this.sessionStorage.retrieve(PICKUP_TOKEN);

    // if the token exist the clone the request to add the token to the header.
    if (!!jwtToken) {
      const authReq = req.clone({
        headers: req.headers.set('x-access-token', jwtToken),
      });
      // Pass on the cloned request instead of the original request.
      return next.handle(authReq);
    } else {
      return next.handle(req);
    }
  }
}
