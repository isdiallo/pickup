import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  HttpInterceptor,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
  HttpRequest,
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { LoginService } from '../auth/login.service';

import { catchError } from 'rxjs/operators';
import { IDENTITY_URL_PATTERN } from '../pickup-shared.constants';
import { BroadcasterService } from '../services/broadcaster.service';
import { PickupEvent } from '../model';

@Injectable()
export class ErrorsInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private broadcasterService: BroadcasterService
  ) {}

  getError(error: any): PickupEvent {
    if (error.message && error.description) {
      return {
        type: 'error',
        message: error.message,
        description: error.description,
      };
    } else if (error.error) {
      return this.getError(error.error);
    }
    return {
      type: 'error',
      message: 'internal.error',
      description: 'Internal Error occurs, please contact and administrator.',
    };
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((error: HttpEvent<any>) => {
        if (error instanceof HttpErrorResponse) {
          if (
            error.url.includes(IDENTITY_URL_PATTERN) &&
            (error.status === 401 || error.status === 403)
          ) {
            this.broadcasterService.broadcast({
              type: 'authentication',
              message: 'user.current.error',
              description:
                'You are not authenticated. Please Try again after your authentication.',
            });
          } else {
            this.broadcasterService.broadcast(this.getError(error));
          }
        }
        return throwError(error);
      })
    );
  }
}
