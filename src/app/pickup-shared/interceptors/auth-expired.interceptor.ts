import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpHandler,
  HttpEvent,
  HttpRequest,
  HttpErrorResponse,
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';

import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { IDENTITY_URL_PATTERN, PICKUP_TOKEN } from '../pickup-shared.constants';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

@Injectable()
export class AuthExpiredInterceptor implements HttpInterceptor {
  constructor(
    private route: Router,
    private localStorage: LocalStorageService,
    private sessionStorage: SessionStorageService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((error: HttpEvent<any>) => {
        if (
          error instanceof HttpErrorResponse &&
          (error.status === 403 || error.status === 401) // 403: No token provide, 401: not valid token
        ) {
          this.localStorage.clear(PICKUP_TOKEN);
          this.sessionStorage.clear(PICKUP_TOKEN);
          // If we are on the app side then redirect to the login page
          if (this.route.url.startsWith('/app')) {
            this.route.navigate(['/login']);
          }
        }
        return throwError(event);
      })
    );
  }
}
