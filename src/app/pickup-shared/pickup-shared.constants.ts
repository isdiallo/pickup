export const PICKUP_TOKEN = 'token';
export const USER_TYPES = [
  {
    type: 'RELAY',
    value: 'Relay User Account'
  },
  {
    type: 'MERCHANT',
    value: 'Business User Account'
  },
  {
    type: 'AGENT',
    value: 'Agent User Account'
  }
];
export const MAX_MOBILE_WIDTH = '668px';
export const IDENTITY_URL_PATTERN = 'api/users/current';
export const MODAL_MIN_WIDTH = '22rem';
export const DEFAULT_REDIRECT_AFTER_LOGIN = '/app';
export const MAIL_REGEX = new RegExp(/\S+@\S+\.\S+/);
export const PHONE_REGEX = new RegExp(/^\d{9}$/);
export const URL_REGEX = new RegExp(/https?:\/\/[-a-zA-Z0-9@:%._\+~#=]/);
