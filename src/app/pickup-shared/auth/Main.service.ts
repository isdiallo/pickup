/******
 * Is this Service still used? To be removed otherwise
 ******/

import { Injectable } from '@angular/core';
import { UserService } from './userAccount.service';
import { Subject, Observable } from 'rxjs';
import { UserType } from '../model/index';

@Injectable()
export class MainService {
  private userIdentity: any;
  private authenticated = false;
  private authenticationState = new Subject<any>();
  constructor(private userService: UserService) {}

  authenticate(identity) {
    this.userIdentity = identity;
    this.authenticated = !!this.userIdentity;
    this.authenticationState.next(this.userIdentity);
  }

  /******
   * If the authorities is undefined or is a empty list or
   * the connected user type is in the authorities' list then the user is granted authority
   ******/
  hasAuthority(authorities: any[], userType: UserType): boolean {
    return (
      !authorities || authorities.length === 0 || authorities.includes(userType)
    );
  }

  // If force is given, we retrieve update userInfo with values from the BE (reset)
  identity(force?: boolean) {
    if (force) {
      this.userIdentity = undefined;
    }
    if (!!this.userIdentity) {
      return Promise.resolve(this.userIdentity);
    }
    return this.userService
      .getUser()
      .toPromise()
      .then((account) => {
        if (!!account) {
          this.userIdentity = account;
        } else {
          this.userIdentity = null;
        }
        this.authenticated = !!account;
        this.authenticationState.next(this.userIdentity);
        return this.userIdentity;
      })
      .catch((error) => {
        this.userIdentity = null;
        this.authenticated = false;
        this.authenticationState.next(this.userIdentity);
        return null;
      });
  }

  isAuthenticated(): boolean {
    return this.authenticated;
  }

  isIdentityResolved(): boolean {
    return !!this.userIdentity;
  }

  getAuthenticationState(): Observable<any> {
    return this.authenticationState.asObservable();
  }

  getUserImg(): string {
    return this.isIdentityResolved ? this.userIdentity.imageUrl : '';
  }
}
