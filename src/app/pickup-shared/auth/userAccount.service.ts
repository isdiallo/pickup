import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {
  UserAccountResponseInterface,
  UserUpdateInterface,
  RelayCreateOrUpdateResponseInterface,
} from '../model/auth.model';
import { RegisterInterface } from '../model/index';
import { RelayInterface } from '../model/relay.model';
import { catchError } from 'rxjs/operators';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {}

  getUser(): Observable<UserAccountResponseInterface> {
    return this.http
      .get<UserAccountResponseInterface>('/api/users/current')
      .pipe(catchError((error: any) => throwError(error)));
  }

  registerUser(payload: RegisterInterface): Observable<any> {
    return this.http
      .post('/api/users/register', payload)
      .pipe(catchError((error: any) => throwError(error)));
  }

  updateCurrentUser(
    updateUserObj: UserUpdateInterface
  ): Observable<UserAccountResponseInterface> {
    return this.http
      .put<UserAccountResponseInterface>(`api/users/current`, updateUserObj)
      .pipe(catchError((error: any) => throwError(error)));
  }

  createOrUpdateRelay(
    relay: RelayInterface
  ): Observable<RelayCreateOrUpdateResponseInterface> {
    return this.http
      .post<RelayCreateOrUpdateResponseInterface>(`api/relay`, relay)
      .pipe(catchError((error: any) => throwError(error)));
  }
}
