import { Injectable } from '@angular/core';
import { AuthServerProvider } from './authServerProvider.service';
import { MainService } from './Main.service';

@Injectable()
export class LoginService {
  constructor(
    private authServer: AuthServerProvider,
    private main: MainService
  ) {}

  login(credentials, callback?) {
    const cb = callback || function() {};

    return new Promise((resolve, reject) => {
      this.authServer.login(credentials).subscribe(
        data => {
          this.main.identity(true).catch(account => {
            resolve(data);
          });
          return cb();
        },
        error => {
          this.logout();
          reject(error);
          return cb(error);
        }
      );
    });
  }

  logout() {
    this.authServer.logout();
    this.main.authenticate(null);
  }
}
