import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

import * as fromSharedConstants from '../pickup-shared.constants';
import * as fromModel from '../model';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { ChangePasswordInterface } from '../../pickup-app/models';

@Injectable()
export class AuthServerProvider {
  constructor(
    private http: HttpClient,
    private localStorage: LocalStorageService,
    private sessionStorage: SessionStorageService
  ) {}

  getToken() {
    return (
      this.localStorage.retrieve(fromSharedConstants.PICKUP_TOKEN) ||
      this.sessionStorage.retrieve(fromSharedConstants.PICKUP_TOKEN)
    );
  }

  login(credentials: fromModel.Credential): Observable<any> {
    const data = {
      username: credentials.username,
      password: credentials.password,
      rememberMe: credentials.rememberMe,
    };

    return this.http
      .post<fromModel.LoginInterface>('/api/users/login', data)
      .pipe(
        tap((loginResponse) => {
          const token = loginResponse.token;
          if (!!token) {
            this.storeAuthenticationToken(token, credentials.rememberMe);
          }
        })
      );
  }

  storeAuthenticationToken(token: string, rememberMe: boolean) {
    if (rememberMe) {
      this.localStorage.store(fromSharedConstants.PICKUP_TOKEN, token);
    } else {
      this.sessionStorage.store(fromSharedConstants.PICKUP_TOKEN, token);
    }
  }

  logout() {
    // Should also make a call to the server logout api!!!
    this.localStorage.clear(fromSharedConstants.PICKUP_TOKEN);
    this.sessionStorage.clear(fromSharedConstants.PICKUP_TOKEN);
    return this.http.get('/api/users/logout');
  }

  changePassword(changePwdObj: ChangePasswordInterface): Observable<any> {
    return this.http
      .put(`api/users/password`, changePwdObj)
      .pipe(catchError((error: any) => throwError(error.json())));
  }
}
