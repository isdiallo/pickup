import { HttpMsg } from './httpMsg.model';
import { UserType } from './index';
import { RelayInterface } from './relay.model';

export interface Credential {
  username: string;
  password: string;
  rememberMe?: boolean;
}

export interface LoginInterface {
  success?: HttpMsg;
  error?: HttpMsg;
  token?: string;
}

export interface UserAccountResponseInterface {
  success?: HttpMsg;
  error?: HttpMsg;
  data?: UserAccountInterface;
}

export interface UserAccountInterface {
  _id: string;
  _type: UserType;
  updatedAt: string;
  createdAt: string;
  firstname: string;
  lastname: string;
  username: string;
  phone: string;
  mail: string;
  city: string;
  town: string;
  address: string;
  websiteName?: string;
  websiteUrl?: string;
  autoAccept?: boolean;
  sendNotif?: boolean;
  relayId?: RelayInterface;
  __v?: number;
  img?: string;
}

export interface UserUpdateInterface {
  autoAccept?: boolean;
  sendNotif?: boolean;
  websiteName?: string;
  websiteUrl?: string;
}

export interface RelayCreateOrUpdateResponseInterface {
  success?: HttpMsg;
  error?: HttpMsg;
  data?: RelayInterface;
}
