export interface RelayInterface {
  _id?: string;
  name: string;
  address: string;
  city: string;
  town: string;
  holiday: boolean;
  openingHours?: any;
}
