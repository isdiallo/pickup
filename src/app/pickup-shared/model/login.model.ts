import { RegisterUserType } from './index';

// export interface LoginInterface {}
export interface RegisterInterface {
  username: string;
  firstname: string;
  lastname: string;
  mail: string;
  phone: string;
  type: RegisterUserType;
  password: string;
  address: string;
  city: string;
  town: string;
}
