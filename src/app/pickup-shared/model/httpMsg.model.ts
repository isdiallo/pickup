export interface HttpMsg {
  description: string;
  message: string;
}
