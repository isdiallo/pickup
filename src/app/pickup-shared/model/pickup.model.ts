import { PickupUserInterface } from './index';
import { RelayInterface } from './relay.model';

export interface PickupInterface {
  _id: string;
  updatedAt: string;
  createdAt: string;
  toId?: PickupUserInterface;
  fromId: PickupUserInterface;
  cost?: number;
  endUserId: PickupUserInterface;
  assignation?: AssignationInterface;
  isFree?: boolean;
  history?: PickupHistoryInterface[]; // TODO: keep it here or not?
  packages: PickupPackageInterface[];
  state: PickupStateInterface;
  relayId?: RelayInterface;
}

export interface AssignationInterface {
  to: PickupUserInterface;
  by?: string;
  _id: string;
}

export interface PickupHistoryInterface {
  updatedAt?: string;
  createdAt?: string;
  who: PickupUserInterface;
  name: string;
  _id: string;
  state: PickupStateInterface;
}

export interface PickupStateInterface {
  name: string;
  owner?: PickupUserInterface;
}

export interface PickupPackageInterface {
  name?: string;
  quantity: number;
  type: string;
  height?: number;
  length?: number;
  weight?: number;
  width?: number;
  _id?: string;
  fragile?: boolean;
}
