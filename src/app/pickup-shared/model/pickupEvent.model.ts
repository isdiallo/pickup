export interface PickupEvent {
  type: string;
  message: string;
  description: string;
}
