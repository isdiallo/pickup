export interface StateInterface {
  name: string;
  parent?: StateInterface;
}
