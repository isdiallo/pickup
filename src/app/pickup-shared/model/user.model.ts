import { RelayInterface } from './relay.model';

export enum UserType {
  Agent = 'Agent',
  Merchant = 'Merchant',
  Relay = 'UserRelay',
  Admin = 'Admin'
}

export enum RegisterUserType {
  Agent = 'AGENT',
  Merchant = 'MERCHANT',
  Relay = 'RELAY'
}

// Pickup user interface
export interface PickupUserInterface {
  _id?: string;
  _type?: string;
  mail?: string;
  websiteName?: string;
  websiteUrl?: string;
  firstname: string;
  lastname: string;
  phone?: string;
  address?: string;
  city?: string;
  town?: string;
  relayId?: RelayInterface;
}

export interface PickupUserInfoInterface {
  firstname: string;
  lastname: string;
  phone: string;
  address?: string;
  city?: string;
  town?: string;
  websiteName?: string;
  relayId?: RelayAddressInfo;
}

export interface RelayAddressInfo {
  city: string;
  address: string;
  town: string;
  name: string;
}
