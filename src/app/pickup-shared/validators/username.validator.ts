import { ValidatorFn, FormControl } from '@angular/forms';
import { MAIL_REGEX, PHONE_REGEX } from '../pickup-shared.constants';

// An username should be either an email or a phone number
export function usernameValidator(): ValidatorFn {
  return (input: FormControl): { [key: string]: any } => {
    const valid = MAIL_REGEX.test(input.value) || PHONE_REGEX.test(input.value);
    return valid ? null : { invalidUsername: { value: input.valid } };
  };
}

export function emailValidator(): ValidatorFn {
  return (input: FormControl): { [key: string]: boolean } => {
    if (!input) {
      return null;
    }
    return MAIL_REGEX.test(input.value) ? null : { invalidEmail: true };
  };
}

export function phoneValidator(): ValidatorFn {
  return (input: FormControl): { [key: string]: boolean } => {
    if (!input) {
      return null;
    }
    return PHONE_REGEX.test(input.value) ? null : { invalidPhone: true };
  };
}
