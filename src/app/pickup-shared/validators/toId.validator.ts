import { ValidatorFn, FormControl } from '@angular/forms';

export function toIdValidator() {
  return (input: FormControl): { [key: string]: boolean } => {
    // valid if useRelay is not toggled
    if (
      !input.root ||
      !input.root.get('useRelay') ||
      !input.root.get('useRelay').value
    ) {
      return null;
    }
    return input.root.get('toId').value ? null : { toIdRequired: true };
  };
}
