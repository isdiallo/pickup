import { ValidatorFn, FormControl } from '@angular/forms';

/**
 * validate that 2 givent inputs are equals
 * @param from first input item
 * @param against second input item
 */

export function duplicateInputValidator(
  from: string,
  against: string
): ValidatorFn {
  return (input: FormControl): { [key: string]: boolean } => {
    if (
      !input.root.get(from) ||
      !input.root.get(against) ||
      !input.root.get(from).value ||
      !input.root.get(against).value
    ) {
      return null;
    }
    return input.root.get(from).value === input.root.get(against).value
      ? null
      : { noMatch: true };
  };
}
