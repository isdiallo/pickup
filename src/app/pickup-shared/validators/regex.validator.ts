import { ValidatorFn, FormControl } from '@angular/forms';

// A Regexp validator
export function regexpValidator(regexp: RegExp): ValidatorFn {
  return (input: FormControl): { [key: string]: any } => {
    const valid = regexp.test(input.value);
    return valid ? null : { forbidden: { value: input.valid } };
  };
}
