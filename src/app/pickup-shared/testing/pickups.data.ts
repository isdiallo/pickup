import { PickupInterface } from '../model/pickup.model';
import { CreatePickupResponseInterface } from '../../pickup-app/models';

export const PICKUPS: PickupInterface[] = [
  {
    _id: '5a86067442de7a002093fe5f',
    updatedAt: '2018-02-15T22:16:10.150Z',
    fromId: {
      _type: 'Merchant',
      firstname: 'Merchant',
      lastname: 'Test',
      phone: '036524529',
      mail: 'agent@user.com',
      websiteName: 'relay website',
      websiteUrl: 'test.com',
      city: 'Conakry',
      town: 'Ratoma',
      address: 'No Where'
    },
    endUserId: {
      firstname: 'end User first',
      lastname: 'end User last',
      phone: '123456789',
      address: 'No Where Address',
      city: 'End User city',
      town: 'End User town'
    },
    cost: 15000,
    createdAt: '2018-02-15T22:15:16.795Z',
    isFree: false,
    history: [
      {
        updatedAt: '2018-02-15T22:16:10.149Z',
        createdAt: '2018-02-15T22:16:10.149Z',
        name: 'CREATED',
        who: {
          _id: '5a820c8d2bec7921fca9f814',
          _type: 'Merchant',
          firstname: 'Merchant',
          lastname: 'Test'
        },
        _id: '5a86067442de7a002093fe60',
        state: {
          owner: {
            _id: '5a820c8d2bec7921fca9f814',
            _type: 'Merchant',
            firstname: 'Merchant',
            lastname: 'Test'
          },
          name: 'CREATED'
        }
      },
      {
        updatedAt: '2018-02-15T22:16:10.150Z',
        createdAt: '2018-02-15T22:16:10.150Z',
        who: {
          _id: '5a820c8d2bec7921fca9f814',
          _type: 'Merchant',
          firstname: 'Merchant',
          lastname: 'Test'
        },
        name: 'UPDATED',
        _id: '5a8606aa42de7a002093fe98',
        state: {
          name: 'VALIDATED',
          owner: {
            _id: '5a820c8d2bec7921fca9f814',
            _type: 'Merchant',
            firstname: 'Merchant',
            lastname: 'Test'
          }
        }
      }
    ],
    packages: [
      {
        quantity: 1,
        type: 'XL',
        weight: 14,
        width: 13,
        length: 12,
        height: 11,
        _id: '5a86067442de7a002093fe64',
        fragile: false
      },
      {
        quantity: 0,
        type: 'L',
        weight: 14,
        width: 13,
        length: 12,
        height: 11,
        _id: '5a86067442de7a002093fe63',
        fragile: false
      },
      {
        quantity: 0,
        type: 'M',
        weight: 14,
        width: 13,
        length: 12,
        height: 11,
        _id: '5a86067442de7a002093fe62',
        fragile: false
      },
      {
        quantity: 0,
        type: 'S',
        weight: 14,
        width: 13,
        length: 12,
        height: 11,
        _id: '5a86067442de7a002093fe61',
        fragile: false
      }
    ],
    state: {
      owner: {
        _type: 'Merchant',
        firstname: 'Merchant',
        lastname: 'Test',
        phone: '036524529'
      },
      name: 'VALIDATED'
    }
  },
  {
    _id: '5a8dccc3aec7ea08284104a2',
    updatedAt: '2018-02-21T19:47:19.894Z',
    fromId: {
      _type: 'Merchant',
      firstname: 'Merchant',
      lastname: 'Test',
      phone: '036524529',
      mail: 'agent@user.com',
      websiteName: 'relay website',
      websiteUrl: 'test.com',
      city: 'Conakry',
      town: 'Ratoma',
      address: 'No Where'
    },
    endUserId: {
      firstname: 'end User first',
      lastname: 'end User last',
      phone: '123456789',
      address: 'No Where Address',
      city: 'End User city',
      town: 'End User town'
    },
    cost: 15000,
    createdAt: '2018-02-21T19:47:15.223Z',
    isFree: false,
    history: [
      {
        updatedAt: '2018-02-21T19:47:19.893Z',
        createdAt: '2018-02-21T19:47:19.893Z',
        name: 'CREATED',
        who: {
          _id: '5a820c8d2bec7921fca9f814',
          _type: 'Merchant',
          firstname: 'Merchant',
          lastname: 'Test'
        },
        _id: '5a8dccc3aec7ea08284104a3',
        state: {
          owner: {
            _id: '5a820c8d2bec7921fca9f814',
            _type: 'Merchant',
            firstname: 'Merchant',
            lastname: 'Test'
          },
          name: 'CREATED'
        }
      },
      {
        updatedAt: '2018-02-21T19:47:19.894Z',
        createdAt: '2018-02-21T19:47:19.894Z',
        who: {
          _id: '5a820c8d2bec7921fca9f814',
          _type: 'Merchant',
          firstname: 'Merchant',
          lastname: 'Test'
        },
        name: 'UPDATED',
        _id: '5a8dccc7aec7ea08284104ac',
        state: {
          name: 'VALIDATED',
          owner: {
            _id: '5a820c8d2bec7921fca9f814',
            _type: 'Merchant',
            firstname: 'Merchant',
            lastname: 'Test'
          }
        }
      }
    ],
    packages: [
      {
        quantity: 1,
        type: 'XL',
        weight: 14,
        width: 13,
        length: 12,
        height: 11,
        _id: '5a8dccc3aec7ea08284104a7',
        fragile: false
      },
      {
        quantity: 0,
        type: 'L',
        weight: 14,
        width: 13,
        length: 12,
        height: 11,
        _id: '5a8dccc3aec7ea08284104a6',
        fragile: false
      },
      {
        quantity: 0,
        type: 'M',
        weight: 14,
        width: 13,
        length: 12,
        height: 11,
        _id: '5a8dccc3aec7ea08284104a5',
        fragile: false
      },
      {
        quantity: 0,
        type: 'S',
        weight: 14,
        width: 13,
        length: 12,
        height: 11,
        _id: '5a8dccc3aec7ea08284104a4',
        fragile: false
      }
    ],
    state: {
      owner: {
        _type: 'Merchant',
        firstname: 'Merchant',
        lastname: 'Test',
        phone: '036524529'
      },
      name: 'VALIDATED'
    }
  }
];

export const PickupsWithRelay: PickupInterface = {
  _id: '5ae75df70b6a041074e93b0c',
  updatedAt: '2018-04-30T18:18:31.804Z',
  toId: {
    _type: 'UserRelay',
    firstname: 'Test Relay',
    lastname: 'Test Last',
    phone: '145214587',
    relayId: {
      name: 'Relay test',
      address: 'test relay address',
      city: 'Kamsar',
      town: 'Kamsar',
      holiday: false,
      openingHours: []
    }
  },
  fromId: {
    _type: 'Merchant',
    firstname: 'Merchant test',
    lastname: 'Merchant Last',
    address: 'merchant add',
    city: 'merchant city',
    town: 'merchant town',
    phone: '147854125',
    mail: 'merchant@test.fr',
    websiteName: 'test name',
    websiteUrl: 'http://fddfsdf.com'
  },
  endUserId: {
    firstname: 'Test',
    lastname: 'Last',
    phone: '452102547',
    address: 'Adress',
    city: 'City',
    town: 'Town'
  },
  cost: 15000,
  createdAt: '2018-04-30T18:18:31.804Z',
  isFree: false,
  history: [
    {
      name: 'CREATED',
      who: {
        _id: '5a8f373425a72d2eb443743e',
        _type: 'Merchant',
        firstname: 'Merchant test',
        lastname: 'Merchant Last'
      },
      _id: '5ae75df70b6a041074e93b0d',
      state: {
        owner: {
          _id: '5a8f373425a72d2eb443743e',
          _type: 'Merchant',
          firstname: 'Merchant test',
          lastname: 'Merchant Last'
        },
        name: 'CREATED'
      }
    }
  ],
  packages: [
    {
      quantity: 0,
      type: 'XL',
      weight: 14,
      width: 13,
      length: 12,
      height: 11,
      _id: '5ae75df70b6a041074e93b11',
      fragile: false
    },
    {
      quantity: 0,
      type: 'L',
      weight: 14,
      width: 13,
      length: 12,
      height: 11,
      _id: '5ae75df70b6a041074e93b10',
      fragile: false
    },
    {
      quantity: 2,
      type: 'M',
      weight: 14,
      width: 13,
      length: 12,
      height: 11,
      _id: '5ae75df70b6a041074e93b0f',
      fragile: false
    },
    {
      quantity: 0,
      type: 'S',
      weight: 14,
      width: 13,
      length: 12,
      height: 11,
      _id: '5ae75df70b6a041074e93b0e',
      fragile: false
    }
  ],
  state: {
    owner: {
      _type: 'Merchant',
      firstname: 'Merchant test',
      lastname: 'Merchant Last',
      phone: '147854125'
    },
    name: 'CREATED'
  }
};

export const PickupToBeValidated: PickupInterface = {
  _id: '5a985ad9d546de0020d8b940',
  updatedAt: '2018-03-01T19:57:33.848Z',
  fromId: {
    _type: 'Merchant',
    firstname: 'Merchant test',
    lastname: 'Merchant Last',
    address: 'merchant add',
    city: 'merchant city',
    town: 'merchant town',
    phone: '147854125',
    mail: 'merchant@test.fr',
    websiteName: 'test name',
    websiteUrl: 'http://fddfsdf.com'
  },
  endUserId: {
    firstname: 'end User first',
    lastname: 'end User last',
    phone: '123456789',
    address: 'No Where Address',
    city: 'End User city',
    town: 'End User town'
  },
  cost: 15000,
  createdAt: '2018-03-01T19:56:09.080Z',
  isFree: false,
  history: [
    {
      updatedAt: '2018-03-01T19:57:33.846Z',
      createdAt: '2018-03-01T19:57:33.846Z',
      name: 'CREATED',
      who: {
        _id: '5a8f373425a72d2eb443743e',
        _type: 'Merchant',
        firstname: 'Merchant test',
        lastname: 'Merchant Last'
      },
      _id: '5a985ad9d546de0020d8b941',
      state: {
        owner: {
          _id: '5a8f373425a72d2eb443743e',
          _type: 'Merchant',
          firstname: 'Merchant test',
          lastname: 'Merchant Last'
        },
        name: 'CREATED'
      }
    },
    {
      updatedAt: '2018-03-01T19:57:33.847Z',
      createdAt: '2018-03-01T19:57:33.847Z',
      who: {
        _id: '5a8f373425a72d2eb443743e',
        _type: 'Merchant',
        firstname: 'Merchant test',
        lastname: 'Merchant Last'
      },
      name: 'UPDATED',
      _id: '5a985b2dd546de0020d8b959',
      state: {
        name: 'VALIDATED',
        owner: {
          _id: '5a8f373425a72d2eb443743e',
          _type: 'Merchant',
          firstname: 'Merchant test',
          lastname: 'Merchant Last'
        }
      }
    }
  ],
  packages: [
    {
      quantity: 1,
      type: 'XL',
      weight: 14,
      width: 13,
      length: 12,
      height: 11,
      _id: '5a985ad9d546de0020d8b945',
      fragile: true
    },
    {
      quantity: 0,
      type: 'L',
      weight: 14,
      width: 13,
      length: 12,
      height: 11,
      _id: '5a985ad9d546de0020d8b944',
      fragile: false
    },
    {
      quantity: 0,
      type: 'M',
      weight: 14,
      width: 13,
      length: 12,
      height: 11,
      _id: '5a985ad9d546de0020d8b943',
      fragile: false
    },
    {
      quantity: 0,
      type: 'S',
      weight: 14,
      width: 13,
      length: 12,
      height: 11,
      _id: '5a985ad9d546de0020d8b942',
      fragile: false
    }
  ],
  state: {
    owner: {
      _type: 'Merchant',
      firstname: 'Merchant test',
      lastname: 'Merchant Last',
      phone: '147854125'
    },
    name: 'VALIDATED'
  }
};

export const PickupsWithoutRelay: PickupInterface = {
  _id: '5ae75d2f0b6a041074e93aff',
  updatedAt: '2018-04-30T18:15:11.003Z',
  fromId: {
    _type: 'Merchant',
    firstname: 'Merchant test',
    lastname: 'Merchant Last',
    address: 'merchant add',
    city: 'merchant city',
    town: 'merchant town',
    phone: '147854125',
    mail: 'merchant@test.fr',
    websiteName: 'test name',
    websiteUrl: 'http://fddfsdf.com'
  },
  endUserId: {
    firstname: 'Test',
    lastname: 'Last',
    phone: '145214785',
    address: 'Adress test',
    city: 'City ',
    town: 'Town'
  },
  cost: 15000,
  createdAt: '2018-04-30T18:15:11.003Z',
  isFree: false,
  history: [
    {
      name: 'CREATED',
      who: {
        _id: '5a8f373425a72d2eb443743e',
        _type: 'Merchant',
        firstname: 'Merchant test',
        lastname: 'Merchant Last'
      },
      _id: '5ae75d2f0b6a041074e93b00',
      state: {
        owner: {
          _id: '5a8f373425a72d2eb443743e',
          _type: 'Merchant',
          firstname: 'Merchant test',
          lastname: 'Merchant Last'
        },
        name: 'CREATED'
      }
    }
  ],
  packages: [
    {
      quantity: 0,
      type: 'XL',
      weight: 14,
      width: 13,
      length: 12,
      height: 11,
      _id: '5ae75d2f0b6a041074e93b04',
      fragile: false
    },
    {
      quantity: 2,
      type: 'L',
      weight: 14,
      width: 13,
      length: 12,
      height: 11,
      _id: '5ae75d2f0b6a041074e93b03',
      fragile: false
    },
    {
      quantity: 0,
      type: 'M',
      weight: 14,
      width: 13,
      length: 12,
      height: 11,
      _id: '5ae75d2f0b6a041074e93b02',
      fragile: false
    },
    {
      quantity: 0,
      type: 'S',
      weight: 14,
      width: 13,
      length: 12,
      height: 11,
      _id: '5ae75d2f0b6a041074e93b01',
      fragile: false
    }
  ],
  state: {
    owner: {
      _type: 'Merchant',
      firstname: 'Merchant test',
      lastname: 'Merchant Last',
      phone: '147854125'
    },
    name: 'CREATED'
  }
};

export const PickupCreationResponse: CreatePickupResponseInterface = {
  success: {
    message: 'pickup.creation.success',
    description: 'Pickup created successfuly.'
  },
  data: {
    _id: '5af340c90984340fe845611c',
    updatedAt: '2018-05-09T18:41:13.916Z',
    fromId: {
      _type: 'Merchant',
      firstname: 'Merchant test',
      lastname: 'Merchant Last',
      address: 'merchant add',
      city: 'merchant city',
      town: 'merchant town',
      phone: '147854125',
      mail: 'merchant@test.fr',
      websiteName: 'test name',
      websiteUrl: 'http://fddfsdf.com'
    },
    endUserId: {
      firstname: 'test',
      lastname: 'tsdfjk',
      phone: '145874521',
      address: 'Adress test',
      city: 'Toulouse',
      town: 'Town'
    },
    cost: 15000,
    createdAt: '2018-05-09T18:41:13.916Z',
    isFree: false,
    history: [
      {
        name: 'CREATED',
        who: {
          _id: '5a8f373425a72d2eb443743e',
          _type: 'Merchant',
          firstname: 'Merchant test',
          lastname: 'Merchant Last'
        },
        _id: '5af340c90984340fe845611d',
        state: {
          owner: {
            _id: '5a8f373425a72d2eb443743e',
            _type: 'Merchant',
            firstname: 'Merchant test',
            lastname: 'Merchant Last'
          },
          name: 'CREATED'
        }
      }
    ],
    packages: [
      {
        quantity: 4,
        type: 'XL',
        weight: 14,
        width: 13,
        length: 12,
        height: 11,
        _id: '5af340c90984340fe8456121',
        fragile: false
      },
      {
        quantity: 0,
        type: 'L',
        weight: 14,
        width: 13,
        length: 12,
        height: 11,
        _id: '5af340c90984340fe8456120',
        fragile: false
      },
      {
        quantity: 0,
        type: 'M',
        weight: 14,
        width: 13,
        length: 12,
        height: 11,
        _id: '5af340c90984340fe845611f',
        fragile: false
      },
      {
        quantity: 0,
        type: 'S',
        weight: 14,
        width: 13,
        length: 12,
        height: 11,
        _id: '5af340c90984340fe845611e',
        fragile: false
      }
    ],
    state: {
      owner: {
        _type: 'Merchant',
        firstname: 'Merchant test',
        lastname: 'Merchant Last',
        phone: '147854125'
      },
      name: 'CREATED'
    }
  }
};
