import {
  ActivatedRouteSnapshot,
  Params,
  ParamMap,
  convertToParamMap
} from '@angular/router';
import { ReplaySubject } from 'rxjs/ReplaySubject';

export class ActivatedRouteStub {
  private subject = new ReplaySubject<ParamMap>();

  constructor(initialParams?: Params) {
    this.setParamMap(initialParams);
  }

  /** The mock paramMap observable */
  readonly snapshot = {
    paramMap: this.subject.asObservable()
  };

  /** Set the paramMap observables's next value */
  setParamMap(params?: Params) {
    this.subject.next(convertToParamMap(params));
  }
}
