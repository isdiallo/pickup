import { MessageInterface } from '../../pickup-app/models';

export const MSGs: MessageInterface[] = [
  {
    content: 'Message 1',
    createdAt: '2018-02-15T22:16:10.150Z',
    from: { firstname: 'Test first', lastname: 'Test last' }
  },
  {
    content: 'Message 2',
    createdAt: '2018-02-15T22:16:10.150Z',
    from: { firstname: 'Test first', lastname: 'Test last' }
  },
  {
    content: 'Message 3',
    createdAt: '2018-02-15T22:16:10.150Z',
    from: { firstname: 'Test first', lastname: 'Test last' }
  }
];
