import { ComponentFixture } from '@angular/core/testing';

export class Page<T> {
  // getter properties wait to query the DOM until called.
  get buttons() {
    return this.queryAll<HTMLButtonElement>('button');
  }

  get componentDe() {
    return this.fixture.debugElement;
  }

  constructor(private fixture: ComponentFixture<T>) {
    const component = fixture.componentInstance;
  }

  //// query helpers ////
  query<B>(selector: string): B {
    return this.componentDe.nativeElement.querySelector(selector);
  }

  queryAll<A>(selector: string): A[] {
    return this.componentDe.nativeElement.querySelectorAll(selector);
  }
}
