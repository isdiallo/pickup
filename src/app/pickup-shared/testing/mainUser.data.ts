import { RegisterInterface, RegisterUserType, Credential } from '../model';
import { RelayInterface } from '../model/relay.model';

export const RegisterInfo: RegisterInterface = {
  username: '123654789',
  firstname: 'Test First',
  lastname: 'Test Last',
  mail: 'test@test.fr',
  password: 'pwdtest',
  city: 'City',
  town: 'Town',
  address: 'Address',
  type: RegisterUserType.Merchant,
  phone: '123654789'
};

export const RelayInfo: RelayInterface = {
  name: 'Relay Test',
  address: 'Relay address',
  city: 'Relay city',
  town: 'Relay town',
  holiday: true
};

export const LoginData: Credential = {
  username: 'Test',
  password: 'testpwd'
};

export const ChangePwdData = { newPwd: 'newPwd', currentPwd: 'currentPwd' };
