import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appHasAuthority]'
})
export class HasAuthorityUserRelayStubDirective {
  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {}
  @Input()
  set appHasAuthority(authorities: string) {
    this.viewContainer.clear();
    if (authorities.includes('UserRelay')) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }
  }
}
