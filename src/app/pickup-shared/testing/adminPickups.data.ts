import { PickupLoadInterface } from '../../pickup-app/models';

export const adminData: PickupLoadInterface = {
  success: {
    message: 'pickup.fetch.all.success',
    description: 'You have fetched successfuly your pickups.'
  },
  data: [
    {
      _id: '5a86067442de7a002093fe5f',
      updatedAt: '2018-02-15T22:16:10.150Z',
      fromId: {
        _type: 'Merchant',
        firstname: 'Merchant',
        lastname: 'Test',
        phone: '036524529',
        mail: 'agent@user.com',
        websiteName: 'relay website',
        websiteUrl: 'test.com',
        city: 'Conakry',
        town: 'Ratoma',
        address: 'No Where'
      },
      endUserId: {
        firstname: 'end User first',
        lastname: 'end User last',
        phone: '123456789',
        address: 'No Where Address',
        city: 'End User city',
        town: 'End User town'
      },
      cost: 15000,
      createdAt: '2018-02-15T22:15:16.795Z',
      isFree: false,
      history: [
        {
          updatedAt: '2018-02-15T22:16:10.149Z',
          createdAt: '2018-02-15T22:16:10.149Z',
          name: 'CREATED',
          who: {
            _id: '5a820c8d2bec7921fca9f814',
            _type: 'Merchant',
            firstname: 'Merchant',
            lastname: 'Test'
          },
          _id: '5a86067442de7a002093fe60',
          state: {
            owner: {
              _id: '5a820c8d2bec7921fca9f814',
              _type: 'Merchant',
              firstname: 'Merchant',
              lastname: 'Test'
            },
            name: 'CREATED'
          }
        },
        {
          updatedAt: '2018-02-15T22:16:10.150Z',
          createdAt: '2018-02-15T22:16:10.150Z',
          who: {
            _id: '5a820c8d2bec7921fca9f814',
            _type: 'Merchant',
            firstname: 'Merchant',
            lastname: 'Test'
          },
          name: 'UPDATED',
          _id: '5a8606aa42de7a002093fe98',
          state: {
            name: 'VALIDATED',
            owner: {
              _id: '5a820c8d2bec7921fca9f814',
              _type: 'Merchant',
              firstname: 'Merchant',
              lastname: 'Test'
            }
          }
        }
      ],
      packages: [
        {
          quantity: 1,
          type: 'XL',
          weight: 14,
          width: 13,
          length: 12,
          height: 11,
          _id: '5a86067442de7a002093fe64',
          fragile: false
        },
        {
          quantity: 0,
          type: 'L',
          weight: 14,
          width: 13,
          length: 12,
          height: 11,
          _id: '5a86067442de7a002093fe63',
          fragile: false
        },
        {
          quantity: 0,
          type: 'M',
          weight: 14,
          width: 13,
          length: 12,
          height: 11,
          _id: '5a86067442de7a002093fe62',
          fragile: false
        },
        {
          quantity: 0,
          type: 'S',
          weight: 14,
          width: 13,
          length: 12,
          height: 11,
          _id: '5a86067442de7a002093fe61',
          fragile: false
        }
      ],
      state: {
        owner: {
          _type: 'Merchant',
          firstname: 'Merchant',
          lastname: 'Test',
          phone: '036524529'
        },
        name: 'VALIDATED'
      }
    },
    {
      _id: '5a8dccc3aec7ea08284104a2',
      updatedAt: '2018-02-21T19:47:19.894Z',
      fromId: {
        _type: 'Merchant',
        firstname: 'Merchant',
        lastname: 'Test',
        phone: '036524529',
        mail: 'agent@user.com',
        websiteName: 'relay website',
        websiteUrl: 'test.com',
        city: 'Conakry',
        town: 'Ratoma',
        address: 'No Where'
      },
      endUserId: {
        firstname: 'end User first',
        lastname: 'end User last',
        phone: '123456789',
        address: 'No Where Address',
        city: 'End User city',
        town: 'End User town'
      },
      cost: 15000,
      createdAt: '2018-02-21T19:47:15.223Z',
      isFree: false,
      history: [
        {
          updatedAt: '2018-02-21T19:47:19.893Z',
          createdAt: '2018-02-21T19:47:19.893Z',
          name: 'CREATED',
          who: {
            _id: '5a820c8d2bec7921fca9f814',
            _type: 'Merchant',
            firstname: 'Merchant',
            lastname: 'Test'
          },
          _id: '5a8dccc3aec7ea08284104a3',
          state: {
            owner: {
              _id: '5a820c8d2bec7921fca9f814',
              _type: 'Merchant',
              firstname: 'Merchant',
              lastname: 'Test'
            },
            name: 'CREATED'
          }
        },
        {
          updatedAt: '2018-02-21T19:47:19.894Z',
          createdAt: '2018-02-21T19:47:19.894Z',
          who: {
            _id: '5a820c8d2bec7921fca9f814',
            _type: 'Merchant',
            firstname: 'Merchant',
            lastname: 'Test'
          },
          name: 'UPDATED',
          _id: '5a8dccc7aec7ea08284104ac',
          state: {
            name: 'VALIDATED',
            owner: {
              _id: '5a820c8d2bec7921fca9f814',
              _type: 'Merchant',
              firstname: 'Merchant',
              lastname: 'Test'
            }
          }
        }
      ],
      packages: [
        {
          quantity: 1,
          type: 'XL',
          weight: 14,
          width: 13,
          length: 12,
          height: 11,
          _id: '5a8dccc3aec7ea08284104a7',
          fragile: false
        },
        {
          quantity: 0,
          type: 'L',
          weight: 14,
          width: 13,
          length: 12,
          height: 11,
          _id: '5a8dccc3aec7ea08284104a6',
          fragile: false
        },
        {
          quantity: 0,
          type: 'M',
          weight: 14,
          width: 13,
          length: 12,
          height: 11,
          _id: '5a8dccc3aec7ea08284104a5',
          fragile: false
        },
        {
          quantity: 0,
          type: 'S',
          weight: 14,
          width: 13,
          length: 12,
          height: 11,
          _id: '5a8dccc3aec7ea08284104a4',
          fragile: false
        }
      ],
      state: {
        owner: {
          _type: 'Merchant',
          firstname: 'Merchant',
          lastname: 'Test',
          phone: '036524529'
        },
        name: 'VALIDATED'
      }
    }
  ]
};
