import { UserInterface, UserRelayInterface } from '../../pickup-app/models';
import {
  PickupUserInfoInterface,
  UserAccountInterface,
  UserType
} from '../model';

export const AGENTS: UserInterface[] = [
  {
    firstname: 'Agent 1 first',
    lastname: 'Agent 1 last',
    phone: '123654123',
    address: 'test address',
    city: 'test city',
    town: 'test town'
  },
  {
    firstname: 'Agent 2 first',
    lastname: 'Agent 2 last',
    phone: '223654123',
    address: 'test address',
    city: 'test city',
    town: 'test town'
  },
  {
    firstname: 'Agent 3 first',
    lastname: 'Agent 3 last',
    phone: '323654123',
    address: 'test address',
    city: 'test city',
    town: 'test town'
  }
];

export const USERSRELAY: UserRelayInterface[] = [
  {
    firstname: 'Agent 1 first',
    lastname: 'Agent 1 last',
    phone: '123654123',
    address: 'test address',
    city: 'test city',
    town: 'test town',
    relayId: {
      name: 'Relay 1',
      address: 'Address 1',
      city: 'city 1',
      town: 'town 1',
      holiday: false
    }
  },
  {
    firstname: 'Agent 2 first',
    lastname: 'Agent 2 last',
    phone: '223654123',
    address: 'test address',
    city: 'test city',
    town: 'test town',
    relayId: {
      name: 'Relay 2',
      address: 'Address 2',
      city: 'city 2',
      town: 'town 2',
      holiday: false
    }
  },
  {
    firstname: 'Agent 3 first',
    lastname: 'Agent 3 last',
    phone: '323654123',
    address: 'test address',
    city: 'test city',
    town: 'test town',
    relayId: {
      name: 'Relay 3',
      address: 'Address 3',
      city: 'city 3',
      town: 'town 3',
      holiday: false
    }
  }
];

export const UserMerchantInfo: PickupUserInfoInterface = {
  firstname: 'Test Merch',
  lastname: 'Last Merch',
  phone: '125412547',
  address: 'Merch address',
  city: 'Merch city',
  town: 'Merch town',
  websiteName: 'www.merchant.com'
};

export const UserRelayInfo: PickupUserInfoInterface = {
  firstname: 'Test User R',
  lastname: 'Last User R',
  phone: '125412547',
  address: 'User R address',
  city: 'User R city',
  town: 'User R town',
  relayId: {
    name: 'Relay Test',
    city: 'Relay city',
    address: 'Relay address',
    town: 'Relay town'
  }
};

export const UserAgentInfo: PickupUserInfoInterface | UserInterface = {
  firstname: 'Test Agent',
  lastname: 'Last Agent',
  phone: '125412547',
  address: 'Agent address',
  city: 'Agent city',
  town: 'Agent town'
};

export const UserAgentAccount: UserInterface = {
  _id: '5af1f58f1a20b800208d304e',
  firstname: 'Agent First',
  lastname: 'Agent Last',
  address: 'test Address',
  city: 'Test city',
  town: 'Test Town',
  phone: '145236547'
};

export const MerchantUserAccount: UserAccountInterface = {
  _id: '1212sdffsdf',
  _type: UserType.Merchant,
  updatedAt: '',
  createdAt: '',
  username: 'merchant@test.fr',
  mail: 'merchant@test.fr',
  firstname: 'Test Merch',
  lastname: 'Last Merch',
  phone: '125412547',
  address: 'Merch address',
  city: 'Merch city',
  town: 'Merch town',
  websiteName: 'www.merchant.com'
};
