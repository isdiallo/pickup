import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appHasAuthority]'
})
export class HasAuthorityMerchantStubDirective {
  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {}
  @Input()
  set appHasAuthority(authorities: string) {
    this.viewContainer.clear();
    if (authorities.includes('Merchant')) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }
  }
}
