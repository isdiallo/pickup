import { Injectable } from '@angular/core';

function _window(): any {
  return window;
}

@Injectable()
export class WindowRefService {
  getNativeWindow(): any {
    return _window();
  }
}
