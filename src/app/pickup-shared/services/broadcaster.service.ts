import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { PickupEvent } from '../model/pickupEvent.model';

@Injectable()
export class BroadcasterService {
  _eventBus: Subject<PickupEvent>;

  constructor() {
    this._eventBus = new Subject<PickupEvent>();
  }

  broadcast(event: PickupEvent) {
    this._eventBus.next(event);
  }
}
