import { WindowRefService } from './window-ref.service';
import { BroadcasterService } from './broadcaster.service';

export const sharedServices: any[] = [WindowRefService, BroadcasterService];

export * from './window-ref.service';
export * from './broadcaster.service';
