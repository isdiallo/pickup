import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';

import { Store } from '@ngrx/store';

import * as fromSharedStore from '../store';
import { UserType } from '../model/index';
import { map, switchMap, tap, take, filter } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { MainService } from '../auth/index';

@Injectable()
export class RouteAccessGuard implements CanActivate, CanActivateChild {
  constructor(
    private sharedStore: Store<fromSharedStore.AppSharedState>,
    private router: Router,
    private mainUserService: MainService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    const { url } = state;
    const { authorities } = route.data;

    return this.checkIsLoaded().pipe(
      switchMap((loaded) => {
        if (loaded) {
          return this.checkLogin(authorities, url);
        } else {
          this.sharedStore.dispatch(new fromSharedStore.SaveUrlAndLogin(url));
          return of(false);
        }
      })
    );
  }

  canActivateChild(route: ActivatedRouteSnapshot) {
    const { authorities } = route.data;
    return this.sharedStore.select(fromSharedStore.getSharedState).pipe(
      map((authState) => {
        return (
          authState &&
          authState.userInfo &&
          authState.userInfo._type &&
          this.mainUserService.hasAuthority(
            authorities,
            authState.userInfo._type
          )
        );
      }),
      take(1)
    );
  }

  checkLogin(authorities: any[], url: string): Observable<boolean> {
    return this.sharedStore.select(fromSharedStore.getSharedState).pipe(
      map((authState) => {
        if (!authState.isAuthenticated) {
          this.sharedStore.dispatch(new fromSharedStore.SaveUrlAndLogin(url));
          return false;
        }
        // User is authenticated, then check if he has the authority
        return this.mainUserService.hasAuthority(
          authorities,
          authState.userInfo._type
        );
      }),
      take(1)
    );
  }

  // checkUserAuthorities(authorities: any[], userAuth: UserType) {
  //   return (
  //     !authorities || authorities.length === 0 || authorities.includes(userAuth)
  //   );
  // }

  checkIsLoaded(): Observable<boolean> {
    return this.sharedStore.select(fromSharedStore.getUserInfoLoaded).pipe(
      tap((loaded) => {
        if (!loaded) {
          // Identity never been call before, get identity
          this.sharedStore.dispatch(new fromSharedStore.GetIdentity());
        }
      }),
      filter((loaded) => loaded),
      take(1)
    );
  }
}
