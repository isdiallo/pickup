import { createSelector } from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromMainUser from '../reducers/MainUser.reducer';

export const getSharedState = createSelector(
  fromFeature.getAppSharedState,
  (state: fromFeature.AppSharedState) => state.mainUser
);

export const getUserInfo = createSelector(
  getSharedState,
  (state: fromMainUser.MainUserState) => state.userInfo
);

export const isAuthenticated = createSelector(
  getSharedState,
  fromMainUser.isAuthenticated
);

export const getUserInfoLoading = createSelector(
  getSharedState,
  fromMainUser.getUserInfoLoading
);

export const getUserInfoLoaded = createSelector(
  getSharedState,
  fromMainUser.getUserInfoLoaded
);

export const getUserImg = createSelector(
  getSharedState,
  fromMainUser.getUserImg
);

export const getUserType = createSelector(
  getUserInfo,
  userInfo => (!!userInfo ? userInfo._type : undefined)
);

export const getUserId = createSelector(
  getUserInfo,
  userInfo => (!!userInfo ? userInfo._id : undefined)
);

export const getRedirectedUrl = createSelector(
  getSharedState,
  fromMainUser.getRedirectedUrl
);

export const isIdentityResolved = createSelector(
  getSharedState,
  fromMainUser.isIdentityResolved
);
