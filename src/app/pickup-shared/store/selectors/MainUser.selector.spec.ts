import { TestBed } from '@angular/core/testing';
import { StoreModule, combineReducers, Store } from '@ngrx/store';
import * as fromReducers from '../reducers';
import * as fromRoot from '../../../store';
import * as fromMainUserState from '../reducers/MainUser.reducer';
import * as fromSelectors from './MainUser.selector';
import * as fromActions from '../actions';
import { MerchantUserAccount } from '../../testing/users.data';

describe('MainUser Selector', () => {
  let store: Store<fromReducers.AppSharedState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          ...fromRoot.reducers,
          appShared: combineReducers(fromReducers.reducers)
        })
      ]
    });

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  describe('getSharedState ==> mainUser', () => {
    it('should return mainUser state', () => {
      const { initialState } = fromMainUserState;
      let result;
      store
        .select(fromSelectors.getSharedState)
        .subscribe(value => (result = value));
      expect(result).toEqual(initialState);

      store.dispatch(
        new fromActions.GetIdentitySuccess({
          success: { description: '', message: '' },
          data: MerchantUserAccount
        })
      );

      expect(result).toEqual({
        userInfo: MerchantUserAccount,
        isAuthenticated: true,
        loading: false,
        loaded: true,
        redirectUrl: '/app'
      });
    });
  });

  describe('getUserInfo selector', () => {
    it('should return userInfo', () => {
      let result;
      store
        .select(fromSelectors.getUserInfo)
        .subscribe(value => (result = value));
      expect(result).toEqual(undefined);

      store.dispatch(
        new fromActions.GetIdentitySuccess({
          success: { description: '', message: '' },
          data: MerchantUserAccount
        })
      );
      expect(result).toEqual(MerchantUserAccount);
    });
  });

  describe('isAuthenticated selector', () => {
    it('should return whether or not the user is authenticated', () => {
      let result;
      store
        .select(fromSelectors.isAuthenticated)
        .subscribe(value => (result = value));
      expect(result).toEqual(false);

      store.dispatch(
        new fromActions.GetIdentitySuccess({
          success: { description: '', message: '' },
          data: MerchantUserAccount
        })
      );
      expect(result).toEqual(true);
    });
  });

  describe('getUserInfoLoading selector', () => {
    it('should return whether or not user info is being loaded', () => {
      let result;
      store
        .select(fromSelectors.getUserInfoLoading)
        .subscribe(value => (result = value));
      expect(result).toEqual(false);

      store.dispatch(new fromActions.GetIdentity());
      expect(result).toEqual(true);
      store.dispatch(
        new fromActions.GetIdentitySuccess({
          success: { description: '', message: '' },
          data: MerchantUserAccount
        })
      );
      expect(result).toEqual(false);
    });
  });

  describe('getUserInfoLoaded selector', () => {
    it('should return whether or not user info las being loaded', () => {
      let result;
      store
        .select(fromSelectors.getUserInfoLoaded)
        .subscribe(value => (result = value));
      expect(result).toEqual(false);

      store.dispatch(new fromActions.GetIdentity());
      expect(result).toEqual(false);
      store.dispatch(
        new fromActions.GetIdentitySuccess({
          success: { description: '', message: '' },
          data: MerchantUserAccount
        })
      );
      expect(result).toEqual(true);
    });
  });

  describe('getUserType selector', () => {
    it('should return the userType', () => {
      let result;
      store
        .select(fromSelectors.getUserType)
        .subscribe(value => (result = value));
      expect(result).toEqual(undefined);

      store.dispatch(new fromActions.GetIdentity());
      expect(result).toEqual(undefined);
      store.dispatch(
        new fromActions.GetIdentitySuccess({
          success: { description: '', message: '' },
          data: MerchantUserAccount
        })
      );
      expect(result).toEqual('Merchant');
    });
  });

  describe('getUserId selector', () => {
    it('should return the user id', () => {
      let result;
      store
        .select(fromSelectors.getUserId)
        .subscribe(value => (result = value));
      expect(result).toEqual(undefined);

      store.dispatch(new fromActions.GetIdentity());
      expect(result).toEqual(undefined);
      store.dispatch(
        new fromActions.GetIdentitySuccess({
          success: { description: '', message: '' },
          data: MerchantUserAccount
        })
      );
      expect(result).toEqual(MerchantUserAccount._id);
    });
  });

  describe('getRedirectedUrl selector', () => {
    it('should return the redirectedUrl', () => {
      let result;
      store
        .select(fromSelectors.getRedirectedUrl)
        .subscribe(value => (result = value));
      expect(result).toEqual('/app');

      store.dispatch(new fromActions.SaveUrlAndLogin('/testing'));
      expect(result).toEqual('/testing');

      store.dispatch(new fromActions.GetIdentity());
      expect(result).toEqual('/testing');
      store.dispatch(
        new fromActions.GetIdentitySuccess({
          success: { description: '', message: '' },
          data: MerchantUserAccount
        })
      );
      expect(result).toEqual('/testing');
    });
  });

  describe('isIdentityResolved selector', () => {
    it('should tell whether or not user identity has been resolved', () => {
      let result;
      store
        .select(fromSelectors.isIdentityResolved)
        .subscribe(value => (result = value));
      expect(result).toEqual(false);

      store.dispatch(new fromActions.SaveUrlAndLogin('/testing'));
      expect(result).toEqual(false);

      store.dispatch(new fromActions.GetIdentity());
      expect(result).toEqual(false);

      store.dispatch(
        new fromActions.GetIdentitySuccess({
          success: { description: '', message: '' },
          data: MerchantUserAccount
        })
      );
      expect(result).toEqual(true);
    });
  });
});
