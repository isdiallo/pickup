import * as fromMainUserAction from './MainUser.action';
import { LoginInterface } from '../../model';
import {
  RegisterInfo,
  RelayInfo,
  LoginData,
  ChangePwdData
} from '../../testing/mainUser.data';
import { MerchantUserAccount } from '../../testing/users.data';
describe('MainUser Action', () => {
  describe('Login Actions', () => {
    describe('DoLogin Action', () => {
      it('should create an action', () => {
        const payload = LoginData;
        const action = new fromMainUserAction.DoLogin(payload);
        expect({ ...action }).toEqual({
          type: fromMainUserAction.DO_LOGIN,
          payload
        });
      });
    });
    describe('DoLoginSuccess Action', () => {
      it('should create an action', () => {
        const payload: LoginInterface = {
          success: { description: '', message: '' },
          token: 'test token'
        };
        const action = new fromMainUserAction.DoLoginSuccess(payload);
        expect({ ...action }).toEqual({
          type: fromMainUserAction.DO_LOGIN_SUCCESS,
          payload
        });
      });
    });
    describe('DoLoginFail Action', () => {
      it('should create an action', () => {
        const payload = {};
        const action = new fromMainUserAction.DoLoginFail({});
        expect({ ...action }).toEqual({
          type: fromMainUserAction.DO_LOGIN_FAIL,
          payload
        });
      });
    });
  });
  describe('Logout Actions', () => {
    describe('DoLogout Action', () => {
      it('should create an action', () => {
        const action = new fromMainUserAction.DoLogout();
        expect({ ...action }).toEqual({
          type: fromMainUserAction.DO_LOGOUT
        });
      });
    });

    describe('DoLogoutSuccess Action', () => {
      it('should create an action', () => {
        const payload = {};
        const action = new fromMainUserAction.DoLogoutSuccess({});
        expect({ ...action }).toEqual({
          type: fromMainUserAction.DO_LOGOUT_SUCCESS,
          payload
        });
      });
    });

    describe('DoLogoutFail Action', () => {
      it('should create an action', () => {
        const payload = {};
        const action = new fromMainUserAction.DoLogoutFail({});
        expect({ ...action }).toEqual({
          type: fromMainUserAction.DO_LOGOUT_FAIL,
          payload
        });
      });
    });
  });
  describe('Register Actions', () => {
    describe('DoRegister Action', () => {
      it('should create an action', () => {
        const payload = RegisterInfo;
        const action = new fromMainUserAction.DoRegister(payload);
        expect({ ...action }).toEqual({
          type: fromMainUserAction.DO_REGISTER,
          payload
        });
      });
    });

    describe('DoRegisterSuccess Action', () => {
      it('should create an action', () => {
        const payload = { success: { message: 'success' } };
        const action = new fromMainUserAction.DoRegisterSuccess(payload);
        expect({ ...action }).toEqual({
          type: fromMainUserAction.DO_REGISTER_SUCCESS,
          payload
        });
      });
    });

    describe('DoRegisterFail Action', () => {
      it('should create an action', () => {
        const payload = { error: { message: 'error' } };
        const action = new fromMainUserAction.DoRegisterFail(payload);
        expect({ ...action }).toEqual({
          type: fromMainUserAction.DO_REGISTER_FAIL,
          payload
        });
      });
    });
  });
  describe('SaveUrlAndLogin Action', () => {
    describe('SaveUrlAndLogin Action', () => {
      it('should create an action', () => {
        const payload = 'test/testurl';
        const action = new fromMainUserAction.SaveUrlAndLogin(payload);
        expect({ ...action }).toEqual({
          type: fromMainUserAction.SAVE_URL_AND_LOGIN,
          payload
        });
      });
    });
  });
  describe('User Identity Actions', () => {
    describe('GetIdentity Action', () => {
      it('should create an action', () => {
        const action = new fromMainUserAction.GetIdentity();
        expect({ ...action }).toEqual({
          type: fromMainUserAction.GET_IDENTITY
        });
      });
    });

    describe('GetIdentitySuccess Action', () => {
      it('should create an action', () => {
        const payload = MerchantUserAccount;
        const action = new fromMainUserAction.GetIdentitySuccess({
          success: { message: '', description: '' },
          data: MerchantUserAccount
        });
        expect({ ...action }).toEqual({
          type: fromMainUserAction.GET_IDENTITY_SUCSESS,
          payload: { success: { message: '', description: '' }, data: payload }
        });
      });
    });

    describe('GetIdentityFail Action', () => {
      it('should create an action', () => {
        const payload = { error: { message: 'error' } };
        const action = new fromMainUserAction.GetIdentityFail(payload);
        expect({ ...action }).toEqual({
          type: fromMainUserAction.GET_IDENTITY_FAIL,
          payload
        });
      });
    });
  });
  describe('Change PWD Actions', () => {
    describe('ChangePassword Action', () => {
      it('should create an action', () => {
        const payload = ChangePwdData;
        const action = new fromMainUserAction.ChangePassword(payload);
        expect({ ...action }).toEqual({
          type: fromMainUserAction.CHANGE_PASSWORD,
          payload
        });
      });
    });

    describe('ChangePasswordSuccess Action', () => {
      it('should create an action', () => {
        const action = new fromMainUserAction.ChangePasswordSuccess();
        expect({ ...action }).toEqual({
          type: fromMainUserAction.CHANGE_PASSWORD_SUCCESS
        });
      });
    });

    describe('ChangePasswordFail Action', () => {
      it('should create an action', () => {
        const payload = { error: { message: 'error' } };
        const action = new fromMainUserAction.ChangePasswordFail(payload);
        expect({ ...action }).toEqual({
          type: fromMainUserAction.CHANGE_PASSWORD_FAIL,
          payload
        });
      });
    });
  });
  describe('Current Users Actions', () => {
    describe('UpdateCurrentUser Action', () => {
      it('should create an action', () => {
        const payload = { autoAccept: true };
        const action = new fromMainUserAction.UpdateCurrentUser(payload);
        expect({ ...action }).toEqual({
          type: fromMainUserAction.UPDATE_CURRENT_USER,
          payload
        });
      });
    });

    describe('UpdateCurrentUserSuccess Action', () => {
      it('should create an action', () => {
        const payload = {
          success: { message: 'success msg', description: 'success desc' }
        };
        const action = new fromMainUserAction.UpdateCurrentUserSuccess(payload);
        expect({ ...action }).toEqual({
          type: fromMainUserAction.UPDATE_CURRENT_USER_SUCCESS,
          payload
        });
      });
    });

    describe('UpdateCurrentUserFail Action', () => {
      it('should create an action', () => {
        const payload = { error: { message: 'error' } };
        const action = new fromMainUserAction.UpdateCurrentUserFail(payload);
        expect({ ...action }).toEqual({
          type: fromMainUserAction.UPDATE_CURRENT_USER_FAIL,
          payload
        });
      });
    });
  });
  describe('Create/Update Relay Actions', () => {
    describe('CreateOrUpdateRelay Action', () => {
      it('should create an action', () => {
        const payload = RelayInfo;
        const action = new fromMainUserAction.CreateOrUpdateRelay(payload);
        expect({ ...action }).toEqual({
          type: fromMainUserAction.CREATE_OR_UPDATE_RELAY,
          payload
        });
      });
    });

    describe('CreateOrUpdateRelaySuccess Action', () => {
      it('should create an action', () => {
        const payload = { data: RelayInfo };
        const action = new fromMainUserAction.CreateOrUpdateRelaySuccess(
          payload
        );
        expect({ ...action }).toEqual({
          type: fromMainUserAction.CREATE_OR_UPDATE_RELAY_SUCCESS,
          payload
        });
      });
    });

    describe('CreateOrUpdateRelayFail Action', () => {
      it('should create an action', () => {
        const payload = { error: { message: 'error' } };
        const action = new fromMainUserAction.CreateOrUpdateRelayFail(payload);
        expect({ ...action }).toEqual({
          type: fromMainUserAction.CREATE_OR_UPDATE_RELAY_FAIL,
          payload
        });
      });
    });
  });
});
