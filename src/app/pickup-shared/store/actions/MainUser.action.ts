import { Action } from '@ngrx/store';
import {
  LoginInterface,
  Credential,
  UserAccountResponseInterface,
  RegisterInterface,
  UserUpdateInterface,
  RelayCreateOrUpdateResponseInterface
} from '../../model/index';
import { ChangePasswordInterface } from '../../../pickup-app/models';
import { RelayInterface } from '../../model/relay.model';

// Login actions
export const DO_LOGIN = '[App Shared::User] Do Login';
export const DO_LOGIN_SUCCESS = '[App Shared::User] Do Login Success';
export const DO_LOGIN_FAIL = '[App Shared::User] Do Login Fail';

// Logout actions
export const DO_LOGOUT = '[App Shared::User] Do Logout';
export const DO_LOGOUT_SUCCESS = '[App Shared::User] Do Logout Success';
export const DO_LOGOUT_FAIL = '[App Shared::User] Do Logout Fail';

// Register actions
export const DO_REGISTER = '[App Shared::User] Do Register';
export const DO_REGISTER_SUCCESS = '[App Shared::User] Do Register Success';
export const DO_REGISTER_FAIL = '[App Shared::User] Do Register Fail';

// Ask to login actions
export const SAVE_URL_AND_LOGIN =
  '[App Shared::User] Do save the url and redirect to login page';

// Get user identity actions
export const GET_IDENTITY = '[App Shared::User] Get Current User identity';
export const GET_IDENTITY_SUCSESS =
  '[App Shared::User] Get Current User identity Success';
export const GET_IDENTITY_FAIL =
  '[App Shared::User] Get Current User identity Fail';

// Change user password
export const CHANGE_PASSWORD = '[App Pickup::Users] change password';
export const CHANGE_PASSWORD_SUCCESS =
  '[App Pickup::Users] change password success';
export const CHANGE_PASSWORD_FAIL = '[App Pickup::Users] change password fail';

// Update current user information
export const UPDATE_CURRENT_USER =
  '[App Shared::User] update current user info';
export const UPDATE_CURRENT_USER_SUCCESS =
  '[App Shared::User] update current user info success';
export const UPDATE_CURRENT_USER_FAIL =
  '[App Shared::User] update current user info fails';

// Create or Update relay
export const CREATE_OR_UPDATE_RELAY =
  '[App Shared::User] create or update relay';
export const CREATE_OR_UPDATE_RELAY_SUCCESS =
  '[App Shared::User] create or update relay success';
export const CREATE_OR_UPDATE_RELAY_FAIL =
  '[App Shared::User] create or update relay fail';

export class DoLogin implements Action {
  readonly type = DO_LOGIN;
  constructor(public payload: Credential) {}
}

export class DoLoginSuccess implements Action {
  readonly type = DO_LOGIN_SUCCESS;
  constructor(public payload: LoginInterface) {}
}

export class DoLoginFail implements Action {
  readonly type = DO_LOGIN_FAIL;
  constructor(public payload: LoginInterface) {}
}

export class DoLogout implements Action {
  readonly type = DO_LOGOUT;
}

export class DoLogoutSuccess implements Action {
  readonly type = DO_LOGOUT_SUCCESS;
  constructor(public payload: any) {}
}
export class DoLogoutFail implements Action {
  readonly type = DO_LOGOUT_FAIL;
  constructor(public payload: any) {}
}

export class GetIdentity implements Action {
  readonly type = GET_IDENTITY;
}

export class GetIdentitySuccess implements Action {
  readonly type = GET_IDENTITY_SUCSESS;
  constructor(public payload: UserAccountResponseInterface) {}
}

export class GetIdentityFail implements Action {
  readonly type = GET_IDENTITY_FAIL;
  constructor(public payload: any) {}
}

export class DoRegister implements Action {
  readonly type = DO_REGISTER;
  constructor(public payload: RegisterInterface) {}
}

export class DoRegisterSuccess implements Action {
  readonly type = DO_REGISTER_SUCCESS;
  constructor(public payload: any) {}
}

export class DoRegisterFail implements Action {
  readonly type = DO_REGISTER_FAIL;
  constructor(public payload: any) {}
}

export class SaveUrlAndLogin implements Action {
  readonly type = SAVE_URL_AND_LOGIN;
  constructor(public payload: string) {}
}

export class ChangePassword implements Action {
  readonly type = CHANGE_PASSWORD;
  constructor(public payload: ChangePasswordInterface) {}
}

export class ChangePasswordSuccess implements Action {
  readonly type = CHANGE_PASSWORD_SUCCESS;
  constructor() {}
}

export class ChangePasswordFail implements Action {
  readonly type = CHANGE_PASSWORD_FAIL;
  constructor(public payload: any) {}
}

export class UpdateCurrentUser implements Action {
  readonly type = UPDATE_CURRENT_USER;
  constructor(public payload: UserUpdateInterface) {}
}

export class UpdateCurrentUserSuccess implements Action {
  readonly type = UPDATE_CURRENT_USER_SUCCESS;
  constructor(public payload: UserAccountResponseInterface) {}
}

export class UpdateCurrentUserFail implements Action {
  readonly type = UPDATE_CURRENT_USER_FAIL;
  constructor(public payload: any) {}
}

export class CreateOrUpdateRelay implements Action {
  readonly type = CREATE_OR_UPDATE_RELAY;
  constructor(public payload: RelayInterface) {}
}

export class CreateOrUpdateRelaySuccess implements Action {
  readonly type = CREATE_OR_UPDATE_RELAY_SUCCESS;
  constructor(public payload: RelayCreateOrUpdateResponseInterface) {}
}

export class CreateOrUpdateRelayFail implements Action {
  readonly type = CREATE_OR_UPDATE_RELAY_FAIL;
  constructor(public payload: any) {}
}

export type LoginAction =
  | DoLogin
  | DoLoginSuccess
  | DoLoginFail
  | GetIdentity
  | GetIdentitySuccess
  | GetIdentityFail
  | DoLogout
  | DoLogoutSuccess
  | DoLogoutFail
  | DoRegister
  | DoRegisterSuccess
  | DoRegisterFail
  | SaveUrlAndLogin
  | ChangePassword
  | ChangePasswordSuccess
  | ChangePasswordFail
  | UpdateCurrentUser
  | UpdateCurrentUserSuccess
  | UpdateCurrentUserFail
  | CreateOrUpdateRelay
  | CreateOrUpdateRelaySuccess
  | CreateOrUpdateRelayFail;
