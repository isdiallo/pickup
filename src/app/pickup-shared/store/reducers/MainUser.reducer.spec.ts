import * as fromMainUser from './MainUser.reducer';
import * as fromActions from '../actions/MainUser.action';
import {
  LoginData,
  RegisterInfo,
  ChangePwdData,
  RelayInfo
} from '../../testing/mainUser.data';
import { LoginInterface } from '../../model';
import { MerchantUserAccount } from '../../testing/users.data';

describe('MainUser Reducer', () => {
  describe('Undefined action', () => {
    it('should return default state', () => {
      const { initialState } = fromMainUser;
      const action = {} as any;
      const state = fromMainUser.reducer(undefined, action);
      expect(state).toBe(initialState);
    });
  });

  describe('Login action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromMainUser;
      const action = new fromActions.DoLogin(LoginData);
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(false);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('Login Success action', () => {
    it('should set loading to false and loaded to true and isAuthenticated to true', () => {
      const { initialState } = fromMainUser;
      const payload: LoginInterface = {
        success: { description: '', message: '' },
        token: 'test token'
      };
      const action = new fromActions.DoLoginSuccess(payload);
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(true);
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('Login Fail action', () => {
    it('should set loading to false and loaded to false and isAuthenticated to false', () => {
      const { initialState } = fromMainUser;
      const payload = {
        error: { description: 'error', message: 'err' }
      };
      const action = new fromActions.DoLoginFail(payload);
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(false);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('Logout action', () => {
    it('should set loading to true and loaded to false and isAuthenticated to false', () => {
      const { initialState } = fromMainUser;
      const action = new fromActions.DoLogout();
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(false);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('Logout Success action', () => {
    it('should set loading to false and loaded to false and isAuthenticated to false', () => {
      const { initialState } = fromMainUser;
      const action = new fromActions.DoLogoutSuccess({});
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(false);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('Logout Fail action', () => {
    it('should set loading to false and loaded to false and isAuthenticated to false', () => {
      const { initialState } = fromMainUser;
      const action = new fromActions.DoLogoutFail({});
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(false);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('Register action', () => {
    it('should set loading to true and loaded to false and isAuthenticated to false', () => {
      const { initialState } = fromMainUser;
      const payload = RegisterInfo;
      const action = new fromActions.DoRegister(payload);
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(false);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('Register Success action', () => {
    it('should set loading to false and loaded to true and isAuthenticated to true', () => {
      const { initialState } = fromMainUser;
      const payload = { success: { message: 'success' } };
      const action = new fromActions.DoRegisterSuccess(payload);
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(true);
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('Register Fail action', () => {
    it('should set loading to false and loaded to false and isAuthenticated to false', () => {
      const { initialState } = fromMainUser;
      const payload = { error: { message: 'error' } };
      const action = new fromActions.DoRegisterFail(payload);
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(false);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('Save Url and login action', () => {
    it('should set loading to false and loaded to false and isAuthenticated to false and update the redirect url', () => {
      const { initialState } = fromMainUser;
      const payload = 'test/testurl';
      const action = new fromActions.SaveUrlAndLogin(payload);
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(false);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
      expect(state.redirectUrl).toEqual('test/testurl');
    });
  });

  describe('Get identity action', () => {
    it('should set loading to true and loaded to false and isAuthenticated to false', () => {
      const { initialState } = fromMainUser;
      const action = new fromActions.GetIdentity();
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(false);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('Get identity Success action', () => {
    it('should set userInfo, loading to true and loaded to false and isAuthenticated to true', () => {
      const { initialState } = fromMainUser;
      const payload = MerchantUserAccount;
      const action = new fromActions.GetIdentitySuccess({
        success: { message: '', description: '' },
        data: MerchantUserAccount
      });
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(MerchantUserAccount);
      expect(state.isAuthenticated).toEqual(true);
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('Get identity Fail action', () => {
    it('should set loading to false and loaded to true and isAuthenticated to false', () => {
      const { initialState } = fromMainUser;
      const action = new fromActions.GetIdentityFail({});
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(false);
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('Change Password action', () => {
    it('should return the same state as before, in our case initialState', () => {
      const { initialState } = fromMainUser;
      const payload = ChangePwdData;
      const action = new fromActions.ChangePassword(payload);
      const state = fromMainUser.reducer(initialState, action);
      expect(state).toEqual(initialState);
    });
  });

  describe('Change Password Success action', () => {
    it('should return the same state as before, in our case initialState', () => {
      const { initialState } = fromMainUser;
      const action = new fromActions.ChangePasswordSuccess();
      const state = fromMainUser.reducer(initialState, action);
      expect(state).toEqual(initialState);
    });
  });

  describe('Change Password fail action', () => {
    it('should return the same state as before, in our case initialState', () => {
      const { initialState } = fromMainUser;
      const action = new fromActions.ChangePasswordFail({});
      const state = fromMainUser.reducer(initialState, action);
      expect(state).toEqual(initialState);
    });
  });

  describe('UpdateCurrentUser action', () => {
    it('should set loading to true and loaded to false and isAuthenticated to false', () => {
      const { initialState } = fromMainUser;
      const payload = { autoAccept: true };
      const action = new fromActions.UpdateCurrentUser(payload);
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(false);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('UpdateCurrentUser Success action', () => {
    it('should set userInfo, loading to true and loaded to false and isAuthenticated to true', () => {
      const { initialState } = fromMainUser;
      const payload = {
        success: { message: 'success msg', description: 'success desc' }
      };
      const action = new fromActions.UpdateCurrentUserSuccess(payload);
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(true);
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('UpdateCurrentUser Fail action', () => {
    it('should set loading to false and loaded to true and isAuthenticated to false', () => {
      const { initialState } = fromMainUser;
      const action = new fromActions.UpdateCurrentUserFail({});
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.isAuthenticated).toEqual(false);
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
      expect(state.redirectUrl).toEqual('/app');
    });
  });
  describe('Create/Update Relay action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromMainUser;
      const action = new fromActions.CreateOrUpdateRelay(RelayInfo);
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('Create/Update Success Relay action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromMainUser;
      const action = new fromActions.CreateOrUpdateRelaySuccess({
        data: RelayInfo
      });
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual({ relayId: RelayInfo });
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
      expect(state.redirectUrl).toEqual('/app');
    });
  });

  describe('Create/Update Fail Relay action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromMainUser;
      const action = new fromActions.CreateOrUpdateRelayFail({});
      const state = fromMainUser.reducer(initialState, action);
      expect(state.userInfo).toEqual(undefined);
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
      expect(state.redirectUrl).toEqual('/app');
    });
  });
});
