import * as fromMainUser from '../actions/MainUser.action';
import * as fromUserModel from '../../model/auth.model';
import { DEFAULT_REDIRECT_AFTER_LOGIN } from '../../pickup-shared.constants';

export interface MainUserState {
  userInfo: fromUserModel.UserAccountInterface | undefined | null;
  isAuthenticated: boolean;
  loading: boolean;
  loaded: boolean; // true: Identity has been called at least one time, succeed or not!
  redirectUrl: string;
}

export const initialState: MainUserState = {
  userInfo: undefined,
  isAuthenticated: false,
  loaded: false,
  loading: false,
  redirectUrl: DEFAULT_REDIRECT_AFTER_LOGIN // Defaut redirection by default!
};

export function reducer(
  state = initialState,
  action: fromMainUser.LoginAction
): MainUserState {
  switch (action.type) {
    case fromMainUser.DO_REGISTER:
    case fromMainUser.DO_LOGIN: {
      return {
        ...state,
        loading: true
      };
    }

    case fromMainUser.DO_REGISTER_SUCCESS:
    case fromMainUser.DO_LOGIN_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        isAuthenticated: true
      };
    }

    case fromMainUser.DO_REGISTER_FAIL:
    case fromMainUser.DO_LOGIN_FAIL: {
      return {
        ...state,
        loaded: false,
        loading: false,
        isAuthenticated: false
      };
    }
    case fromMainUser.GET_IDENTITY:
    case fromMainUser.CREATE_OR_UPDATE_RELAY:
    case fromMainUser.UPDATE_CURRENT_USER: {
      return {
        ...state,
        loaded: false,
        loading: true
      };
    }
    case fromMainUser.GET_IDENTITY_SUCSESS:
    case fromMainUser.UPDATE_CURRENT_USER_SUCCESS: {
      const userInfo = action.payload.data;
      return {
        ...state,
        loaded: true,
        loading: false,
        isAuthenticated: true,
        userInfo
      };
    }
    case fromMainUser.CREATE_OR_UPDATE_RELAY_SUCCESS: {
      const relayId = action.payload.data;
      return {
        ...state,
        loaded: true,
        loading: false,
        userInfo: {
          ...state.userInfo,
          relayId
        }
      };
    }
    case fromMainUser.UPDATE_CURRENT_USER_FAIL:
    case fromMainUser.CREATE_OR_UPDATE_RELAY_FAIL: {
      return {
        ...state,
        loaded: true,
        loading: false
      };
    }
    case fromMainUser.GET_IDENTITY_FAIL: {
      const userInfo = undefined;
      return {
        ...state,
        loading: false,
        loaded: true, // Should be true as it is used to determine if the if identity has been called
        isAuthenticated: false,
        userInfo
      };
    }
    case fromMainUser.DO_LOGOUT: {
      const userInfo = undefined;
      const redirectUrl = DEFAULT_REDIRECT_AFTER_LOGIN; // Reset the redirectUrl to its default
      return {
        ...state,
        loaded: false,
        loading: true,
        userInfo,
        redirectUrl
      };
    }
    case fromMainUser.DO_LOGOUT_SUCCESS: {
      return {
        ...state,
        isAuthenticated: false,
        loaded: false,
        loading: false
      };
    }
    case fromMainUser.DO_LOGOUT_FAIL: {
      return {
        ...state,
        isAuthenticated: false,
        loaded: false,
        loading: false
      };
    }
    case fromMainUser.SAVE_URL_AND_LOGIN: {
      const redirectUrl = action.payload;
      return {
        ...state,
        redirectUrl
      };
    }
    case fromMainUser.CHANGE_PASSWORD:
    case fromMainUser.CHANGE_PASSWORD_SUCCESS:
    case fromMainUser.CHANGE_PASSWORD_FAIL: {
      return state;
    }
  }
  return state;
}

// Main user Selectors
export const getUserInfo = (state: MainUserState) => state.userInfo;
export const isAuthenticated = (state: MainUserState) => state.isAuthenticated;
export const getUserInfoLoading = (state: MainUserState) => state.loading;
export const getUserInfoLoaded = (state: MainUserState) => state.loaded;
export const getUserImg = (state: MainUserState) =>
  !!state.userInfo && !!state.userInfo.img ? state.userInfo.img : '';
export const getRedirectedUrl = (state: MainUserState) => state.redirectUrl;
export const isIdentityResolved = (state: MainUserState) => !!state.userInfo;
