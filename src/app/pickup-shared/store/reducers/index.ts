import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import * as fromMainUser from './MainUser.reducer';

export class AppSharedState {
  mainUser: fromMainUser.MainUserState;
}

export const reducers: ActionReducerMap<AppSharedState> = {
  mainUser: fromMainUser.reducer
};

export const getAppSharedState = createFeatureSelector<AppSharedState>(
  'appShared'
);
