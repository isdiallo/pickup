import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of, concat } from 'rxjs';
import { map, flatMap, switchMap, catchError, tap } from 'rxjs/operators';

import * as fromRoot from '../../../store';
import * as fromAuth from '../../auth';
import * as mainUserAction from '../actions';

import { AppSharedState } from '../../store/reducers';
import { getRedirectedUrl } from '../../store/selectors';
import { ChangePasswordInterface } from '../../../pickup-app/models';
import {
  UserUpdateInterface,
  RelayCreateOrUpdateResponseInterface,
} from '../../model';
import { RelayInterface } from '../../model/relay.model';
import { CreateOrUpdateRelaySuccess } from '../actions';

@Injectable()
export class MainUserEffect {
  constructor(
    private actions$: Actions,
    private authServer: fromAuth.AuthServerProvider,
    private userService: fromAuth.UserService,
    private sharedStore: Store<AppSharedState>
  ) {}

  @Effect()
  doLogin$ = this.actions$.pipe(
    ofType(mainUserAction.DO_LOGIN),
    map((action: mainUserAction.DoLogin) => action.payload), // mapping here so we can access the credention (action.paylod)
    switchMap((credential) => {
      return this.authServer.login(credential).pipe(
        map((token) => new mainUserAction.DoLoginSuccess(token)), // the auth token! Not saved in the store
        catchError((error) => of(new mainUserAction.DoLoginFail(error)))
      );
    })
  );

  @Effect()
  doRegister$ = this.actions$.pipe(
    ofType(mainUserAction.DO_REGISTER),
    map((action: mainUserAction.DoRegister) => action.payload),
    switchMap((userCreationData) => {
      return this.userService.registerUser(userCreationData).pipe(
        map((token) => new mainUserAction.DoRegisterSuccess(token)),
        catchError((error) => of(new mainUserAction.DoRegisterFail(error)))
      );
    })
  );

  @Effect()
  loginSuccess$ = this.actions$.pipe(
    ofType(mainUserAction.DO_LOGIN_SUCCESS),
    flatMap((_) =>
      concat(
        of(new mainUserAction.GetIdentity()),
        this.sharedStore
          .select(getRedirectedUrl)
          .pipe(map((url) => new fromRoot.Go({ path: [url] })))
      )
    )
  );

  @Effect()
  saveUrlAndLogin$ = this.actions$.pipe(
    ofType(mainUserAction.SAVE_URL_AND_LOGIN),

    map(() => {
      return new fromRoot.Go({
        path: ['/login'],
      });
    })
  );

  @Effect()
  registerSuccess$ = this.actions$.pipe(
    ofType(mainUserAction.DO_REGISTER_SUCCESS),
    flatMap((_) =>
      concat(
        of(new mainUserAction.GetIdentity()),
        of(new fromRoot.Go({ path: ['/app'] }))
      )
    )
  );

  @Effect()
  logoutSuccess$ = this.actions$.pipe(
    ofType(mainUserAction.DO_LOGOUT_SUCCESS, mainUserAction.DO_LOGOUT_FAIL),
    map((_) => new fromRoot.Go({ path: ['/'] }))
  );

  @Effect()
  getCurrent$ = this.actions$.pipe(
    ofType(mainUserAction.GET_IDENTITY),
    switchMap(() =>
      this.userService.getUser().pipe(
        map((userInfo) => new mainUserAction.GetIdentitySuccess(userInfo)),
        catchError((error) => of(new mainUserAction.GetIdentityFail(error)))
      )
    )
  );

  @Effect()
  doLogout$ = this.actions$.pipe(
    ofType(mainUserAction.DO_LOGOUT),
    switchMap(() => {
      return this.authServer.logout().pipe(
        map((data) => new mainUserAction.DoLogoutSuccess(data)),
        catchError((error) => of(new mainUserAction.DoLogoutFail(error)))
      );
    })
  );

  @Effect()
  changePassword$ = this.actions$.pipe(
    ofType(mainUserAction.CHANGE_PASSWORD),
    map((action: mainUserAction.ChangePassword) => action.payload),
    switchMap((changePwdObj: ChangePasswordInterface) =>
      this.authServer.changePassword(changePwdObj).pipe(
        map((_) => new mainUserAction.ChangePasswordSuccess()),
        catchError((error) => of(new mainUserAction.ChangePasswordFail(error)))
      )
    )
  );

  @Effect()
  updateCurrentUser$ = this.actions$.pipe(
    ofType(mainUserAction.UPDATE_CURRENT_USER),
    map((action: mainUserAction.UpdateCurrentUser) => action.payload),
    switchMap((userUpdateObj: UserUpdateInterface) =>
      this.userService.updateCurrentUser(userUpdateObj).pipe(
        map(
          (updatedUser) =>
            new mainUserAction.UpdateCurrentUserSuccess(updatedUser)
        ),
        catchError((error) =>
          of(new mainUserAction.UpdateCurrentUserFail(error))
        )
      )
    )
  );

  @Effect()
  createOrUpdateRelay$ = this.actions$.pipe(
    ofType(mainUserAction.CREATE_OR_UPDATE_RELAY),
    map((action: mainUserAction.CreateOrUpdateRelay) => action.payload),
    switchMap((relay: RelayInterface) =>
      this.userService.createOrUpdateRelay(relay).pipe(
        map(
          (relayResponse: RelayCreateOrUpdateResponseInterface) =>
            new mainUserAction.CreateOrUpdateRelaySuccess(relayResponse)
        ),
        catchError((error) =>
          of(new mainUserAction.CreateOrUpdateRelayFail(error))
        )
      )
    )
  );
}
