import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  constructor() {}
  @Input() type: String;
  year = new Date().getFullYear();
  backgroundStyle = {};

  ngOnInit() {
    if (this.type !== 'app') {
      this.type = 'shared';
    }
    this.backgroundStyle = {
      'background-image':
        this.type === 'app'
          ? 'radial-gradient(circle farthest-corner at right bottom, #9FA8DA 0,#3F51B5 27%,#303F9F 74%,#1A237E 100%)'
          : 'radial-gradient(circle farthest-corner at right bottom, #CE93D8 0,#9C27B0 27%,#7B1FA2 74%,#4A148C 100%)'
    };
  }
}
