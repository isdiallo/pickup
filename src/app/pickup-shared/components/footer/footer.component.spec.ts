import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterComponent } from './footer.component';
import { DebugElement } from '@angular/core';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [FooterComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be shared footer by default', () => {
    fixture.detectChanges();
    expect(component.type).toEqual('shared');
  });

  it('should be shared footer if type not app', () => {
    component.type = 'not app';
    fixture.detectChanges();
    expect(component.type).toEqual('shared');
  });

  it('should have the copy right with the correct year as first child', () => {
    fixture.detectChanges();
    const footerDe: DebugElement = fixture.debugElement;
    const footerEl: HTMLElement = footerDe.nativeElement.querySelector(
      '.footer'
    );
    expect(footerEl.childElementCount).toEqual(6);
    const copyRight: HTMLElement = footerDe.nativeElement.querySelector(
      '.footer span:first-child'
    );
    expect(copyRight.textContent).toContain(
      'Copyright 2017-' + new Date().getFullYear()
    );
  });
});
