import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertListComponent } from './alert-list.component';
import { AlertComponent } from '../alert/alert.component';
import { DebugElement } from '@angular/core';

describe('AlertListComponent', () => {
  let component: AlertListComponent;
  let fixture: ComponentFixture<AlertListComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [AlertListComponent, AlertComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should close the index message', () => {
    component.messages = [
      { type: 'error', message: 'test.error', description: 'Test error' },
      { type: 'success', message: 'test.success', description: 'Test success' },
      { type: 'info', message: 'test.info', description: 'Test info' }
    ];
    fixture.detectChanges();
    expect(component.messages.length).toEqual(3);
    component.close(1);
    fixture.detectChanges();
    const alertListDe: DebugElement = fixture.debugElement;
    const alertListEl: HTMLElement[] = alertListDe.nativeElement.querySelectorAll(
      'app-alert'
    );
    expect(component.messages.length).toEqual(2);
    expect(alertListEl.length).toEqual(2);
  });
});
