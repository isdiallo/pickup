import { Component, OnInit, Input } from '@angular/core';
import { PickupEvent } from '../../model';

@Component({
  selector: 'app-alert-list',
  templateUrl: './alert-list.component.html',
  styleUrls: ['./alert-list.component.scss']
})
export class AlertListComponent implements OnInit {
  constructor() {}
  @Input() messages: PickupEvent[];

  close(idx: number) {
    this.messages.splice(idx, 1);
  }
  ngOnInit() {}
}
