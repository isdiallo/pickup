import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertComponent } from './alert.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('AlertComponent', () => {
  let component: AlertComponent;
  let fixture: ComponentFixture<AlertComponent>;
  const defaultClasses = {
    message: true
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AlertComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Current classes should be the default classes', () => {
    fixture.detectChanges();
    expect(component.currentClasses).toEqual({ message: true, info: true });
  });

  it('Alert Error classes', () => {
    component.type = 'error';
    fixture.detectChanges(); // ngOnInit
    expect(component.currentClasses).toEqual({
      ...defaultClasses,
      error: true
    });
  });

  it('Alert Info classes', () => {
    component.type = 'info';
    fixture.detectChanges();
    expect(component.currentClasses).toEqual({
      ...defaultClasses,
      info: true
    });
  });

  it('Alert Success classes', () => {
    component.type = 'success';
    fixture.detectChanges();
    expect(component.currentClasses).toEqual({
      ...defaultClasses,
      success: true
    });
  });

  it('Should emit close event onClose', () => {
    spyOn(component.close, 'emit');
    component.onClose();
    fixture.detectChanges();
    expect(component.close.emit).toHaveBeenCalledTimes(1);
  });

  it('Should show the content of message description', () => {
    component.message = {
      type: 'error',
      description: 'Test error',
      message: 'test.error'
    };
    fixture.detectChanges();
    const alertDe: DebugElement = fixture.debugElement;
    const alertMsg: HTMLElement = alertDe.query(By.css('span')).nativeElement;
    expect(alertMsg.textContent).toEqual('Test error');
  });

  it('Should have error class for error alert', () => {
    component.message = {
      type: 'error',
      description: 'Test error',
      message: 'test.error'
    };
    component.type = 'error';
    fixture.detectChanges(); // ngOnInit
    const alertDe: DebugElement = fixture.debugElement;
    const alertElement: HTMLElement = alertDe.query(By.css('div'))
      .nativeElement;
    expect(alertElement.className).toBe('message error');
  });

  it('Should have info class for info alert', () => {
    component.message = {
      type: 'info',
      description: 'Test info',
      message: 'test.info'
    };
    component.type = 'info';
    fixture.detectChanges(); // ngOnInit
    const alertDe: DebugElement = fixture.debugElement;
    const alertElement: HTMLElement = alertDe.query(By.css('div'))
      .nativeElement;
    expect(alertElement.className).toBe('message info');
  });

  it('Should have success class for success alert', () => {
    component.message = {
      type: 'success',
      description: 'Test success',
      message: 'test.success'
    };
    component.type = 'success';
    fixture.detectChanges(); // ngOnInit
    const alertDe: DebugElement = fixture.debugElement;
    const alertElement: HTMLElement = alertDe.query(By.css('div'))
      .nativeElement;
    expect(alertElement.className).toBe('message success');
  });

  it('Should have info class for default (without given type) alert', () => {
    component.message = {
      type: 'default',
      description: 'Test default',
      message: 'test.default'
    };
    fixture.detectChanges(); // ngOnInit
    const alertDe: DebugElement = fixture.debugElement;
    const alertElement: HTMLElement = alertDe.query(By.css('div'))
      .nativeElement;
    expect(alertElement.className).toBe('message info');
  });
});
