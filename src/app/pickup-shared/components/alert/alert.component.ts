import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PickupEvent } from '../../model';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  constructor() {}

  currentClasses = {
    message: true
  };

  @Input() type: string;
  @Output() close = new EventEmitter<any>();
  @Input() message: PickupEvent;

  onClose() {
    this.close.emit();
  }

  ngOnInit() {
    switch (this.type) {
      case 'success': {
        this.currentClasses['success'] = true;
        break;
      }
      case 'error': {
        this.currentClasses['error'] = true;
        break;
      }
      case 'info': {
        this.currentClasses['info'] = true;
        break;
      }
      default: {
        this.currentClasses['info'] = true;
      }
    }
  }
}
