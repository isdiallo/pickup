import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromSharedStore from './store';

@Directive({ selector: '[appHasAuthority]' })
export class HasAuthorityDirective {
  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private sharedStore: Store<fromSharedStore.AppSharedState>
  ) {}

  @Input()
  set appHasAuthority(authorities: string) {
    this.sharedStore.select(fromSharedStore.getUserType).subscribe(userType => {
      this.viewContainer.clear();
      // has authority
      if (authorities && authorities.includes(userType)) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      }
    });
  }
}
