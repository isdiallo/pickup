import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Store } from '@ngrx/store';

import * as fromSharedStore from './pickup-shared/store';

const appRoutes: Routes = [
  {
    path: 'app',
    loadChildren: () => import('./pickup-app/pickup-app.module').then(m => m.PickupAppModule)
  },
  {
    path: '',
    loadChildren: () => import('./pickup-info/pickup-info.module').then(m => m.PickupInfoModule)
  },
  {
    path: '**',
    redirectTo: ''
  }
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor(private sharedStore: Store<fromSharedStore.AppSharedState>) {
    this.sharedStore.dispatch(new fromSharedStore.GetIdentity());
  }
}
