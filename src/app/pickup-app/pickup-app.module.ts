import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDialogModule } from '@angular/material/dialog';
import { FlexLayoutModule } from '@angular/flex-layout';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { reducers, effects } from './store';

import * as fromServices from './services';

import { PickupAppComponent } from './pickup-app.component';
import { PickupAppRoutingModule } from './pickup-app-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PickupsComponent } from './components/pickups/pickups.component';
import { UserNamePipe } from '../pickup-shared/pipes/user.pipe';
import { PickupSharedModule } from '../pickup-shared/pickup-shared.module';
import { ShowCodeDirective } from './components/pickups/show-code.directive';
import { PickupsListComponent } from './components/pickups/pickups-list.component';
import { PickupViewComponent } from './components/pickup-view/pickup-view.component';
import { PickupsContainerComponent } from './components/pickups/pickups-container.component';
import { PickupSizeComponent } from './components/pickup-size/pickup-size.component';
import { MessageComponent } from './components/message/message.component';
import { MessageViewComponent } from './components/message-view/message-view.component';
import { CreatePickupViewComponent } from './components/pickup-view/create-pickup-view.component';
import { AdminComponent } from './components/admin/admin.component';
import { AdminPickupComponent } from './components/admin/admin-pickup.component';
import { SinglePickupViewComponent } from './components/pickup-view/single-pickup-view.component';
import { PickupUserComponent } from './components/pickup-view/pickup-user.component';
import { AgentAssignationComponent } from './components/admin/agent-assignation.component';
import { AdminPickupValidationComponent } from './components/admin/admin-pickup-validation.component';
import { SettingsComponent } from './components/settings/settings.component';
import { UserTypePipe } from '../pickup-shared/pipes/user-type.pipe';
import { UserDetailsComponent } from './components/settings/user-details.component';
import { ChangePasswordComponent } from './components/settings/change-password.component';
import { MerchantDetailsComponent } from './components/settings/merchant-details.component';
import { RelayDetailsComponent } from './components/settings/relay-details.component';

@NgModule({
  declarations: [
    PickupAppComponent,
    DashboardComponent,
    PickupsComponent,
    PickupsListComponent,
    PickupViewComponent,
    PickupsContainerComponent,
    PickupSizeComponent,
    MessageComponent,
    MessageViewComponent,
    CreatePickupViewComponent,
    AdminComponent,
    AdminPickupComponent,
    SinglePickupViewComponent,
    PickupUserComponent,
    AgentAssignationComponent,
    AdminPickupValidationComponent,
    SettingsComponent,
    UserDetailsComponent,
    ChangePasswordComponent,
    MerchantDetailsComponent,
    RelayDetailsComponent,
    UserNamePipe,
    UserTypePipe,
    ShowCodeDirective,
  ],
  imports: [
    CommonModule,
    MatMenuModule,
    LayoutModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatInputModule,
    MatTabsModule,
    MatTooltipModule,
    MatCardModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatDialogModule,
    FlexLayoutModule,
    PickupSharedModule,
    HttpClientModule,
    PickupAppRoutingModule,
    StoreModule.forFeature('appPickup', reducers), // will allow the lazy load of anything related to the store
    EffectsModule.forFeature(effects),
  ],
  entryComponents: [AgentAssignationComponent],
  providers: [...fromServices.services],
})
export class PickupAppModule {}
