import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PickupAppComponent } from './pickup-app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { RouteAccessGuard } from '../pickup-shared/guards/routeAccess.guard';
import { PickupViewComponent } from './components/pickup-view/pickup-view.component';
import { PickupsContainerComponent } from './components/pickups/pickups-container.component';
import { CreatePickupViewComponent } from './components/pickup-view/create-pickup-view.component';
import { AdminComponent } from './components/admin/admin.component';
import { SinglePickupViewComponent } from './components/pickup-view/single-pickup-view.component';
import { AdminPickupValidationComponent } from './components/admin/admin-pickup-validation.component';
import { SettingsComponent } from './components/settings/settings.component';
import { MessageViewComponent } from './components/message-view/message-view.component';

const pickupAppRoutes: Routes = [
  {
    path: '',
    component: PickupAppComponent,
    canActivate: [RouteAccessGuard],
    data: {
      authorities: []
    },
    children: [
      {
        path: '', // Make a component less so that we can manage easily the children guards
        canActivateChild: [RouteAccessGuard],
        children: [
          {
            path: '',
            component: DashboardComponent,
            data: {
              authorities: ['Admin', 'Agent', 'UserRelay', 'Merchant'],
              pageTitle: 'Dashboard'
            }
          },
          {
            path: 'create-pickups',
            component: CreatePickupViewComponent,
            data: {
              name: 'createPickups',
              authorities: ['Merchant']
            }
          },
          // {
          //   path: "admin",
          //   component: AdminComponent,
          //   data: {
          //     name: "admin",
          //     authorities: ["Admin"]
          //   }
          // },
          {
            path: 'messages',
            component: MessageViewComponent,
            data: {
              name: 'message',
              authorities: ['Admin', 'Agent', 'UserRelay', 'Merchant']
            }
          },
          {
            path: 'settings',
            component: SettingsComponent,
            data: {
              name: 'settings',
              authorities: ['Admin', 'Agent', 'UserRelay', 'Merchant']
            }
          },
          {
            path: 'admin-pickup-validation',
            component: AdminPickupValidationComponent,
            data: {
              name: 'adminPickupValidation',
              authorities: ['Admin']
            }
          },
          {
            path: 'pickup/:pickupId',
            component: SinglePickupViewComponent,
            data: {
              name: 'singlePickup',
              authorities: ['Admin', 'Agent', 'UserRelay', 'Merchant']
            }
          },
          {
            path: 'pickups',
            component: PickupsContainerComponent,
            data: {
              authorities: ['Agent', 'UserRelay', 'Merchant'],
              pageTitle: 'Pickups'
            },
            canActivateChild: [RouteAccessGuard],
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: '/app'
              },
              {
                path: 'to-be-validated',
                component: PickupViewComponent,

                data: {
                  name: 'toBeValidated',
                  authorities: ['Merchant']
                }
              },
              {
                path: 'validated',
                component: PickupViewComponent,
                data: {
                  name: 'validated',
                  authorities: ['Merchant']
                }
              },
              {
                path: 'delivered',
                component: PickupViewComponent,
                data: {
                  name: 'delivered',
                  authorities: ['Merchant']
                }
              },
              {
                path: 'assigned',
                component: PickupViewComponent,
                data: {
                  name: 'assigned',
                  authorities: ['UserRelay', 'Agent']
                }
              },
              {
                path: 'my-last',
                component: PickupViewComponent,
                data: {
                  name: 'myLast',
                  authorities: ['UserRelay', 'Agent']
                }
              }
            ]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(pickupAppRoutes)],
  exports: [RouterModule],
  providers: [RouteAccessGuard]
})
export class PickupAppRoutingModule {}
