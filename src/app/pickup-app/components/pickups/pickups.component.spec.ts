import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickupsComponent } from './pickups.component';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { UserNamePipe } from '../../../pickup-shared/pipes/user.pipe';
import { HasAuthorityDirective } from '../../../pickup-shared/has-authority.directive';
import { RouterModule } from '@angular/router';
import { ShowCodeDirective } from './show-code.directive';
import { Page } from '../../../pickup-shared/testing/page';
import { PickupsWithRelay } from '../../../pickup-shared/testing/pickups.data';
import { Store, StoreModule, combineReducers } from '@ngrx/store';
import * as fromSharedStore from '../../../pickup-shared/store';
import * as fromSharedActions from '../../../pickup-shared/store/actions';
import * as fromRoot from '../../../store';
import { UserType } from '../../../pickup-shared/model';
import { RouterTestingModule } from '@angular/router/testing';
import { DebugElement } from '@angular/core';
import { RouterLinkStubDirective } from '../../../pickup-shared/testing/RouterLink.stub';
import { By } from '@angular/platform-browser';

describe('PickupsComponent', () => {
  let component: PickupsComponent;
  let fixture: ComponentFixture<PickupsComponent>;
  let pickupPage: Page<PickupsComponent>;
  let store: Store<fromSharedStore.AppSharedState>;

  function updateStore() {
    store.dispatch(
      new fromSharedActions.GetIdentitySuccess({
        success: { description: '', message: '' },
        data: {
          _id: 'fgdsfg5sdgfsdfg',
          _type: UserType.Merchant,
          updatedAt: '',
          createdAt: '',
          firstname: 'Merch Test',
          lastname: 'Merch Last',
          username: 'merchant@test.fr',
          phone: '145214587',
          mail: 'merchant@test.fr',
          city: 'Merch City',
          town: 'Merch town',
          address: 'Merch address',
        },
      })
    );
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PickupsComponent,
        UserNamePipe,
        HasAuthorityDirective,
        ShowCodeDirective,
        RouterLinkStubDirective,
      ],
      imports: [
        MatIconModule,
        MatFormFieldModule,
        RouterModule,
        RouterTestingModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          appShared: combineReducers(fromSharedStore.reducers),
        }),
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickupsComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);
    pickupPage = new Page(fixture);
    updateStore();
    component.pickup = PickupsWithRelay;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have all necessary fields on pickup header shown', () => {
    expect(
      pickupPage.query<HTMLElement>('.pickup-header  .validate > button')
    ).toBeTruthy();
    expect(
      pickupPage.query<HTMLElement>('.pickup-header  .validate > button')
        .textContent
    ).toBe('Validate');
    expect(pickupPage.query<HTMLElement>('.to').innerText).toBe(
      'Test L. - 452102547'
    );
    expect(pickupPage.query<HTMLElement>('.fragile')).toBeFalsy();
    expect(
      pickupPage.query<HTMLElement>('.package > span:nth-of-type(1)')
        .textContent
    ).toBe('2');
    expect(
      pickupPage.query<HTMLElement>('.package > span:nth-of-type(2)')
        .textContent
    ).toBe('package(s)');
    expect(pickupPage.query<HTMLElement>('.cost').innerText).toBe('15000 GNF');
    expect(pickupPage.query<HTMLElement>('.free')).toBeFalsy();
    expect(pickupPage.query<HTMLElement>('.state').innerText).toBe('CREATED');
    expect(pickupPage.query<HTMLElement>('.more')).toBeTruthy();
  });

  it('should not expand the pickup by default', () => {
    expect(pickupPage.query<HTMLElement>('.pickup-content')).toBeFalsy();
  });

  it('should expand the pickup when more is clicked', () => {
    pickupPage.query<HTMLElement>('.more > button').click();
    expect(component.isExpanded).toBe(true);
    fixture.detectChanges();
    expect(pickupPage.query<HTMLElement>('.pickup-content')).toBeTruthy();
  });

  it('should show necessary fields when pickup expanded', () => {
    pickupPage.query<HTMLElement>('.more > button').click();
    expect(component.isExpanded).toBe(true);
    fixture.detectChanges();
    // dates line
    expect(
      pickupPage.query<HTMLElement>(
        '.dates > .date-entry:first-child > .pull-right'
      ).textContent
    ).toBe('Mon 30 Apr 19:18');
    expect(
      pickupPage.query<HTMLElement>(
        '.dates > .date-entry:last-child > .pull-right'
      ).textContent
    ).toBe('Mon 30 Apr 19:18');
    // users
    expect(
      pickupPage.query<HTMLElement>(
        '.users > .user:nth-child(1) > .header > span'
      ).textContent
    ).toBe('Relay User');
    expect(
      pickupPage.query<HTMLElement>(
        '.users > .user:nth-child(1) > div:nth-child(2) span:last-child'
      ).textContent
    ).toBe('Relay test');
    expect(
      pickupPage.query<HTMLElement>(
        '.users > .user:nth-child(1) > div:nth-child(3) span:last-child'
      ).textContent
    ).toBe('145214587');
    expect(
      pickupPage.query<HTMLElement>(
        '.users > .user:nth-child(1) > div:nth-child(4) span:last-child'
      ).textContent
    ).toBe('test relay address-Kamsar-Kamsar');
    expect(
      pickupPage.query<HTMLElement>(
        '.users > .user:nth-child(2) > .header > span'
      ).textContent
    ).toBe('End User');
    expect(
      pickupPage.query<HTMLElement>(
        '.users > .user:nth-child(2) > div:nth-child(2) span:last-child'
      ).textContent
    ).toBe('Test L.');
    expect(
      pickupPage.query<HTMLElement>(
        '.users > .user:nth-child(2) > div:nth-child(3) span:last-child'
      ).textContent
    ).toBe('452102547');
    expect(
      pickupPage.query<HTMLElement>(
        '.users > .user:nth-child(2) > div:nth-child(4) span:last-child'
      ).textContent
    ).toBe('Adress-Town-City');
    // actions
    expect(pickupPage.queryAll<HTMLElement>('.actions > .action').length).toBe(
      1
    );
    expect(
      pickupPage.query<HTMLElement>('.actions > .action > button:first-child')
        .textContent
    ).toBe('History');
  });

  it('should emit validate event whith the correct pickupId when click on validate btn', () => {
    let pickupId: string;
    component.validate.subscribe((id: string) => (pickupId = id));
    pickupPage.query<HTMLElement>('.pickup-header  .validate > button').click();
    expect(pickupId).toBe('5ae75df70b6a041074e93b0c');
  });

  it('should open individual pickup in a tab when click on open icon', () => {
    pickupPage.query<HTMLElement>('.more > button').click();
    fixture.detectChanges();
    const linkDes = fixture.debugElement.queryAll(
      By.directive(RouterLinkStubDirective)
    );
    const routerLinks = linkDes.map((de) =>
      de.injector.get(RouterLinkStubDirective)
    );
    const openPickupDe = linkDes[0];
    const openPickupLink = routerLinks[0];
    pickupPage
      .query<HTMLElement>('.actions > .action > button:last-child')
      .click();
    expect(openPickupLink.navigatedTo).toEqual([
      '/app',
      'pickup',
      '5ae75df70b6a041074e93b0c',
    ]);
  });
});
