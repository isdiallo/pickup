import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromSharedStore from '../../../pickup-shared/store';

@Directive({ selector: '[appShowCode]' })
export class ShowCodeDirective {
  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private sharedStore: Store<fromSharedStore.AppSharedState>
  ) {}

  @Input()
  set appShowCode(state: string) {
    this.sharedStore.select(fromSharedStore.getUserType).subscribe(userType => {
      this.viewContainer.clear();
      // show code
      if (
        (userType === 'Merchant' && state === 'ASSIGNED') ||
        (userType === 'UserRelay' && state === 'TRANSMITTED') ||
        (userType === 'Agent' && state === 'ONGOING')
      ) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      }
    });
  }
}
