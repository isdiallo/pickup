import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common';

import { PickupInterface } from '../../../pickup-shared/model/pickup.model';
import { ValidateCodeEventInterface } from '../../models';

@Component({
  selector: 'app-pickup',
  templateUrl: './pickups.component.html',
  styleUrls: ['./pickups.component.scss']
})
export class PickupsComponent {
  constructor() {}

  @Input() pickup: PickupInterface;
  @Output() validate = new EventEmitter<string>();
  @Output() sendCodeEvent = new EventEmitter<string>();
  @Output() validateCodeEvent = new EventEmitter<ValidateCodeEventInterface>();

  isExpanded = false;
  isShowCode = false;

  validatePickup() {
    this.validate.emit(this.pickup._id);
  }

  getNbPacks(): number {
    return this.pickup.packages.reduce((accum, currentVal) => {
      return accum + currentVal.quantity;
    }, 0);
  }
  sendCode() {
    // Send Code only when code validation panel is not shown
    if (!this.isShowCode) {
      this.sendCodeEvent.emit(this.pickup._id);
    }
    this.isShowCode = !this.isShowCode;
  }

  validateCode(code) {
    if (code) {
      this.validateCodeEvent.emit({ _id: this.pickup._id, code: code });
    }
  }

  showHistory() {
    console.log('showing history of', this.pickup);
  }

  isFragile(): boolean {
    return this.pickup.packages.some(pack => pack.fragile);
  }

  expandCollapase() {
    this.isExpanded = !this.isExpanded;
  }
}
