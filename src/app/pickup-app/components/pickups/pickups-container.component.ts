import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import * as fromSharedStore from '../../../pickup-shared/store';
import { PICKUP_TABS } from '../pickups/pickups.constants';

@Component({
  templateUrl: './pickups-container.component.html'
})
export class PickupsContainerComponent implements OnInit {
  currentPickupTabs: any[];
  constructor(private sharedStore: Store<fromSharedStore.AppSharedState>) {}
  ngOnInit() {
    this.sharedStore.select(fromSharedStore.getUserType).subscribe(userType => {
      this.currentPickupTabs = PICKUP_TABS.filter(tab =>
        tab.users.includes(userType)
      );
    });
  }
}
