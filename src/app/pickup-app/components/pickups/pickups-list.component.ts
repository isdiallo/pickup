import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PickupInterface } from '../../../pickup-shared/model/pickup.model';
import { ValidateCodeEventInterface } from '../../models';

@Component({
  templateUrl: './pickups-list.component.html',
  selector: 'app-pickups-list',
  styleUrls: ['./pickups-list.component.scss']
})
export class PickupsListComponent {
  @Input() pickups: PickupInterface[];
  @Output() validate = new EventEmitter<string>();
  @Output() sendCodeEvent = new EventEmitter<string>();
  @Output() validateCodeEvent = new EventEmitter<ValidateCodeEventInterface>();

  constructor() {}

  validatePickup(event: string) {
    this.validate.emit(event);
  }

  validateCode(event: ValidateCodeEventInterface) {
    this.validateCodeEvent.emit(event);
  }

  sendCode(event: string) {
    this.sendCodeEvent.emit(event);
  }
}
