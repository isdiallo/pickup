export const PICKUP_TABS = [
  {
    path: '/app/pickups/to-be-validated',
    label: 'To Be Validated',
    users: ['Merchant']
  },
  {
    path: '/app/pickups/validated',
    label: 'Validated',
    users: ['Merchant']
  },
  {
    path: '/app/pickups/delivered',
    label: 'Delivered',
    users: ['Merchant']
  },
  {
    path: '/app/pickups/assigned',
    label: 'Assigned',
    users: ['Agent', 'UserRelay']
  },
  {
    path: '/app/pickups/my-last',
    label: 'Last',
    users: ['Agent', 'UserRelay']
  }
];
