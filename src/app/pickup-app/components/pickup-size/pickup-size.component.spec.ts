import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickupSizeComponent } from './pickup-size.component';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';

describe('PickupSizeComponent', () => {
  let component: PickupSizeComponent;
  let fixture: ComponentFixture<PickupSizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PickupSizeComponent],
      imports: [MatIconModule, MatTooltipModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickupSizeComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should initialize correctely the params, tooltipContent to XL and tooltipMsg to This is an extra large pickup', () => {
    fixture.detectChanges();
    expect(component.tooltipContent).toBe('XL');
    expect(component.tooltipMsg).toBe('This is an extra large pickup');
  });

  it('should initialize tooltipMsg to This is a large pickup when content is L', () => {
    component.content = 'L';
    fixture.detectChanges();
    expect(component.tooltipMsg).toBe('This is a large pickup');
  });

  it('should initialize tooltipMsg to This is a medium pickup when content is M', () => {
    component.content = 'M';
    fixture.detectChanges();
    expect(component.tooltipMsg).toBe('This is a medium pickup');
  });

  it('should initialize tooltipMsg to This is a small pickup when content is S', () => {
    component.content = 'S';
    fixture.detectChanges();
    expect(component.tooltipMsg).toBe('This is a small pickup');
  });

  it('should initialize tooltipMsg and tooltipContent accordingly to what content and msg inputs', () => {
    component.content = 'Z';
    component.msg = 'This is a Z pickup';
    fixture.detectChanges();
    expect(component.tooltipMsg).toBe('This is a Z pickup');
    expect(component.tooltipContent).toBe('Z');
  });
});
