import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pickup-size',
  templateUrl: './pickup-size.component.html',
  styleUrls: ['./pickup-size.component.scss']
})
export class PickupSizeComponent implements OnInit {
  constructor() {}

  @Input() msg: string;
  @Input() content: string;
  tooltipMsg: string;
  tooltipContent: string;

  ngOnInit() {
    this.tooltipContent = !!this.content ? this.content.toUpperCase() : 'XL';

    if (!!this.msg) {
      this.tooltipMsg = this.msg;
    } else {
      switch (this.tooltipContent) {
        case 'XL': {
          this.tooltipMsg = 'This is an extra large pickup';
          break;
        }
        case 'L': {
          this.tooltipMsg = 'This is a large pickup';
          break;
        }
        case 'M': {
          this.tooltipMsg = 'This is a medium pickup';
          break;
        }
        case 'S': {
          this.tooltipMsg = 'This is a small pickup';
          break;
        }
        default: {
          this.tooltipMsg = 'This is an extra large pickup';
        }
      }
    }
  }
}
