import { Component, OnInit, OnDestroy } from '@angular/core';

import { Store } from '@ngrx/store';
import * as fromMessageStore from '../../store';
import { Observable } from 'rxjs';
import { MessageInterface } from '../../models';
import { BroadcasterService } from '../../../pickup-shared/services';
import { filter } from 'rxjs/operators';
import { PickupEvent } from '../../../pickup-shared/model';

@Component({
  selector: 'app-message-view',
  templateUrl: './message-view.component.html',
  styleUrls: ['./message-view.component.scss'],
})
export class MessageViewComponent implements OnInit, OnDestroy {
  constructor(
    private messageStore: Store<fromMessageStore.AppPickupState>,
    private broadcastService: BroadcasterService
  ) {}
  messages$: Observable<MessageInterface[]>;

  eventList: PickupEvent[] = [];
  events$ = this.broadcastService._eventBus
    .pipe(filter((event: PickupEvent) => event.type === 'error'))
    .subscribe((event) => {
      this.eventList.push(event);
    });

  ngOnInit() {
    this.messageStore.dispatch(new fromMessageStore.LoadMessages());
    this.messages$ = this.messageStore.select(fromMessageStore.getMessages);
  }

  ngOnDestroy() {
    this.events$.unsubscribe();
  }
}
