import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageViewComponent } from './message-view.component';
import { MessageComponent } from '../message/message.component';
import * as fromPickupStore from '../../store';
import * as fromRoot from '../../../store';

import { Store, StoreModule, combineReducers } from '@ngrx/store';
import { AlertListComponent } from '../../../pickup-shared/components/alert-list/alert-list.component';
import { AlertComponent } from '../../../pickup-shared/components/alert/alert.component';
import { BroadcasterService } from '../../../pickup-shared/services';

describe('MessageViewComponent', () => {
  let component: MessageViewComponent;
  let fixture: ComponentFixture<MessageViewComponent>;
  let messageStore: Store<fromPickupStore.AppPickupState>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MessageViewComponent,
        MessageComponent,
        AlertListComponent,
        AlertComponent
      ],
      imports: [
        StoreModule.forRoot({
          ...fromRoot.reducers,
          appPickup: combineReducers(fromPickupStore.reducers)
        })
      ],
      providers: [BroadcasterService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageViewComponent);
    messageStore = TestBed.get(Store);
    spyOn(messageStore, 'dispatch').and.callThrough();
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
