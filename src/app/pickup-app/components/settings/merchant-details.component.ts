import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { MerchantDetailsInterface } from '../../models';
import {
  UserAccountInterface,
  PickupEvent
} from '../../../pickup-shared/model';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { websiteUrlRequiredValidator } from '../../validators/website-url.validator';

@Component({
  selector: 'app-merchant-details',
  templateUrl: './merchant-details.component.html',
  styleUrls: ['./settings.component.scss']
})
export class MerchantDetailsComponent implements OnInit {
  constructor(private formBuilder: FormBuilder) {}
  @Input() details: UserAccountInterface;
  @Output() updatedDetails = new EventEmitter<MerchantDetailsInterface>();
  merchantDetailsForm: FormGroup;
  submitDisabled: boolean;

  onSubmitDetails() {
    const preSubmit = {
      autoAccept: this.merchantDetailsForm.value.autoAccept,
      sendNotif: this.merchantDetailsForm.value.sendNotif
    };
    if (this.merchantDetailsForm.value.websiteName) {
      if (this.merchantDetailsForm.value.websiteUrl) {
        preSubmit['websiteName'] = this.merchantDetailsForm.value.websiteName;
        preSubmit['websiteUrl'] = this.merchantDetailsForm.value.websiteUrl;
      }
    } else {
      if (this.merchantDetailsForm.value.websiteUrl) {
        preSubmit['websiteUrl'] = this.merchantDetailsForm.value.websiteUrl;
      }
    }
    this.updatedDetails.emit(preSubmit);
  }

  onChanged(event) {
    this.submitDisabled = false;
  }

  ngOnInit() {
    this.submitDisabled = true;
    this.merchantDetailsForm = this.formBuilder.group(
      {
        websiteName: [
          this.details && this.details.websiteName
            ? this.details.websiteName
            : ''
        ],
        websiteUrl: [
          this.details && this.details.websiteUrl
            ? this.details.websiteUrl
            : '',
          [websiteUrlRequiredValidator()]
        ],
        autoAccept: [
          this.details && this.details.autoAccept
            ? this.details.autoAccept
            : false
        ],
        sendNotif: [
          this.details && this.details.sendNotif
            ? this.details.sendNotif
            : false
        ]
      },
      { validator: websiteUrlRequiredValidator() }
    );
  }
}
