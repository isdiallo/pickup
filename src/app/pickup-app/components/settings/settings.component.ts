import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromSharedStore from '../../../pickup-shared/store';
import { UserAccountInterface } from '../../../pickup-shared/model';
import {
  ChangePasswordInterface,
  MerchantDetailsInterface,
} from '../../models';
import { RelayInterface } from '../../../pickup-shared/model/relay.model';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  constructor(private sharedStored: Store<fromSharedStore.AppSharedState>) {}
  userInfo$: Observable<UserAccountInterface>;
  panelOpenState = false;

  changePwd(event: ChangePasswordInterface) {
    this.sharedStored.dispatch(new fromSharedStore.ChangePassword(event));
  }

  updateDetails(event: MerchantDetailsInterface) {
    this.sharedStored.dispatch(new fromSharedStore.UpdateCurrentUser(event));
  }

  updateRelay(event: RelayInterface) {
    this.sharedStored.dispatch(new fromSharedStore.CreateOrUpdateRelay(event));
  }

  ngOnInit() {
    this.userInfo$ = this.sharedStored.select(fromSharedStore.getUserInfo);
  }
}
