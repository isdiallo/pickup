import { ChangePasswordComponent } from './change-password.component';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Page } from '../../../pickup-shared/testing/page';

describe('ChangePasswordComponent', () => {
  let component: ChangePasswordComponent;
  let fixture: ComponentFixture<ChangePasswordComponent>;
  let changePwdPage: Page<ChangePasswordComponent>;

  function updateChangePwdForm(key: string, value: string) {
    component.changePwdForm.controls[key].setValue(value);
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
      ],
      declarations: [ChangePasswordComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasswordComponent);
    component = fixture.componentInstance;
    changePwdPage = new Page(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should disable update password button', () => {
    expect(changePwdPage.query<HTMLButtonElement>('button').disabled).toBe(
      true
    );
  });
  it('should keep update password button disabled if only current password given', () => {
    updateChangePwdForm('currentPwd', 'Test');
    fixture.detectChanges();
    expect(changePwdPage.query<HTMLButtonElement>('button').disabled).toBe(
      true
    );
  });

  it('should keep update password button disabled if only new password given', () => {
    updateChangePwdForm('newPwd', 'Test');
    fixture.detectChanges();
    expect(changePwdPage.query<HTMLButtonElement>('button').disabled).toBe(
      true
    );
  });

  it('should keep update password button disabled if confirm password not given', () => {
    updateChangePwdForm('currentPwd', 'Test');
    updateChangePwdForm('newPwd', 'Test');
    fixture.detectChanges();
    expect(changePwdPage.query<HTMLButtonElement>('button').disabled).toBe(
      true
    );
  });

  it('should keep update password button disabled if new and confirm pwd are differents', () => {
    updateChangePwdForm('currentPwd', 'Test');
    updateChangePwdForm('newPwd', 'TestNew');
    updateChangePwdForm('confirmNewPwd', 'TestNe');
    fixture.detectChanges();
    expect(changePwdPage.query<HTMLButtonElement>('button').disabled).toBe(
      true
    );
  });

  it('should enable update password when all fields are correts', () => {
    updateChangePwdForm('currentPwd', 'Test');
    updateChangePwdForm('newPwd', 'TestNew');
    updateChangePwdForm('confirmNewPwd', 'TestNew');
    fixture.detectChanges();
    expect(changePwdPage.query<HTMLButtonElement>('button').disabled).toBe(
      false
    );
  });

  it('should emit event with current and new pwd when click on update password btn', () => {
    updateChangePwdForm('currentPwd', 'Test');
    updateChangePwdForm('newPwd', 'TestNew');
    updateChangePwdForm('confirmNewPwd', 'TestNew');
    let emitedObj;
    component.changePwd.subscribe((pwdObj: any) => (emitedObj = pwdObj));
    fixture.detectChanges();
    changePwdPage.query<HTMLButtonElement>('button').click();
    expect(emitedObj).toEqual({
      newPwd: 'TestNew',
      currentPwd: 'Test',
    });
  });
});
