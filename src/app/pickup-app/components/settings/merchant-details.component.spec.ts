import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Page } from '../../../pickup-shared/testing/page';
import { MerchantDetailsComponent } from './merchant-details.component';
import { MerchantUserAccount } from '../../../pickup-shared/testing/users.data';

describe('MerchantDetailsComponent', () => {
  let component: MerchantDetailsComponent;
  let fixture: ComponentFixture<MerchantDetailsComponent>;
  let merchantDetailPage: Page<MerchantDetailsComponent>;

  function updateMerchantDetailForm(key: string, value: string | boolean) {
    component.merchantDetailsForm.controls[key].setValue(value);
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        ReactiveFormsModule,
        MatSlideToggleModule,
        BrowserAnimationsModule,
      ],
      declarations: [MerchantDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchantDetailsComponent);
    component = fixture.componentInstance;
    merchantDetailPage = new Page(fixture);
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(merchantDetailPage.query<HTMLButtonElement>('button')).toBeFalsy();
  });

  it('should disable update details button', () => {
    component.details = MerchantUserAccount;
    fixture.detectChanges();
    expect(merchantDetailPage.query<HTMLButtonElement>('button').disabled).toBe(
      true
    );
  });
  it('should keep update details button disabled if website name is given without the website url', () => {
    component.details = MerchantUserAccount;
    component.submitDisabled = false;
    fixture.detectChanges();
    updateMerchantDetailForm('websiteName', 'Test');
    fixture.detectChanges();
    expect(merchantDetailPage.query<HTMLButtonElement>('button').disabled).toBe(
      true
    );
  });

  xit('should enable update details button if user change something and form is correct', () => {
    component.details = MerchantUserAccount;
    fixture.detectChanges();
    updateMerchantDetailForm('sendNotif', true);
    fixture.detectChanges();
    expect(merchantDetailPage.query<HTMLButtonElement>('button').disabled).toBe(
      true
    );
    // expect(component.submitDisabled).toBe(false);
    component.submitDisabled = false;
    fixture.detectChanges();
    expect(merchantDetailPage.query<HTMLButtonElement>('button').disabled).toBe(
      false
    );
  });

  // it('should keep update password button disabled if confirm password not given', () => {
  //   updateChangePwdForm('currentPwd', 'Test');
  //   updateChangePwdForm('newPwd', 'Test');
  //   fixture.detectChanges();
  //   expect(changePwdPage.query<HTMLButtonElement>('button').disabled).toBe(
  //     true
  //   );
  // });

  it('should keep update details button disabled if url is not correct', () => {
    component.details = MerchantUserAccount;
    component.submitDisabled = false;
    fixture.detectChanges();
    updateMerchantDetailForm('websiteName', 'Test');
    fixture.detectChanges();
    updateMerchantDetailForm('websiteUrl', 'http://');
    fixture.detectChanges();
    expect(merchantDetailPage.query<HTMLButtonElement>('button').disabled).toBe(
      true
    );
  });

  xit('should enable update details when all fields are corrects', () => {
    component.details = MerchantUserAccount;
    component.submitDisabled = false;
    fixture.detectChanges();
    expect(component.submitDisabled).toBe(false);
    updateMerchantDetailForm('websiteName', 'Test');
    fixture.detectChanges();
    updateMerchantDetailForm('websiteUrl', 'http://www.test.com');
    fixture.detectChanges();
    expect(merchantDetailPage.query<HTMLButtonElement>('button').disabled).toBe(
      false
    );
  });

  it('should emit event with updated object information', () => {
    component.details = MerchantUserAccount;
    // component.submitDisabled = false;
    // expect(component.submitDisabled).toBe(true);

    fixture.detectChanges();

    // updateMerchantDetailForm('websiteName', 'Test');
    fixture.detectChanges();
    // updateMerchantDetailForm('websiteUrl', 'http://www.test.com');
    // fixture.detectChanges();

    // let emitedObj;
    // component.updatedDetails.subscribe(
    //   (updatedObj: any) => (emitedObj = updatedObj)
    // );
    // fixture.detectChanges();
    // merchantDetailPage.query<HTMLButtonElement>('button').click();
    // expect(emitedObj).toEqual({
    //   websiteUrl: 'http://www.test.com',
    //   websiteName: 'Test',
    //   autoAccept: false,
    //   sendNotif: false
    // });
  });
});
