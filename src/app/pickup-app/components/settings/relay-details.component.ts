import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { UserAccountInterface } from '../../../pickup-shared/model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RelayInterface } from '../../../pickup-shared/model/relay.model';

@Component({
  selector: 'app-relay-details',
  templateUrl: './relay-details.component.html',
  styleUrls: ['./settings.component.scss']
})
export class RelayDetailsComponent implements OnInit {
  constructor(private formBuilder: FormBuilder) {}
  @Input() details: UserAccountInterface;
  @Output() relayDetails = new EventEmitter<RelayInterface>();
  creatingRelay = false;
  relayDetailsForm: FormGroup;

  onSubmitDetails() {
    if (this.details.relayId && this.details.relayId._id) {
      this.relayDetails.emit({
        ...this.relayDetailsForm.value,
        _id: this.details.relayId._id
      });
    } else {
      this.relayDetails.emit(this.relayDetailsForm.value);
    }
  }

  ngOnInit() {
    this.relayDetailsForm = this.formBuilder.group({
      name: [
        this.details.relayId && this.details.relayId.name
          ? this.details.relayId.name
          : '',
        Validators.required
      ],
      address: [
        this.details.relayId && this.details.relayId.address
          ? this.details.relayId.address
          : '',
        Validators.required
      ],
      city: [
        this.details.relayId && this.details.relayId.city
          ? this.details.relayId.city
          : '',
        Validators.required
      ],
      town: [
        this.details.relayId && this.details.relayId.town
          ? this.details.relayId.town
          : '',
        Validators.required
      ],
      holiday: [
        this.details.relayId && this.details.relayId.holiday
          ? this.details.relayId.holiday
          : false
      ]
    });
  }
}
