import { Component, Input } from '@angular/core';
import { UserAccountInterface } from '../../../pickup-shared/model';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./settings.component.scss']
})
export class UserDetailsComponent {
  constructor() {}
  @Input() userInfo: UserAccountInterface;
}
