import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { ChangePasswordInterface } from '../../models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { duplicateInputValidator } from '../../../pickup-shared/validators/duplicate-input.validator';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./settings.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  constructor(private formBuilder: FormBuilder) {}
  @Output() changePwd = new EventEmitter<ChangePasswordInterface>();
  changePwdForm: FormGroup;
  hide = true;
  hideCurrent = true;
  hideConfirm = true;

  onChangePwd() {
    this.changePwd.emit({
      currentPwd: this.changePwdForm.value.currentPwd,
      newPwd: this.changePwdForm.value.newPwd
    });
  }

  ngOnInit() {
    this.changePwdForm = this.formBuilder.group({
      currentPwd: ['', Validators.required],
      newPwd: ['', Validators.required],
      confirmNewPwd: [
        '',
        [
          Validators.required,
          duplicateInputValidator('newPwd', 'confirmNewPwd')
        ]
      ]
    });
  }
}
