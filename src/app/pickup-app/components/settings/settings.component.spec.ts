import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsComponent } from './settings.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { UserDetailsComponent } from './user-details.component';
import { ChangePasswordComponent } from './change-password.component';
import { MerchantDetailsComponent } from './merchant-details.component';
import { RelayDetailsComponent } from './relay-details.component';

describe('SettingsComponent', () => {
  let component: SettingsComponent;
  let fixture: ComponentFixture<SettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SettingsComponent,
        UserDetailsComponent,
        ChangePasswordComponent,
        MerchantDetailsComponent,
        RelayDetailsComponent,
      ],
      imports: [MatExpansionModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
