import { Component, OnInit, Input } from '@angular/core';
import { PickupUserInfoInterface } from '../../../pickup-shared/model/user.model';

@Component({
  selector: 'app-pickup-user',
  templateUrl: './pickup-user.component.html',
  styleUrls: ['./pickup-user.component.scss']
})
export class PickupUserComponent implements OnInit {
  constructor() {}
  @Input() userInfo: PickupUserInfoInterface;
  ngOnInit() {}
}
