import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { INITIALIZED_PACKAGES_FORM_BUILDER } from '../../pickup-app.constant';

import { Store } from '@ngrx/store';

import * as fromPickupStore from '../../store';
import * as fromSharedStore from '../../../pickup-shared/store';
import { Observable } from 'rxjs';
import { take, filter } from 'rxjs/operators';
import { CreatePickupInterface, UserRelayInterface } from '../../models';
import { BroadcasterService } from '../../../pickup-shared/services';
import { PickupEvent } from '../../../pickup-shared/model';
import {
  toIdValidator,
  phoneValidator,
} from '../../../pickup-shared/validators';

@Component({
  selector: 'app-pickup-create-view',
  templateUrl: './create-pickup-view.component.html',
  styleUrls: ['./create-pickup-view.component.scss'],
})
export class CreatePickupViewComponent implements OnInit, OnDestroy {
  constructor(
    private formBuilder: FormBuilder,
    private pickupStore: Store<fromPickupStore.AppPickupState>,
    private sharedStore: Store<fromSharedStore.AppSharedState>,
    private broadcastService: BroadcasterService
  ) {
    this.createPickupForm();
  }

  messages: PickupEvent[] = [];
  messages$ = this.broadcastService._eventBus
    .pipe(filter((event: PickupEvent) => event.type === 'error'))
    .subscribe((event) => {
      this.messages.push(event);
    });

  pickupCreationForm: FormGroup;
  packages = INITIALIZED_PACKAGES_FORM_BUILDER;

  charge$: Observable<number>;
  relays$: Observable<UserRelayInterface[]>;

  createPickupForm() {
    this.pickupCreationForm = this.formBuilder.group(
      {
        toId: [''],
        packages: this.formBuilder.group(
          this.packages.reduce((accum, current) => {
            return {
              ...accum,
              [current.name]: this.formBuilder.group({
                fragile: current.fragile,
                quantity: [current.quantity, Validators.required],
              }),
            };
          }, {})
        ),
        endUser: this.formBuilder.group({
          firstname: ['', Validators.required],
          lastname: ['', Validators.required],
          phone: ['', [Validators.required, phoneValidator()]],
          address: ['', Validators.required],
          city: ['', Validators.required],
          town: ['', Validators.required],
        }),
        useRelay: [false, Validators.required],
        isFree: [false, Validators.required],
      },
      { validator: toIdValidator() }
    );
  }

  onSubmit() {
    this.messages = [];
    const atLeastOnPack = Object.keys(
      this.pickupCreationForm.value.packages
    ).some(
      (elem) => this.pickupCreationForm.value.packages[elem]['quantity'] > 0
    );

    if (atLeastOnPack) {
      this.pickupStore.dispatch(
        new fromPickupStore.SimulatePickupCreation(
          this.pickupCreationForm.value
        )
      );
    } else {
      this.messages.push({
        type: 'error',
        description: 'You need to give at least one package',
        message: 'pickup.create.packageEmpty',
      });
    }
  }

  packChanged(event) {
    this.pickupStore.dispatch(new fromPickupStore.PickupPackageChanged());
  }

  createPickup() {
    const { ['toId']: toId, ...withOutRelayId } = this.pickupCreationForm.value;

    this.sharedStore
      .select(fromSharedStore.getUserId)
      .pipe(take(1))
      .subscribe((fromId) => {
        const sentData: CreatePickupInterface = this.pickupCreationForm.value
          .useRelay
          ? { ...this.pickupCreationForm.value, fromId }
          : { ...withOutRelayId, fromId };
        this.pickupStore.dispatch(new fromPickupStore.CreatePickup(sentData));
      });
  }

  ngOnInit() {
    this.charge$ = this.pickupStore.select(fromPickupStore.getSimulationCharge);
    this.pickupStore.dispatch(new fromPickupStore.LoadUsersRelay());
    this.relays$ = this.pickupStore.select(fromPickupStore.getUsersRelay);
  }

  ngOnDestroy() {
    this.messages$.unsubscribe();
  }
}
