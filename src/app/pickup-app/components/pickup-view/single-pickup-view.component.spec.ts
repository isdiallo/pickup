import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglePickupViewComponent } from './single-pickup-view.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { PickupUserComponent } from './pickup-user.component';
import { ShowCodeDirective } from '../pickups/show-code.directive';
import { UserNamePipe } from '../../../pickup-shared/pipes/user.pipe';
import { Store, StoreModule, combineReducers } from '@ngrx/store';
import * as fromPickupStore from '../../store';
import * as fromSharedStore from '../../../pickup-shared/store';
import * as fromActions from '../../store/actions';
import * as fromSharedActions from '../../../pickup-shared/store/actions';
import * as fromRoot from '../../../store';
import { BroadcasterService } from '../../../pickup-shared/services';
import { ActivatedRoute } from '@angular/router';
import { Page } from '../../../pickup-shared/testing/page';
import { PICKUPS } from '../../../pickup-shared/testing/pickups.data';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserType } from '../../../pickup-shared/model';

describe('SinglePickupViewComponent', () => {
  let component: SinglePickupViewComponent;
  let fixture: ComponentFixture<SinglePickupViewComponent>;
  let store: Store<fromPickupStore.AppPickupState>;
  let pickupViewPage: Page<SinglePickupViewComponent>;

  function updateStore() {
    store.dispatch(
      new fromActions.LoadPickupsSuccess({
        data: PICKUPS,
        success: {
          description: 'load pickups success',
          message: 'load.pickups.success',
        },
      })
    );
    store.dispatch(
      new fromSharedActions.GetIdentitySuccess({
        success: { description: '', message: '' },
        data: {
          _id: 'fgdsfg5sdgfsdfg',
          _type: UserType.Merchant,
          updatedAt: '',
          createdAt: '',
          firstname: 'Merch Test',
          lastname: 'Merch Last',
          username: 'merchant@test.fr',
          phone: '145214587',
          mail: 'merchant@test.fr',
          city: 'Merch City',
          town: 'Merch town',
          address: 'Merch address',
        },
      })
    );
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SinglePickupViewComponent,
        PickupUserComponent,
        ShowCodeDirective,
        UserNamePipe,
      ],
      imports: [
        MatExpansionModule,
        MatFormFieldModule,
        MatCardModule,
        BrowserAnimationsModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          appPickup: combineReducers(fromPickupStore.reducers),
          appShared: combineReducers(fromSharedStore.reducers),
        }),
      ],
      providers: [
        BroadcasterService,
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: {
                pickupId: '5a8dccc3aec7ea08284104a2',
              },
            },
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglePickupViewComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);
    updateStore();
    pickupViewPage = new Page(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should show the 5a8dccc3aec7ea08284104a2 pickup', () => {
    expect(
      pickupViewPage.query<HTMLElement>('.single-pickup-content')
    ).toBeTruthy();
  });
  it('should only show Merchant and End User info', () => {
    expect(
      pickupViewPage.queryAll<HTMLElement>('mat-expansion-panel').length
    ).toBe(2);
    expect(
      pickupViewPage.query<HTMLElement>(
        'mat-expansion-panel:first-child mat-expansion-panel-header mat-panel-title'
      ).innerText
    ).toBe('Merchant User');
    expect(
      pickupViewPage.query<HTMLElement>(
        'mat-expansion-panel:last-child mat-expansion-panel-header mat-panel-title'
      ).innerText
    ).toBe('End User');
    expect(
      pickupViewPage.query<HTMLElement>(
        '.single-pickup-entry:nth-child(1) > span:last-child'
      ).innerText
    ).toBe('Wed 21 Feb 19:47');
    expect(
      pickupViewPage.query<HTMLElement>(
        '.single-pickup-entry:nth-child(2) > span:last-child'
      ).innerText
    ).toBe('Wed 21 Feb 19:47');
    expect(
      pickupViewPage.query<HTMLElement>(
        '.single-pickup-entry:nth-child(3) > span:last-child'
      ).innerText
    ).toBe('VALIDATED');
    expect(
      pickupViewPage.query<HTMLElement>(
        '.single-pickup-entry:nth-child(4) > span:last-child'
      ).innerText
    ).toBe('1');
    expect(
      pickupViewPage.query<HTMLElement>(
        '.single-pickup-entry:nth-child(5) > span:last-child'
      ).innerText
    ).toBe('15000 GNF');
    expect(pickupViewPage.queryAll<HTMLElement>('button').length).toBe(1); // History btn
  });
});
