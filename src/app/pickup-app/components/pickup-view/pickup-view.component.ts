import { Component, OnInit, OnDestroy } from '@angular/core';

import { Store } from '@ngrx/store';
import * as fromPickupStore from '../../store';
import { PickupInterface } from '../../../pickup-shared/model/pickup.model';
import { Observable } from 'rxjs';
import { ValidateCodeEventInterface } from '../../models';
import { BroadcasterService } from '../../../pickup-shared/services';
import { filter } from 'rxjs/operators';
import { PickupEvent } from '../../../pickup-shared/model';

@Component({
  selector: 'app-pickup-view',
  templateUrl: './pickup-view.component.html',
  styleUrls: ['./pickup-view.component.scss'],
})
export class PickupViewComponent implements OnInit, OnDestroy {
  constructor(
    private pickupStore: Store<fromPickupStore.AppPickupState>,
    private broadcastService: BroadcasterService
  ) {}

  visualizedPickups$: Observable<PickupInterface[]>;

  messages: PickupEvent[] = [];
  messages$ = this.broadcastService._eventBus
    .pipe(filter((event: PickupEvent) => event.type === 'error'))
    .subscribe((event) => {
      this.messages.push(event);
    });

  validatePickup(event: string) {
    this.pickupStore.dispatch(new fromPickupStore.ValidatePickup(event));
  }

  sendCode(event: string) {
    this.pickupStore.dispatch(new fromPickupStore.SendCode(event));
  }

  validateCode(event: ValidateCodeEventInterface) {
    this.pickupStore.dispatch(
      new fromPickupStore.ValidateCode(event._id, event.code)
    );
  }

  ngOnInit() {
    this.pickupStore.dispatch(new fromPickupStore.LoadPickups());
    this.visualizedPickups$ = this.pickupStore.select(
      fromPickupStore.getVisualizedPickups
    );
  }

  ngOnDestroy() {
    this.messages$.unsubscribe();
  }
}
