import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickupViewComponent } from './pickup-view.component';
import { PickupsListComponent } from '../pickups/pickups-list.component';
import { PickupsComponent } from '../pickups/pickups.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { HasAuthorityDirective } from '../../../pickup-shared/has-authority.directive';
import { AlertListComponent } from '../../../pickup-shared/components/alert-list/alert-list.component';
import { UserNamePipe } from '../../../pickup-shared/pipes/user.pipe';
import { RouterModule } from '@angular/router';
import { ShowCodeDirective } from '../pickups/show-code.directive';
import { AlertComponent } from '../../../pickup-shared/components/alert/alert.component';
import * as fromPickupStore from '../../store';
import * as fromSharedStore from '../../../pickup-shared/store';
import * as fromActions from '../../store/actions';
import * as fromSharedActions from '../../../pickup-shared/store/actions';
import * as fromRoot from '../../../store';
import { StoreModule, combineReducers, Store } from '@ngrx/store';
import { BroadcasterService } from '../../../pickup-shared/services';
import { Page } from '../../../pickup-shared/testing/page';
import { PICKUPS } from '../../../pickup-shared/testing/pickups.data';
import { UserType } from '../../../pickup-shared/model';

describe('PickupViewComponent', () => {
  let component: PickupViewComponent;
  let fixture: ComponentFixture<PickupViewComponent>;
  let pickupStore: Store<fromPickupStore.AppPickupState>;
  let pickupViewPage: Page<PickupViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PickupViewComponent,
        PickupsListComponent,
        PickupsComponent,
        AlertListComponent,
        AlertComponent,
        UserNamePipe,
        ShowCodeDirective,
        HasAuthorityDirective,
      ],
      imports: [
        MatIconModule,
        RouterModule,
        MatFormFieldModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          appPickup: combineReducers(fromPickupStore.reducers),
          appShared: combineReducers(fromSharedStore.reducers),
        }),
      ],
      providers: [BroadcasterService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickupViewComponent);
    component = fixture.componentInstance;
    pickupViewPage = new Page(fixture);
    pickupStore = TestBed.get(Store);
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should not have pickups at initialization if the store empty', () => {
    fixture.detectChanges();
    expect(pickupViewPage.query<HTMLElement>('.pickups-list')).toBeFalsy();
  });

  it('should have 2 pickups at initialization', () => {
    pickupStore.dispatch(
      new fromActions.LoadPickupsSuccess({
        data: PICKUPS,
        success: {
          description: 'load pickups success',
          message: 'load.pickups.success',
        },
      })
    );
    pickupStore.dispatch(
      new fromSharedActions.GetIdentitySuccess({
        success: { description: '', message: '' },
        data: {
          _id: 'fgdsfg5sdgfsdfg',
          _type: UserType.Merchant,
          updatedAt: '',
          createdAt: '',
          firstname: 'Merch Test',
          lastname: 'Merch Last',
          username: 'merchant@test.fr',
          phone: '145214587',
          mail: 'merchant@test.fr',
          city: 'Merch City',
          town: 'Merch town',
          address: 'Merch address',
        },
      })
    );
    pickupStore.dispatch({
      type: 'ROUTER_NAVIGATION',
      payload: {
        routerState: {
          url: '/app/pickups/validated',
          queryParams: {},
          params: {},
          data: {
            name: 'validated',
            authorities: ['Merchant'],
          },
        },
        event: {},
      },
    });

    fixture.detectChanges();
    expect(pickupViewPage.query<HTMLElement>('.pickups-list')).toBeTruthy();
    expect(pickupViewPage.queryAll<HTMLElement>('.pickups-list').length).toBe(
      1
    );
    expect(
      pickupViewPage.queryAll<HTMLElement>('.pickup-container').length
    ).toBe(2);
  });
});
