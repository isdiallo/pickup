import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickupUserComponent } from './pickup-user.component';
import { UserNamePipe } from '../../../pickup-shared/pipes/user.pipe';
import { Page } from '../../../pickup-shared/testing/page';
import {
  UserMerchantInfo,
  UserRelayInfo,
  UserAgentInfo
} from '../../../pickup-shared/testing/users.data';

describe('PickupUserComponent', () => {
  let component: PickupUserComponent;
  let fixture: ComponentFixture<PickupUserComponent>;
  let pickupUserPage: Page<PickupUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PickupUserComponent, UserNamePipe]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickupUserComponent);
    component = fixture.componentInstance;
    pickupUserPage = new Page(fixture);
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should create with a merchant user', () => {
    component.userInfo = UserMerchantInfo;
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(1) > span:last-child'
      ).innerText
    ).toBe('Test Merch L.');
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(2) > span:last-child'
      ).innerText
    ).toBe('125412547');
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(3) > span:last-child'
      ).innerText
    ).toBe('www.merchant.com');
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(4) > span:last-child'
      ).innerText
    ).toBe('Merch address');
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(5) > span:last-child'
      ).innerText
    ).toBe('Merch city');
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(6) > span:last-child'
      ).innerText
    ).toBe('Merch town');
  });

  it('should create with a relay user', () => {
    component.userInfo = UserRelayInfo;
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(1) > span:last-child'
      ).innerText
    ).toBe('Test User R L.');
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(2) > span:last-child'
      ).innerText
    ).toBe('125412547');
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(3) > span:last-child'
      ).innerText
    ).toBe('Relay Test');
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(4) > span:last-child'
      ).innerText
    ).toBe('Relay address');
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(5) > span:last-child'
      ).innerText
    ).toBe('Relay city');
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(6) > span:last-child'
      ).innerText
    ).toBe('Relay town');
  });

  it('should create with an agent user', () => {
    component.userInfo = UserAgentInfo;
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(1) > span:last-child'
      ).innerText
    ).toBe('Test Agent L.');
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(2) > span:last-child'
      ).innerText
    ).toBe('125412547');
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(3) > span:last-child'
      ).innerText
    ).toBe('Agent address');
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(4) > span:last-child'
      ).innerText
    ).toBe('Agent city');
    expect(
      pickupUserPage.query<HTMLElement>(
        '.user-entry:nth-child(5) > span:last-child'
      ).innerText
    ).toBe('Agent town');
  });
});
