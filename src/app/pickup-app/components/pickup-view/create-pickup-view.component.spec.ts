import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { CreatePickupViewComponent } from './create-pickup-view.component';
import { AlertListComponent } from '../../../pickup-shared/components/alert-list/alert-list.component';
import {
  MatFormFieldModule,
  MatSlideToggleModule,
  MatCardModule,
  MatSelectModule,
  MatIconModule,
  MatTooltipModule,
  MatInputModule
} from '@angular/material';
import { PickupSizeComponent } from '../pickup-size/pickup-size.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertComponent } from '../../../pickup-shared/components/alert/alert.component';
import { Store, StoreModule, combineReducers } from '@ngrx/store';
import * as fromPickupStore from '../../store';
import * as fromSharedStore from '../../../pickup-shared/store';
import * as fromRoot from '../../../store';
import * as fromActions from '../../../pickup-app/store/actions';
import { BroadcasterService } from '../../../pickup-shared/services';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Page } from '../../../pickup-shared/testing/page';

describe('CreatePickupViewComponent', () => {
  let component: CreatePickupViewComponent;
  let fixture: ComponentFixture<CreatePickupViewComponent>;
  let pickupStore: Store<fromPickupStore.AppPickupState>;
  let createPickupViewPage: Page<CreatePickupViewComponent>;

  function updatePickupCreationForm(key: string, value: any) {
    component.pickupCreationForm.controls[key].setValue(value);
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CreatePickupViewComponent,
        PickupSizeComponent,
        AlertListComponent,
        AlertComponent
      ],
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatSlideToggleModule,
        MatCardModule,
        MatSelectModule,
        MatIconModule,
        MatTooltipModule,
        MatInputModule,
        BrowserAnimationsModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          appPickup: combineReducers(fromPickupStore.reducers),
          appShared: combineReducers(fromSharedStore.reducers)
        })
      ],
      providers: [BroadcasterService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePickupViewComponent);
    createPickupViewPage = new Page(fixture);
    component = fixture.componentInstance;
    pickupStore = TestBed.get(Store);
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should initialize the pickup creation form', () => {
    fixture.detectChanges();
    expect(component.pickupCreationForm.value).toEqual({
      toId: '',
      packages: {
        XL: { fragile: false, quantity: 0 },
        L: { fragile: false, quantity: 0 },
        M: { fragile: false, quantity: 0 },
        S: { fragile: false, quantity: 0 }
      },
      endUser: {
        firstname: '',
        lastname: '',
        phone: '',
        address: '',
        city: '',
        town: ''
      },
      useRelay: false,
      isFree: false
    });
  });

  it('should disable the show price button at initialization', () => {
    fixture.detectChanges();
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.update-btn').disabled
    ).toBe(true);
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.create-btn')
    ).toBeFalsy();
  });

  it('should not show the show price button if the pickup is free of charge, should disabled the create btn', () => {
    updatePickupCreationForm('isFree', true);
    fixture.detectChanges();
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.update-btn')
    ).toBeFalsy();
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.create-btn').disabled
    ).toBe(true);
  });

  it('should keep show price button disabled if end user if is missing', () => {
    updatePickupCreationForm('endUser', {
      firstname: 'Test User',
      lastname: '',
      phone: '',
      address: '',
      city: '',
      town: ''
    });
    fixture.detectChanges();
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.update-btn').disabled
    ).toBe(true);
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.create-btn')
    ).toBeFalsy();
  });

  it('should keep show price button disabled if end user phone if is missing info is incorrect', () => {
    updatePickupCreationForm('endUser', {
      firstname: 'Test User',
      lastname: 'Test last',
      phone: '125421',
      address: 'Tes address',
      city: 'test city',
      town: 'test town'
    });
    fixture.detectChanges();
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.update-btn').disabled
    ).toBe(true);
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.create-btn')
    ).toBeFalsy();
  });

  it('should show price button enabled if end user info is correct', () => {
    updatePickupCreationForm('endUser', {
      firstname: 'Test User',
      lastname: 'Test last',
      phone: '125421479',
      address: 'Tes address',
      city: 'test city',
      town: 'test town'
    });
    fixture.detectChanges();
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.update-btn').disabled
    ).toBe(false);
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.create-btn')
    ).toBeFalsy();
  });

  it('should show error message when click on show price without giving at least on package', () => {
    updatePickupCreationForm('endUser', {
      firstname: 'Test User',
      lastname: 'Test last',
      phone: '125421479',
      address: 'Tes address',
      city: 'test city',
      town: 'test town'
    });
    fixture.detectChanges();
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.update-btn').disabled
    ).toBe(false);
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.create-btn')
    ).toBeFalsy();
    createPickupViewPage.query<HTMLButtonElement>('.update-btn').click();
    fixture.detectChanges();
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.update-btn').disabled
    ).toBe(false);
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.create-btn')
    ).toBeFalsy();
    expect(
      createPickupViewPage.query<HTMLElement>('.message.error')
    ).toBeTruthy();
  });

  it('should show the price when click on show price', () => {
    updatePickupCreationForm('endUser', {
      firstname: 'Test User',
      lastname: 'Test last',
      phone: '125421479',
      address: 'Tes address',
      city: 'test city',
      town: 'test town'
    });
    updatePickupCreationForm('packages', {
      XL: { fragile: false, quantity: 1 },
      L: { fragile: false, quantity: 0 },
      M: { fragile: false, quantity: 0 },
      S: { fragile: false, quantity: 0 }
    });
    fixture.detectChanges();
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.update-btn').disabled
    ).toBe(false);
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.create-btn')
    ).toBeFalsy();
    createPickupViewPage.query<HTMLButtonElement>('.update-btn').click();
    const action = new fromActions.SimulatePickupCreationSuccess({
      success: {
        description: 'simulate pickup creation success',
        message: 'simulate.pickup.creation.success'
      },
      data: { charge: 1500 }
    });
    pickupStore.dispatch(action);
    fixture.detectChanges();

    expect(
      createPickupViewPage.query<HTMLButtonElement>('.update-btn').disabled
    ).toBe(false);
    expect(
      createPickupViewPage.query<HTMLButtonElement>('.create-btn').disabled
    ).toBe(false);
    expect(
      createPickupViewPage.query<HTMLSpanElement>('.price').innerText
    ).toBe('1500 GNF');
  });
});
