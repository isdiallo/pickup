import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PickupInterface } from '../../../pickup-shared/model/pickup.model';
import { Observable } from 'rxjs';

import { Store } from '@ngrx/store';

import * as fromPickupStore from '../../store';
import { BroadcasterService } from '../../../pickup-shared/services';
import { PickupEvent } from '../../../pickup-shared/model';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-single-pickup-view',
  templateUrl: './single-pickup-view.component.html',
  styleUrls: ['./single-pickup-view.component.scss'],
})
export class SinglePickupViewComponent implements OnInit, OnDestroy {
  constructor(
    private pickupStore: Store<fromPickupStore.AppPickupState>,
    private broadcastService: BroadcasterService,
    private route: ActivatedRoute
  ) {}

  panelOpenState = false;
  isShowCode = false;
  pickup$: Observable<PickupInterface>;
  messages: PickupEvent[] = [];

  messages$ = this.broadcastService._eventBus
    .pipe(filter((event: PickupEvent) => event.type === 'error'))
    .subscribe((event) => {
      this.messages.push(event);
    });

  ngOnInit() {
    this.pickup$ = this.pickupStore.select(
      fromPickupStore.getSelectedPickup(this.route.snapshot.params.pickupId)
    );
  }

  showHistory() {
    console.log('Showing History');
  }

  validatePickup() {
    this.pickupStore.dispatch(
      new fromPickupStore.ValidatePickup(this.route.snapshot.params.pickupId)
    );
  }

  getNbPacks(pickup: PickupInterface): number {
    return pickup.packages.reduce((accum, currentVal) => {
      return accum + currentVal.quantity;
    }, 0);
  }

  sendCode() {
    if (!this.isShowCode) {
      this.pickupStore.dispatch(
        new fromPickupStore.SendCode(this.route.snapshot.params.pickupId)
      );
    }
    this.isShowCode = !this.isShowCode;
  }

  validateCode(code: string) {
    if (code) {
      this.pickupStore.dispatch(
        new fromPickupStore.ValidateCode(
          this.route.snapshot.params.pickupId,
          code
        )
      );
    }
  }

  ngOnDestroy() {
    this.messages$.unsubscribe();
  }
}
