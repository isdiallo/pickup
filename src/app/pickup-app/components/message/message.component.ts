import { Component, Input, OnInit } from '@angular/core';

import { MessageInterface } from '../../models';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
  constructor() {}
  @Input() message: MessageInterface;
  ngOnInit() {}
}
