import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageComponent } from './message.component';
import { Page } from '../../../pickup-shared/testing/page';

describe('MessageComponent', () => {
  let component: MessageComponent;
  let fixture: ComponentFixture<MessageComponent>;
  let messagePage: Page<MessageComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [MessageComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageComponent);
    component = fixture.componentInstance;
    messagePage = new Page(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not render if no message', () => {
    expect(messagePage.query<HTMLElement>('.msg-container')).toBeFalsy();
    expect(messagePage.query<HTMLElement>('.sender')).toBeFalsy();
  });

  it('should render only message content not the sender', () => {
    component.message = {
      content: 'Test Message',
      createdAt: '2017-12-10T16:10:54.004Z'
    };
    fixture.detectChanges();
    expect(messagePage.query<HTMLElement>('.msg-container')).toBeTruthy();
    expect(messagePage.query<HTMLElement>('.sender')).toBeFalsy();
    expect(messagePage.query<HTMLElement>('.content').textContent).toBe(
      'Test Message'
    );
    expect(messagePage.query<HTMLElement>('.time').textContent).toBe(
      'Sun 10 Dec 16:10'
    );
  });

  it('should render message content and sender', () => {
    component.message = {
      content: 'Test Message',
      createdAt: '2017-12-10T16:10:54.004Z',
      from: { firstname: 'Test First', lastname: 'Test Last' }
    };
    fixture.detectChanges();
    expect(messagePage.query<HTMLElement>('.msg-container')).toBeTruthy();
    expect(messagePage.query<HTMLElement>('.sender')).toBeTruthy();
    expect(messagePage.query<HTMLElement>('.content').textContent).toBe(
      'Test Message'
    );
    expect(messagePage.query<HTMLElement>('.time').textContent).toBe(
      'Sun 10 Dec 16:10'
    );
    expect(
      messagePage.query<HTMLElement>('.sender > span:nth-child(1)').textContent
    ).toBe('Test First');
    expect(
      messagePage.query<HTMLElement>('.sender > span:nth-child(2)').textContent
    ).toBe('Test Last');
  });
});
