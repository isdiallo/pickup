import { AdminPickupComponent } from './admin-pickup.component';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { WindowRefService } from '../../../pickup-shared/services';
import { UserNamePipe } from '../../../pickup-shared/pipes/user.pipe';
import { Page } from '../../../pickup-shared/testing/page';
import { PickupToBeValidated } from '../../../pickup-shared/testing/pickups.data';

describe('AdminPickupComponent', () => {
  let component: AdminPickupComponent;
  let fixture: ComponentFixture<AdminPickupComponent>;
  let adminPickupPage: Page<AdminPickupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminPickupComponent, UserNamePipe],
      imports: [],
      providers: [WindowRefService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPickupComponent);
    component = fixture.componentInstance;
    adminPickupPage = new Page(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(adminPickupPage.query<HTMLElement>('.admin-pickup')).toBeFalsy();
  });

  it('should show the pickup correctly the pickup', () => {
    component.pickup = PickupToBeValidated;
    fixture.detectChanges();
    expect(
      adminPickupPage.query<HTMLButtonElement>('button').disabled
    ).toBeFalsy();
    expect(
      adminPickupPage.query<HTMLElement>('.misc .created-at span:nth-child(1)')
        .textContent
    ).toBe('Thursday');
    expect(
      adminPickupPage.query<HTMLElement>('.misc .created-at span:nth-child(2)')
        .textContent
    ).toBe('01 Mar');
    expect(
      adminPickupPage.query<HTMLElement>('.misc .created-at span:nth-child(3)')
        .textContent
    ).toBe('19:56');
    expect(adminPickupPage.query<HTMLElement>('.misc .fragile').innerText).toBe(
      'Fragile'
    );
    expect(
      adminPickupPage.query<HTMLElement>(
        '.main .main-entry:nth-child(1) span:nth-child(2)'
      ).innerText
    ).toBe(
      'Merchant test M. - 147854125 - merchant add-merchant town-merchant city'
    );
    expect(
      adminPickupPage.query<HTMLElement>(
        '.main .main-entry:nth-child(2) span:nth-child(2)'
      ).innerText
    ).toBe(
      'end User first E. - 123456789 - No Where Address-No Where Address-No Where Address'
    );
    expect(
      adminPickupPage.query<HTMLElement>(
        '.main .main-entry:nth-child(3) span:nth-child(2)'
      ).innerText
    ).toBe('1');
  });
});
