import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPickupValidationComponent } from './admin-pickup-validation.component';
import { AdminPickupComponent } from './admin-pickup.component';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { UserNamePipe } from '../../../pickup-shared/pipes/user.pipe';
import { StoreModule, Store, combineReducers } from '@ngrx/store';
// import { reducers } from '../../store';

import * as fromPickupStore from '../../store';
import * as fromRoot from '../../store';
import {
  BroadcasterService,
  WindowRefService,
} from '../../../pickup-shared/services';
import { AlertComponent } from '../../../pickup-shared/components/alert/alert.component';
import { AlertListComponent } from '../../../pickup-shared/components/alert-list/alert-list.component';
import { of } from 'rxjs';
import { Page } from '../../../pickup-shared/testing/page';
import { PickupToBeValidated } from '../../../pickup-shared/testing/pickups.data';
import { UserAgentAccount } from '../../../pickup-shared/testing/users.data';

describe('AdminPickupValidationComponent', () => {
  let component: AdminPickupValidationComponent;
  let fixture: ComponentFixture<AdminPickupValidationComponent>;
  let pickupStore: Store<fromPickupStore.AppPickupState>;
  let adminPickupValidationPage: Page<AdminPickupValidationComponent>;
  let assignationDialog: MatDialog;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AdminPickupValidationComponent,
        AdminPickupComponent,
        AlertComponent,
        AlertListComponent,
        UserNamePipe,
      ],
      imports: [
        MatCheckboxModule,
        MatDialogModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          appPickup: combineReducers(fromPickupStore.reducers),
        }),
      ],
      providers: [BroadcasterService, WindowRefService],
    }).compileComponents();
  }));

  beforeEach(() => {
    pickupStore = TestBed.get(Store);
    assignationDialog = TestBed.get(MatDialog);
    fixture = TestBed.createComponent(AdminPickupValidationComponent);
    spyOn(pickupStore, 'dispatch').and.callThrough();
    spyOn(assignationDialog, 'open').and.callThrough();
    component = fixture.componentInstance;
    adminPickupValidationPage = new Page(fixture);
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(
      adminPickupValidationPage.query<HTMLButtonElement>('button').disabled
    ).toBe(true);
  });

  it('should have Assign to an Agent button enabled if a pickup is selected', () => {
    fixture.detectChanges();
    expect(
      adminPickupValidationPage.query<HTMLElement>('.admin-entry')
    ).toBeFalsy();

    pickupStore.dispatch(
      new fromPickupStore.LoadPickupsSuccess({
        success: { message: '', description: '' },
        data: [PickupToBeValidated],
      })
    );

    fixture.detectChanges();
    expect(
      adminPickupValidationPage.query<HTMLElement>('.admin-entry')
    ).toBeTruthy();
    expect(
      adminPickupValidationPage.query<HTMLElement>('mat-checkbox')
    ).toBeTruthy();
    adminPickupValidationPage
      .query<HTMLElement>('.mat-checkbox-inner-container')
      .click();
    fixture.detectChanges();
    expect(
      adminPickupValidationPage.query<HTMLButtonElement>(
        '.admin-pickup-validation button:nth-child(1)'
      ).disabled
    ).toBe(false);
    expect(component.selectedPickups.length).toBe(1);
    expect(component.selectedPickups[0]).toBe(PickupToBeValidated._id);
  });

  it('should open a popup to assign pickup to an agent', () => {
    pickupStore.dispatch(
      new fromPickupStore.LoadPickupsSuccess({
        success: { message: '', description: '' },
        data: [PickupToBeValidated],
      })
    );
    pickupStore.dispatch(
      new fromPickupStore.LoadUsersAgentSuccess({
        success: { description: '', message: '' },
        data: [UserAgentAccount],
      })
    );
    fixture.detectChanges();
    adminPickupValidationPage
      .query<HTMLElement>('.mat-checkbox-inner-container')
      .click();
    adminPickupValidationPage
      .query<HTMLElement>('.admin-pickup-validation button:nth-child(1)')
      .click();

    fixture.detectChanges();
    // expect(assignationDialog).toHaveBeenCalled();
  });
});
