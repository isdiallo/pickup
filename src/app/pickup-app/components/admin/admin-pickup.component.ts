import { Component, Input } from '@angular/core';
import { PickupInterface } from '../../../pickup-shared/model/pickup.model';
import { WindowRefService } from '../../../pickup-shared/services/window-ref.service';

@Component({
  selector: 'app-admin-pickup',
  templateUrl: './admin-pickup.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminPickupComponent {
  constructor(private window: WindowRefService) {}
  @Input() pickup: PickupInterface;

  openInNewTab(pickupId: string) {
    this.window
      .getNativeWindow()
      .open(this.window.getNativeWindow().origin + '/app/pickup/' + pickupId);
  }
  isFragile(): boolean {
    return this.pickup.packages.some(pack => pack.fragile);
  }
}
