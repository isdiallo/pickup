import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Observable } from 'rxjs';

import { Store } from '@ngrx/store';

import * as fromPickupStore from '../../store';
import { PickupInterface } from '../../../pickup-shared/model/pickup.model';
import { UserInterface } from '../../models';
import { take, filter } from 'rxjs/operators';

import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AgentAssignationComponent } from './agent-assignation.component';
import { MODAL_MIN_WIDTH } from '../../../pickup-shared/pickup-shared.constants';
import { BroadcasterService } from '../../../pickup-shared/services';
import { PickupEvent } from '../../../pickup-shared/model';

@Component({
  selector: 'app-admin-pickup-validation',
  templateUrl: './admin-pickup-validation.component.html',
  styleUrls: ['./admin-pickup-validation.component.scss'],
})
export class AdminPickupValidationComponent implements OnInit, OnDestroy {
  constructor(
    private pickupStore: Store<fromPickupStore.AppPickupState>,
    private assignationDialog: MatDialog,
    private broadcastService: BroadcasterService
  ) {}
  selectedPickups: string[] = [];
  toBeValidatedPickups$: Observable<PickupInterface[]>;
  agents$: Observable<UserInterface[]>;
  messages: PickupEvent[] = [];

  messages$ = this.broadcastService._eventBus
    .pipe(filter((event: PickupEvent) => event.type === 'error'))
    .subscribe((event) => {
      this.messages.push(event);
    });

  pickupSelected(pickupId, event) {
    if (this.selectedPickups.includes(pickupId)) {
      this.selectedPickups = this.selectedPickups.filter(
        (elem) => elem !== pickupId
      );
    } else {
      this.selectedPickups.push(pickupId);
    }
  }

  assignToAgent() {
    this.agents$.pipe(take(1)).subscribe((agents) => {
      // Agents should be loaded at the init, if not reload it
      if (agents.length === 0) {
        this.pickupStore.dispatch(new fromPickupStore.LoadUsersAgent());
      } else {
        const assignationDialogRef = this.assignationDialog.open(
          AgentAssignationComponent,
          {
            data: { agents: agents },
            minWidth: MODAL_MIN_WIDTH,
          }
        );
        assignationDialogRef.afterClosed().subscribe((selectedAgent) => {
          this.pickupStore.dispatch(
            new fromPickupStore.AssignPickups({
              pickups: this.selectedPickups,
              to: selectedAgent,
            })
          );
        });
      }
    });
  }

  ngOnInit() {
    this.toBeValidatedPickups$ = this.pickupStore.select(
      fromPickupStore.getPickupByState('VALIDATED')
    );

    this.pickupStore.dispatch(new fromPickupStore.LoadUsersAgent());
    this.agents$ = this.pickupStore.select(fromPickupStore.getUsersAgent);
  }

  ngOnDestroy() {
    this.messages$.unsubscribe();
  }
}
