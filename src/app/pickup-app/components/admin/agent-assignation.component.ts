import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-agent-assignation',
  templateUrl: './agent-assignation.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AgentAssignationComponent {
  constructor(
    private assignationDialogRef: MatDialogRef<AgentAssignationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  onNoClick(): void {
    this.assignationDialogRef.close();
  }
}
