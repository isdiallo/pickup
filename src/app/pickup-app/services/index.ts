import { PickupService } from './pickups.service';
import { UsersService } from './users.service';
import { MessagesService } from './messages.service';

export const services: any[] = [PickupService, UsersService, MessagesService];

export * from './pickups.service';
export * from './users.service';
export * from './messages.service';
