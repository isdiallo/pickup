import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import {
  CreatePickupResponseInterface,
  SimulatedPickupResponseInterface,
  PickupLoadInterface,
  SpecificPickupLoadInterface,
  CreatePickupInterface,
  PickupAssignationInterface,
  PickupAssignationResponseInterface,
  PickupValidationResponseInterface,
} from '../models';

@Injectable()
export class PickupService {
  constructor(private http: HttpClient) {}

  getPickups(): Observable<PickupLoadInterface> {
    return this.http
      .get<PickupLoadInterface>(`api/pickup`)
      .pipe(catchError((error: any) => throwError(error.json())));
  }

  getSpecificPickup(pickupId: string): Observable<SpecificPickupLoadInterface> {
    return this.http
      .get<SpecificPickupLoadInterface>(`api/pickup/${pickupId}`)
      .pipe(catchError((error: any) => throwError(error.json())));
  }

  simulatePickupCreation(
    pickup: CreatePickupInterface
  ): Observable<SimulatedPickupResponseInterface> {
    return this.http
      .post<SimulatedPickupResponseInterface>(`api/pickup/simulate`, pickup)
      .pipe(catchError((error: any) => throwError(error)));
  }

  createPickup(
    pickup: CreatePickupInterface
  ): Observable<CreatePickupResponseInterface> {
    return this.http
      .post<CreatePickupResponseInterface>(`api/pickup`, pickup)
      .pipe(catchError((error: any) => throwError(error)));
  }

  assignPickup(
    assignationObj: PickupAssignationInterface
  ): Observable<PickupAssignationResponseInterface> {
    return this.http
      .post<PickupAssignationResponseInterface>(
        `api/pickup/assign`,
        assignationObj
      )
      .pipe(catchError((error: any) => throwError(error)));
  }

  validatePickup(
    pickupId: string
  ): Observable<PickupValidationResponseInterface> {
    return this.http
      .put(`api/pickup/${pickupId}/validate`, {})
      .pipe(catchError((error: any) => throwError(error)));
  }

  sendCode(pickupId: string): Observable<any> {
    return this.http
      .put(`api/pickup/${pickupId}/sendcode`, {})
      .pipe(catchError((error: any) => throwError(error)));
  }

  validateCode(
    pickupId: string,
    code: string
  ): Observable<SpecificPickupLoadInterface> {
    return this.http
      .put<SpecificPickupLoadInterface>(`api/pickup/${pickupId}/validatecode`, {
        code: code,
      })
      .pipe(catchError((error: any) => throwError(error)));
  }
}
