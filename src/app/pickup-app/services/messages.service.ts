import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http/';
import { Observable, throwError } from 'rxjs';
import { MessageResponseInterface } from '../models';
import { catchError } from 'rxjs/operators';

@Injectable()
export class MessagesService {
  constructor(private http: HttpClient) {}

  loadMessagess(): Observable<MessageResponseInterface> {
    return this.http
      .get<MessageResponseInterface>(`api/message`)
      .pipe(catchError((error: any) => throwError(error.json())));
  }
}
