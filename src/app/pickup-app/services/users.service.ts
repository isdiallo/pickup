import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UserResponseInterface, UserRelayResponseInterface } from '../models';

@Injectable()
export class UsersService {
  constructor(private http: HttpClient) {}

  getAgents(): Observable<UserResponseInterface> {
    return this.http
      .get<UserResponseInterface>(`api/users/agents`)
      .pipe(catchError((error: any) => throwError(error.json())));
  }

  getUsersRelay(): Observable<UserRelayResponseInterface> {
    return this.http
      .get<UserRelayResponseInterface>(`api/users/usersrelay`)
      .pipe(catchError((error: any) => throwError(error.json())));
  }
}
