import { HttpMsg, PickupUserInterface } from '../../pickup-shared/model';
import { PickupInterface } from '../../pickup-shared/model/pickup.model';

export interface PickupLoadInterface {
  error?: HttpMsg;
  success?: HttpMsg;
  data?: PickupInterface[];
}

// Create a new interface instead of using data?: PickupInterface[] | PickupInterface
// Because using reduce on data (in the reducer). Reduce apply only on list
export interface SpecificPickupLoadInterface {
  error?: HttpMsg;
  success?: HttpMsg;
  data?: PickupInterface;
}

export interface SimulatedPackageInterface {
  fragile: boolean;
  quantity: number;
}

export interface SimulatedPickupResponseInterface {
  error?: HttpMsg;
  success?: HttpMsg;
  data?: ChargeInterface;
}

export interface CreatePickupInterface {
  packages: { [key: string]: SimulatedPackageInterface };
  fromId: string;
  toId?: string;
  isFree: boolean;
  useRelay: boolean;
  endUser: PickupUserInterface;
}

export interface CreatePickupResponseInterface {
  error?: HttpMsg;
  success?: HttpMsg;
  data?: PickupInterface;
}

export interface ChargeInterface {
  charge: number;
}

export interface PickupAssignationInterface {
  pickups: string[];
  to: string;
}

export interface PickupAssignationResponseInterface {
  error?: HttpMsg;
  success?: HttpMsg;
  data?: {
    success?: string[];
    error: any[];
  };
}

export interface PickupValidationResponseInterface {
  error?: HttpMsg;
  success?: HttpMsg;
  data?: {
    _id: string;
  };
}

export interface ValidateCodeEventInterface {
  _id: string;
  code: string;
}
