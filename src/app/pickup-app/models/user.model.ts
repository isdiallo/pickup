import { HttpMsg } from '../../pickup-shared/model';
import { RelayInterface } from '../../pickup-shared/model/relay.model';

export interface UserInterface {
  firstname: string;
  lastname: string;
  phone: string;
  address: string;
  city: string;
  town: string;
  _id?: string;
}

export interface UserResponseInterface {
  error?: HttpMsg;
  success?: HttpMsg;
  data?: UserInterface[];
}

export interface UserRelayResponseInterface {
  error?: HttpMsg;
  success?: HttpMsg;
  data?: UserRelayInterface[];
}

export interface UserRelayInterface {
  firstname: string;
  lastname: string;
  phone: string;
  address: string;
  city: string;
  town: string;
  _id?: string;
  relayId: RelayInterface;
}

export interface ChangePasswordInterface {
  currentPwd: string;
  newPwd: string;
}

export interface MerchantDetailsInterface {
  autoAccept: boolean;
  sendNotif: boolean;
  websiteName?: string;
  websiteUrl?: string;
}
