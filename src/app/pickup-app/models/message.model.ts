import { HttpMsg } from '../../pickup-shared/model';

export interface MessageInterface {
  createdAt: string;
  from?: {
    lastname: string;
    firstname: string;
  };
  content: string;
}
export interface MessageResponseInterface {
  error?: HttpMsg;
  success?: HttpMsg;
  data?: {
    messages: MessageInterface[];
  };
}
