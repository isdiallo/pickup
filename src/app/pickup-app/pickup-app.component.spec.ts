import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickupAppComponent } from './pickup-app.component';
import {
  MatIconModule,
  MatMenuModule,
  MatToolbarModule,
  MatSidenavModule,
  MatListModule
} from '@angular/material';

describe('PickupAppComponent', () => {
  let component: PickupAppComponent;
  let fixture: ComponentFixture<PickupAppComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [PickupAppComponent],
        imports: [
          MatIconModule,
          MatMenuModule,
          MatToolbarModule,
          MatSidenavModule,
          MatListModule
        ]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PickupAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
