import { ValidatorFn, FormControl, AbstractControl } from '@angular/forms';
import { URL_REGEX } from '../../pickup-shared/pickup-shared.constants';

/**
 * If the website name is given then the url is required
 */

export function websiteUrlRequiredValidator(): ValidatorFn {
  return (input: AbstractControl): { [key: string]: boolean } => {
    if (input.root.get('websiteName') && input.root.get('websiteName').value) {
      if (input.root.get('websiteUrl') && input.root.get('websiteUrl').value) {
        if (URL_REGEX.test(input.root.get('websiteUrl').value)) {
          return null;
        }
        return { urlIncorrect: true };
      }
      return { urlRequired: true };
    } else {
      if (input.root.get('websiteUrl') && !input.root.get('websiteUrl').value) {
        input.root.get('websiteUrl').setErrors(null);
      }
      if (input.root.get('websiteUrl') && input.root.get('websiteUrl').value) {
        if (URL_REGEX.test(input.root.get('websiteUrl').value)) {
          return null;
        }
        return { urlIncorrect: true };
      }
      return null;
    }
  };
}
