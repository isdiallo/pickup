export const PICKUP_ROUTER_MAP = {
  Agent: ['/app/pickups/assigned'],
  UserRelay: ['/app/pickups/assigned'],
  Merchant: ['/app/pickups/to-be-validated']
};

export const INITIALIZED_PACKAGES_FORM_BUILDER = [
  {
    fragile: false,
    quantity: 0,
    name: 'XL'
  },
  {
    fragile: false,
    quantity: 0,
    name: 'L'
  },
  {
    fragile: false,
    quantity: 0,
    name: 'M'
  },
  {
    fragile: false,
    quantity: 0,
    name: 'S'
  }
];
