import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';

import { take } from 'rxjs/operators';

import { Store } from '@ngrx/store';

import * as fromStore from './store';
import * as fromSharedStore from '../pickup-shared/store';
import { Router } from '@angular/router';
import { PICKUP_ROUTER_MAP } from './pickup-app.constant';
import { MAX_MOBILE_WIDTH } from '../pickup-shared/pickup-shared.constants';

@Component({
  selector: 'app-pickup-app',
  templateUrl: './pickup-app.component.html',
  styleUrls: ['./pickup-app.component.scss']
})
export class PickupAppComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;
  layoutChange: boolean;
  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private store: Store<fromStore.AppPickupState>,
    private sharedStore: Store<fromSharedStore.AppSharedState>,
    private route: Router
  ) {
    this.mobileQuery = media.matchMedia(
      '(max-width:' + MAX_MOBILE_WIDTH + ' )'
    );
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  goToPickup() {
    this.sharedStore
      .select(fromSharedStore.getUserType)
      .pipe(take(1))
      .subscribe(userType => {
        this.route.navigate(PICKUP_ROUTER_MAP[userType]);
      });
  }
  signOff() {
    this.sharedStore.dispatch(new fromSharedStore.DoLogout());
    this.store.dispatch(new fromStore.ReinitializeStore());
    this.store.dispatch(new fromStore.ReinitializeMessagesStore());
  }

  ngOnInit() {
    this.store.dispatch(new fromStore.LoadPickups());
  }
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
