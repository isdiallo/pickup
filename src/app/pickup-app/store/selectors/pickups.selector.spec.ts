import { TestBed } from '@angular/core/testing';
import { StoreModule, Store, combineReducers } from '@ngrx/store';

import * as fromRoot from '../../../../app/store/reducers';
import * as fromReducers from '../reducers';
import * as fromPickups from '../reducers/pickups.reducer';
import * as fromActions from '..//actions';
import * as fromSelectors from '../selectors/pickups.selector';
import * as fromSharedStore from '../../../pickup-shared/store';
import { PickupInterface } from '../../../pickup-shared/model/pickup.model';
import { PICKUPS } from '../../../pickup-shared/testing/pickups.data';
import { UserType } from '../../../pickup-shared/model';

describe('Pickups selectors', () => {
  let store: Store<fromReducers.AppPickupState>;

  const pickups: PickupInterface[] = PICKUPS;
  const entities = {
    '5a86067442de7a002093fe5f': pickups[0],
    '5a8dccc3aec7ea08284104a2': pickups[1]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          ...fromRoot.reducers,
          appPickup: combineReducers(fromReducers.reducers),
          appShared: combineReducers(fromSharedStore.reducers)
        })
      ]
    });
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  describe('getPickupState', () => {
    it('should return pickups state', () => {
      const { initialState } = fromPickups;
      let result;
      store
        .select(fromSelectors.getPickupState)
        .subscribe(value => (result = value));
      expect(result).toEqual(initialState);

      store.dispatch(
        new fromActions.LoadPickupsSuccess({
          data: PICKUPS,
          success: {
            description: 'load pickups success',
            message: 'load.pickups.success'
          }
        })
      );
      expect(result).toEqual({
        ...initialState,
        loading: false,
        loaded: true,
        entities
      });
    });
  });

  describe('getAllPickups', () => {
    it('should return pickups as list', () => {
      let result;
      store
        .select(fromSelectors.getAllPickups)
        .subscribe(value => (result = value));
      expect(result).toEqual([]);

      store.dispatch(
        new fromActions.LoadPickupsSuccess({
          data: PICKUPS,
          success: {
            description: 'load pickups success',
            message: 'load.pickups.success'
          }
        })
      );
      expect(result).toEqual(PICKUPS);
    });
  });

  describe('getPickupsEntities', () => {
    it('should return pickups as entities', () => {
      let result;
      store
        .select(fromSelectors.getPickupsEntities)
        .subscribe(value => (result = value));
      expect(result).toEqual({});

      store.dispatch(
        new fromActions.LoadPickupsSuccess({
          data: PICKUPS,
          success: {
            description: 'load pickups success',
            message: 'load.pickups.success'
          }
        })
      );
      expect(result).toEqual(entities);
    });
  });

  describe('getPickupsLoaded', () => {
    it('should return loaded value as boolean', () => {
      let result;
      store
        .select(fromSelectors.getPickupsLoaded)
        .subscribe(value => (result = value));
      expect(result).toEqual(false);

      store.dispatch(
        new fromActions.LoadPickupsSuccess({
          data: PICKUPS,
          success: {
            description: 'load pickups success',
            message: 'load.pickups.success'
          }
        })
      );

      expect(result).toEqual(true);
    });
  });

  describe('getPickupsLoading', () => {
    it('should return loading value as boolean', () => {
      let result;
      store
        .select(fromSelectors.getPickupsLoading)
        .subscribe(value => (result = value));
      expect(result).toEqual(false);

      store.dispatch(new fromActions.LoadPickups());

      expect(result).toEqual(true);
    });
  });

  describe('getSimulationCharge', () => {
    it('should return the pickup creation simulation charge', () => {
      let result;
      store
        .select(fromSelectors.getSimulationCharge)
        .subscribe(value => (result = value));
      expect(result).toEqual(null);

      store.dispatch(
        new fromActions.SimulatePickupCreationSuccess({
          success: {
            description: 'simulate pickup creation success',
            message: 'simulate.pickup.creation.success'
          },
          data: { charge: 1500 }
        })
      );

      expect(result).toEqual(1500);
    });
  });

  describe('getSelectedPickup', () => {
    it('should return the selected pickup', () => {
      let result;
      store
        .select(fromSelectors.getSelectedPickup('5a86067442de7a002093fe5f'))
        .subscribe(value => (result = value));
      expect(result).toEqual(undefined);

      store.dispatch(
        new fromActions.LoadPickupsSuccess({
          data: PICKUPS,
          success: {
            description: 'load pickups success',
            message: 'load.pickups.success'
          }
        })
      );
      expect(result).toEqual(PICKUPS[0]);
    });
  });

  describe('getPickupByState', () => {
    it('should return the pickup according to the state', () => {
      let result;
      store
        .select(fromSelectors.getPickupByState('VALIDATED'))
        .subscribe(value => (result = value));
      let result1;
      store
        .select(fromSelectors.getPickupByState('ONGOING'))
        .subscribe(value => (result1 = value));
      expect(result).toEqual([]);
      expect(result1).toEqual([]);

      store.dispatch(
        new fromActions.LoadPickupsSuccess({
          data: PICKUPS,
          success: {
            description: 'load pickups success',
            message: 'load.pickups.success'
          }
        })
      );
      expect(result1).toEqual([]);
      expect(result).toEqual(PICKUPS);
    });
  });

  describe('getVisualizedPickups', () => {
    it('should return visualized pickups', () => {
      let result;
      store.dispatch(
        new fromSharedStore.GetIdentitySuccess({
          success: { description: '', message: '' },
          data: {
            _id: 'fgdsfg5sdgfsdfg',
            _type: UserType.Merchant,
            updatedAt: '',
            createdAt: '',
            firstname: 'Merch Test',
            lastname: 'Merch Last',
            username: 'merchant@test.fr',
            phone: '145214587',
            mail: 'merchant@test.fr',
            city: 'Merch City',
            town: 'Merch town',
            address: 'Merch address'
          }
        })
      );
      store.dispatch({
        type: 'ROUTER_NAVIGATION',
        payload: {
          routerState: {
            url: '/app/pickups/validated',
            queryParams: {},
            params: {},
            data: {
              name: 'validated',
              authorities: ['Merchant']
            }
          },
          event: {}
        }
      });

      store
        .select(fromSelectors.getVisualizedPickups)
        .subscribe(value => (result = value));
      expect(result).toEqual([]);

      store.dispatch(
        new fromActions.LoadPickupsSuccess({
          data: PICKUPS,
          success: {
            description: 'load pickups success',
            message: 'load.pickups.success'
          }
        })
      );

      expect(result).toEqual(Object.keys(entities).map(el => entities[el]));
    });
  });
});
