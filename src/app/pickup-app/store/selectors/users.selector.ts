import { createSelector } from '@ngrx/store';

import * as fromFeatures from '../reducers';
import * as fromUsers from '../reducers/users.reducers';

export const getUsersState = createSelector(
  fromFeatures.getAppPickupState,
  (state: fromFeatures.AppPickupState) => state.users
);

export const getUsersRelay = createSelector(
  getUsersState,
  fromUsers.getUsersRelay
);

export const getUsersRelayLoading = createSelector(
  getUsersState,
  fromUsers.getUsersRelayLoading
);

export const getUsersRelayLoaded = createSelector(
  getUsersState,
  fromUsers.getUsersRelayLoaded
);

export const getUsersAgent = createSelector(
  getUsersState,
  fromUsers.getUsersAgent
);

export const getUsersAgentLoading = createSelector(
  getUsersState,
  fromUsers.getUsersAgentLoading
);

export const getUsersAgentLoaded = createSelector(
  getUsersState,
  fromUsers.getUsersAgentLoaded
);
