import { createSelector } from '@ngrx/store';

import * as fromFeatures from '../reducers';
import * as fromMessages from '../reducers/message.reducer';

export const getMessagesState = createSelector(
  fromFeatures.getAppPickupState,
  (state: fromFeatures.AppPickupState) => state.messages
);

export const getMessages = createSelector(
  getMessagesState,
  fromMessages.getMessages
);

export const getMessagesLoading = createSelector(
  getMessagesState,
  fromMessages.getMessageLoading
);
export const getMessagesLoaded = createSelector(
  getMessagesState,
  fromMessages.getMessagesLoaded
);
