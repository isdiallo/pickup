import { TestBed } from '@angular/core/testing';

import * as fromRoot from '../../../../app/store/reducers';
import * as fromReducers from '../reducers';
import * as fromMessages from '../reducers/message.reducer';
import * as fromSelectors from './message.selector';
import * as fromActions from '../actions/messages.action';
import { StoreModule, combineReducers, Store } from '@ngrx/store';
import { MSGs } from '../../../pickup-shared/testing/messages.data';

describe('Message Selectors', () => {
  let store: Store<fromReducers.AppPickupState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          ...fromRoot.reducers,
          appPickup: combineReducers(fromReducers.reducers)
        })
      ]
    });

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  describe('getMessagesState', () => {
    it('should return messages state', () => {
      const { initialState } = fromMessages;
      let result;
      store
        .select(fromSelectors.getMessagesState)
        .subscribe(value => (result = value));
      expect(result).toEqual(initialState);

      store.dispatch(
        new fromActions.LoadMessagesSuccess({
          data: {
            messages: MSGs
          },
          success: {
            description: 'load messages success',
            message: 'load.messages.success'
          }
        })
      );
      expect(result).toEqual({
        ...initialState,
        loading: false,
        loaded: true,
        messages: MSGs
      });
    });
  });

  describe('getMessages', () => {
    it('should return the list of messages', () => {
      const { initialState } = fromMessages;
      let result;
      store
        .select(fromSelectors.getMessages)
        .subscribe(value => (result = value));
      expect(result).toEqual([]);

      store.dispatch(
        new fromActions.LoadMessagesSuccess({
          data: {
            messages: MSGs
          },
          success: {
            description: 'load messages success',
            message: 'load.messages.success'
          }
        })
      );
      expect(result).toEqual(MSGs);
    });
  });

  describe('getMessagesLoading', () => {
    it('should return the loading', () => {
      const { initialState } = fromMessages;
      let result;
      store
        .select(fromSelectors.getMessagesLoading)
        .subscribe(value => (result = value));
      expect(result).toEqual(false);

      store.dispatch(new fromActions.LoadMessages());
      expect(result).toEqual(true);
    });
  });

  describe('getMessagesLoaded', () => {
    it('should return loaded', () => {
      const { initialState } = fromMessages;
      let result;
      store
        .select(fromSelectors.getMessagesLoaded)
        .subscribe(value => (result = value));
      expect(result).toEqual(false);

      store.dispatch(
        new fromActions.LoadMessagesSuccess({
          data: {
            messages: MSGs
          },
          success: {
            description: 'load messages success',
            message: 'load.messages.success'
          }
        })
      );
      expect(result).toEqual(true);
    });
  });
});
