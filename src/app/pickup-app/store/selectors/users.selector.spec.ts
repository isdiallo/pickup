import { TestBed } from '@angular/core/testing';

import * as fromRoot from '../../../../app/store/reducers';
import * as fromReducers from '../reducers';
import * as fromUsers from '../reducers/users.reducers';
import * as fromSelectors from '../selectors/users.selector';
import * as fromActions from '../actions/users.action';
import { StoreModule, combineReducers, Store } from '@ngrx/store';
import { AGENTS, USERSRELAY } from '../../../pickup-shared/testing/users.data';

describe('Message Selectors', () => {
  let store: Store<fromReducers.AppPickupState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          ...fromRoot.reducers,
          appPickup: combineReducers(fromReducers.reducers)
        })
      ]
    });

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  describe('getUsersState', () => {
    it('should return users state', () => {
      const { initialState } = fromUsers;
      let result;
      store
        .select(fromSelectors.getUsersState)
        .subscribe(value => (result = value));
      expect(result).toEqual(initialState);

      store.dispatch(new fromActions.LoadUsersAgent());
      store.dispatch(new fromActions.LoadUsersRelay());
      expect(result).toEqual({
        ...initialState,
        agentsLoading: true,
        agentsLoaded: false,
        relaysLoading: true,
        relaysLoaded: false,
        usersAgent: [],
        usersRelay: []
      });
    });
  });

  describe('getUsersRelay', () => {
    it('should return the list of relay users', () => {
      const { initialState } = fromUsers;
      let result;
      store
        .select(fromSelectors.getUsersRelay)
        .subscribe(value => (result = value));
      expect(result).toEqual([]);

      store.dispatch(
        new fromActions.LoadUsersRelaySuccess({
          data: USERSRELAY,
          success: {
            description: 'load users relay success',
            message: 'load.users.relay.success'
          }
        })
      );
      expect(result).toEqual(USERSRELAY);
    });
  });

  describe('getUsersRelayLoading', () => {
    it('should return relaysLoading', () => {
      const { initialState } = fromUsers;
      let result;
      store
        .select(fromSelectors.getUsersRelayLoading)
        .subscribe(value => (result = value));
      expect(result).toEqual(false);

      store.dispatch(new fromActions.LoadUsersRelay());
      expect(result).toEqual(true);
    });
  });

  describe('getUsersRelayLoaded', () => {
    it('should return relaysLoaded', () => {
      const { initialState } = fromUsers;
      let result;
      store
        .select(fromSelectors.getUsersRelayLoaded)
        .subscribe(value => (result = value));
      expect(result).toEqual(false);

      store.dispatch(
        new fromActions.LoadUsersRelaySuccess({
          data: USERSRELAY,
          success: {
            description: 'load users relay success',
            message: 'load.users.relay.success'
          }
        })
      );
      expect(result).toEqual(true);
    });
  });

  describe('getUsersAgent', () => {
    it('should return the list of agents', () => {
      const { initialState } = fromUsers;
      let result;
      store
        .select(fromSelectors.getUsersAgent)
        .subscribe(value => (result = value));
      expect(result).toEqual([]);

      store.dispatch(
        new fromActions.LoadUsersAgentSuccess({
          data: AGENTS,
          success: {
            description: 'load users agent success',
            message: 'load.users.agent.success'
          }
        })
      );
      expect(result).toEqual(AGENTS);
    });
  });

  describe('getUsersAgentLoading', () => {
    it('should return agentsLoading', () => {
      const { initialState } = fromUsers;
      let result;
      store
        .select(fromSelectors.getUsersAgentLoading)
        .subscribe(value => (result = value));
      expect(result).toEqual(false);

      store.dispatch(new fromActions.LoadUsersAgent());
      expect(result).toEqual(true);
    });
  });

  describe('getUsersAgentLoaded', () => {
    it('should return agentsLoaded', () => {
      const { initialState } = fromUsers;
      let result;
      store
        .select(fromSelectors.getUsersAgentLoaded)
        .subscribe(value => (result = value));
      expect(result).toEqual(false);

      store.dispatch(
        new fromActions.LoadUsersAgentSuccess({
          data: AGENTS,
          success: {
            description: 'load users agent success',
            message: 'load.users.agent.success'
          }
        })
      );
      expect(result).toEqual(true);
    });
  });
});
