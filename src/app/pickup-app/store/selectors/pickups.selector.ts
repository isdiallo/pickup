import { createSelector } from '@ngrx/store';

import * as fromSharedStore from '../../../pickup-shared/store';

import * as fromRoot from '../../../store';
import * as fromFeatures from '../reducers';
import * as fromPickups from '../reducers/pickups.reducer';
import { PickupInterface } from '../../../pickup-shared/model/pickup.model';

export const pickupsRouterMapper = {
  Merchant: {
    toBeValidated: ['CREATED'],
    validated: ['VALIDATED', 'ONGOING', 'ASSIGNED', 'TRANSMITTED'],
    delivered: ['DELIVERED']
  },
  Agent: {
    assigned: ['ASSIGNED', 'ONGOING'],
    myLast: ['TRANSMITTED', 'DELIVERED']
  },
  UserRelay: {
    assigned: ['TRANSMITTED'],
    myLast: ['DELIVERED']
  }
};

export const getPickupState = createSelector(
  fromFeatures.getAppPickupState,
  (state: fromFeatures.AppPickupState) => state.pickups
);

export const getPickupsEntities = createSelector(
  getPickupState,
  fromPickups.getPickupsEntities
);

export const getAllPickups = createSelector(getPickupsEntities, entities => {
  return Object.keys(entities).map(key => entities[key]);
});

export const getVisualizedPickups = createSelector(
  getAllPickups,
  fromSharedStore.getUserType,
  fromRoot.getRouterState,
  (entities, userType, router): PickupInterface[] => {
    return (
      router &&
      router.state &&
      router.state.data &&
      router.state.data.name &&
      pickupsRouterMapper[userType] &&
      pickupsRouterMapper[userType][router.state.data.name] &&
      entities.filter(
        entity =>
          pickupsRouterMapper[userType][router.state.data.name].indexOf(
            entity.state.name
          ) >= 0
      )
    );
  }
);

export const getPickupsLoaded = createSelector(
  getPickupState,
  fromPickups.getPickupsLoaded
);
export const getPickupsLoading = createSelector(
  getPickupState,
  fromPickups.getPickupsLoading
);

export const getSimulationCharge = createSelector(
  getPickupState,
  fromPickups.getPickupsSimulationCharge
);

export const getSelectedPickup = (id: string) =>
  createSelector(getPickupsEntities, pickupsEntity => pickupsEntity[id]);

export const getPickupByState = (state: string) =>
  createSelector(getAllPickups, (pickups: PickupInterface[]) =>
    pickups.filter(
      pickup =>
        pickup &&
        pickup.state &&
        pickup.state.name &&
        pickup.state.name === state
    )
  );
