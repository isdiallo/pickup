import * as fromPickups from './pickups.action';
import {
  PICKUPS,
  PickupCreationResponse
} from '../../../pickup-shared/testing/pickups.data';

describe('Pickups Actions', () => {
  describe('Load Pickups Actions', () => {
    describe('Load Pickups', () => {
      it('should create an action', () => {
        const action = new fromPickups.LoadPickups();
        expect({ ...action }).toEqual({
          type: fromPickups.LOAD_PICKUPS
        });
      });
    });

    describe('Load Pickups Success', () => {
      it('should create an action', () => {
        const payload = {
          data: PICKUPS,
          success: {
            description: 'load pickups success',
            message: 'load.pickups.success'
          }
        };
        const action = new fromPickups.LoadPickupsSuccess(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.LOAD_PICKUPS_SUCCESS,
          payload
        });
      });
    });

    describe('Load Pickups Fail', () => {
      it('should create an action', () => {
        const payload = {
          error: {
            message: 'error',
            description: 'error'
          }
        };
        const action = new fromPickups.LoadPickupsFail(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.LOAD_PICKUPS_FAIL,
          payload
        });
      });
    });
  });

  describe('Load Specific Pickup Actions', () => {
    describe('Load Specific Pickup', () => {
      it('should create an action', () => {
        const payload = '5a86067442de7a002093fe5f';
        const action = new fromPickups.LoadSpecificPickup(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.LOAD_SPECIFIC_PICKUP,
          payload
        });
      });
    });

    describe('Load Specific Pickup Success', () => {
      it('should create an action', () => {
        const payload = {
          success: {
            description: 'load specific pickup success',
            message: 'load.specific.pickup.success'
          },
          data: PICKUPS[0]
        };
        const action = new fromPickups.LoadSpecificPickupSuccess(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.LOAD_SPECIFIC_PICKUP_SUCCESS,
          payload
        });
      });
    });

    describe('Load Specific Pickup Fail', () => {
      it('should create an action', () => {
        const payload = {
          error: {
            message: 'error',
            description: 'error'
          }
        };
        const action = new fromPickups.LoadSpecificPickupFail(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.LOAD_SPECIFIC_PICKUP_FAIL,
          payload
        });
      });
    });
  });

  describe('Simulate Pickup Creation Actions', () => {
    describe('Simulate Pickup Creation', () => {
      it('should create an action', () => {
        const payload = {
          packages: {},
          endUser: {
            firstname: 'End user first',
            lastname: 'End User last'
          },
          fromId: '1254sadf568sdf',
          isFree: false,
          useRelay: false
        };
        const action = new fromPickups.SimulatePickupCreation(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.SIMULATE_PICKUP_CREATION,
          payload
        });
      });
    });

    describe('Simulate Pickup Creation Success', () => {
      it('should create an action', () => {
        const payload = {
          success: {
            description: 'simulate pickup creation success',
            message: 'simulate.pickup.creation.success'
          },
          data: { charge: 1500 }
        };
        const action = new fromPickups.SimulatePickupCreationSuccess(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.SIMULATE_PICKUP_CREATION_SUCCESS,
          payload
        });
      });
    });

    describe('Simulate Pickup Creation Fail', () => {
      it('should create an action', () => {
        const payload = {
          error: {
            message: 'error',
            description: 'error'
          }
        };
        const action = new fromPickups.SimulatePickupCreationFail(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.SIMULATE_PICKUP_CREATION_FAIL,
          payload
        });
      });
    });
  });

  describe('Pickup Creation Actions', () => {
    describe('Pickup Creation', () => {
      it('should create an action', () => {
        const payload = {
          packages: {},
          endUser: {
            firstname: 'End user first',
            lastname: 'End User last'
          },
          fromId: '1254sadf568sdf',
          isFree: false,
          useRelay: false
        };
        const action = new fromPickups.CreatePickup(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.CREATE_PICKUP,
          payload
        });
      });
    });

    describe('Pickup Creation Success', () => {
      it('should create an action', () => {
        const payload = PickupCreationResponse;
        const action = new fromPickups.CreatePickupSuccess(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.CREATE_PICKUP_SUCCESS,
          payload
        });
      });
    });

    describe('Pickup Creation Fail', () => {
      it('should create an action', () => {
        const payload = {
          error: {
            message: 'error',
            description: 'error'
          }
        };
        const action = new fromPickups.CreatePickupFail(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.CREATE_PICKUP_FAIL,
          payload
        });
      });
    });
  });

  describe('Pickup Assignation Actions', () => {
    describe('Pickup Assignation', () => {
      it('should create an action', () => {
        const payload = {
          pickups: ['454sadf'],
          to: 'asdfsad5645'
        };
        const action = new fromPickups.AssignPickups(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.ASSIGN_PICKUPS,
          payload
        });
      });
    });

    describe('Pickup Assignation Success', () => {
      it('should create an action', () => {
        const payload = {
          success: {
            message: 'assign.pickup.success',
            description: 'Assign pickup success'
          },
          data: {
            success: ['5a8dccc3aec7ea08284104a2'],
            error: []
          }
        };
        const action = new fromPickups.AssignPickupsSuccess(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.ASSIGN_PICKUPS_SUCCESS,
          payload
        });
      });
    });

    describe('Pickup Assignation Fail', () => {
      it('should create an action', () => {
        const payload = {
          error: {
            message: 'error',
            description: 'error'
          }
        };
        const action = new fromPickups.AssignPickupsFail(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.ASSIGN_PICKUPS_FAIL,
          payload
        });
      });
    });
  });

  describe('Pickup Validation Actions', () => {
    describe('Pickup Validation', () => {
      it('should create an action', () => {
        const payload = 'sdf54s5dffd';
        const action = new fromPickups.ValidatePickup(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.VALIDATE_PICKUP,
          payload
        });
      });
    });

    describe('Pickup Validation Success', () => {
      it('should create an action', () => {
        const payload = {
          success: {
            message: 'pickup.validate.success',
            description:
              'You have validated successfuly the pickup. The process will start soon.'
          },
          data: { _id: '5af340c90984340fe845611c' }
        };
        const action = new fromPickups.ValidatePickupSuccess(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.VALIDATE_PICKUP_SUCCESS,
          payload
        });
      });
    });

    describe('Pickup Validation Fail', () => {
      it('should create an action', () => {
        const payload = {
          error: {
            message: 'error',
            description: 'error'
          }
        };
        const action = new fromPickups.ValidatePickupFail(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.VALIDATE_PICKUP_FAIL,
          payload
        });
      });
    });
  });

  describe('Send Code Actions', () => {
    describe('Send Code Validation', () => {
      it('should create an action', () => {
        const pickupId = 'sdf54s5dffd';
        const action = new fromPickups.SendCode(pickupId);
        expect({ ...action }).toEqual({
          type: fromPickups.SEND_CODE,
          pickupId
        });
      });
    });

    describe('Send Code Validation Success', () => {
      it('should create an action', () => {
        const action = new fromPickups.SendCodeSuccess();
        expect({ ...action }).toEqual({
          type: fromPickups.SEND_CODE_SUCCESS
        });
      });
    });

    describe('Send Code Validation Fail', () => {
      it('should create an action', () => {
        const payload = {
          error: {
            message: 'error',
            description: 'error'
          }
        };
        const action = new fromPickups.SendCodeFail(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.SEND_CODE_FAIL,
          payload
        });
      });
    });
  });

  describe('Validate Code Actions', () => {
    describe('Validate Code Validation', () => {
      it('should create an action', () => {
        const pickupId = 'sdf54s5dffd';
        const payload = '145223';
        const action = new fromPickups.ValidateCode(pickupId, payload);
        expect({ ...action }).toEqual({
          type: fromPickups.VALIDATE_CODE,
          pickupId,
          payload
        });
      });
    });

    describe('Validate Code Validation Success', () => {
      it('should create an action', () => {
        const payload = {
          success: {
            message: 'validate.code.success',
            description: 'validate code success'
          },
          data: PICKUPS[0]
        };
        const action = new fromPickups.ValidateCodeSuccess(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.VALIDATE_CODE_SUCCESS,
          payload
        });
      });
    });

    describe('Validate Code Validation Fail', () => {
      it('should create an action', () => {
        const payload = {
          error: {
            message: 'error',
            description: 'error'
          }
        };
        const action = new fromPickups.ValidateCodeFail(payload);
        expect({ ...action }).toEqual({
          type: fromPickups.VALIDATE_CODE_FAIL,
          payload
        });
      });
    });
  });

  describe('Reinitialize Store', () => {
    it('should create an action', () => {
      const action = new fromPickups.ReinitializeStore();
      expect({ ...action }).toEqual({
        type: fromPickups.REINITIALIZE_STORE
      });
    });
  });

  describe('Pickup Package Changed', () => {
    it('should create an action', () => {
      const action = new fromPickups.PickupPackageChanged();
      expect({ ...action }).toEqual({
        type: fromPickups.PICKUP_PACKAGE_CHANGED
      });
    });
  });
});
