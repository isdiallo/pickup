import * as fromUsers from './users.action';

describe('Users Action', () => {
  describe('Load Users Relay Actions', () => {
    describe('Load Users Relay', () => {
      it('should create an action', () => {
        const action = new fromUsers.LoadUsersRelay();
        expect({ ...action }).toEqual({
          type: fromUsers.LOAD_USERS_RELAY
        });
      });
    });

    describe('Load Users Relay Success', () => {
      it('should create an action', () => {
        const payload = {
          success: {
            description: 'Load Users Relay Success',
            message: 'load.user.relay.success'
          },
          data: [
            {
              firstname: 'Test first',
              lastname: 'Test last',
              phone: '021458745',
              address: 'Test address',
              city: 'Test city',
              town: 'Test town',
              relayId: {
                name: 'Test relay',
                address: 'Test relay address',
                city: 'Test relay city',
                town: 'Test relay town',
                holiday: false
              }
            }
          ]
        };
        const action = new fromUsers.LoadUsersRelaySuccess(payload);
        expect({ ...action }).toEqual({
          type: fromUsers.LOAD_USERS_RELAY_SUCCESS,
          payload
        });
      });
    });

    describe('Load Users Relay Fail', () => {
      it('should create an action', () => {
        const payload = {
          error: {
            message: 'load.user.relay.fail',
            description: 'Laod users relay fail'
          }
        };
        const action = new fromUsers.LoadUsersRelayFail(payload);
        expect({ ...action }).toEqual({
          type: fromUsers.LOAD_USERS_RELAY_FAIL,
          payload
        });
      });
    });
  });

  describe('Load Users Agent Actions', () => {
    describe('Load Users Agent', () => {
      it('should create an action', () => {
        const action = new fromUsers.LoadUsersAgent();
        expect({ ...action }).toEqual({
          type: fromUsers.LOAD_USERS_AGENT
        });
      });
    });

    describe('Load Users Agent Success', () => {
      it('should create an action', () => {
        const payload = {
          success: {
            description: 'Load Users Agent Success',
            message: 'load.user.agent.success'
          },
          data: [
            {
              firstname: 'Test first',
              lastname: 'Test last',
              phone: '021458745',
              address: 'Test address',
              city: 'Test city',
              town: 'Test town'
            }
          ]
        };
        const action = new fromUsers.LoadUsersAgentSuccess(payload);
        expect({ ...action }).toEqual({
          type: fromUsers.LOAD_USERS_AGENT_SUCCESS,
          payload
        });
      });
    });

    describe('Load Users Agent Fail', () => {
      it('should create an action', () => {
        const payload = {
          error: {
            message: 'load.user.agent.fail',
            description: 'Laod users agent fail'
          }
        };
        const action = new fromUsers.LoadUsersAgentFail(payload);
        expect({ ...action }).toEqual({
          type: fromUsers.LOAD_USERS_AGENT_FAIL,
          payload
        });
      });
    });
  });
});
