import { Action } from '@ngrx/store';
import {
  UserResponseInterface,
  UserRelayResponseInterface
} from '../../models';

// Load Relay Users

export const LOAD_USERS_RELAY = '[App Pickup::Users] load users relay';
export const LOAD_USERS_RELAY_SUCCESS =
  '[App Pickup::Users] load users relay success';
export const LOAD_USERS_RELAY_FAIL =
  '[App Pickup::Users] load users relay fail';

export class LoadUsersRelay implements Action {
  readonly type = LOAD_USERS_RELAY;
}

export class LoadUsersRelaySuccess implements Action {
  readonly type = LOAD_USERS_RELAY_SUCCESS;
  constructor(public payload: UserRelayResponseInterface) {}
}

export class LoadUsersRelayFail implements Action {
  readonly type = LOAD_USERS_RELAY_FAIL;
  constructor(public payload: any) {}
}

// Load Agents Users

export const LOAD_USERS_AGENT = '[App Pickup::Users] load users agent';
export const LOAD_USERS_AGENT_SUCCESS =
  '[App Pickup::Users] load users agent success';
export const LOAD_USERS_AGENT_FAIL =
  '[App Pickup::Users] load users agent fail';

export class LoadUsersAgent implements Action {
  readonly type = LOAD_USERS_AGENT;
}

export class LoadUsersAgentSuccess implements Action {
  readonly type = LOAD_USERS_AGENT_SUCCESS;
  constructor(public payload: UserResponseInterface) {}
}

export class LoadUsersAgentFail implements Action {
  readonly type = LOAD_USERS_AGENT_FAIL;
  constructor(public payload: any) {}
}

// export UsersAction
export type UsersAction =
  | LoadUsersRelay
  | LoadUsersRelaySuccess
  | LoadUsersRelayFail
  | LoadUsersAgent
  | LoadUsersAgentSuccess
  | LoadUsersAgentFail;
