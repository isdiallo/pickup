import { Action } from '@ngrx/store';
import {
  PickupLoadInterface,
  SpecificPickupLoadInterface,
  SimulatedPickupResponseInterface,
  CreatePickupInterface,
  CreatePickupResponseInterface,
  PickupAssignationResponseInterface,
  PickupAssignationInterface,
  PickupValidationResponseInterface
} from '../../models';

// load pickups
export const LOAD_PICKUPS = '[App Pickup::Pickup] load pickups';
export const LOAD_PICKUPS_FAIL = '[App Pickup::Pickup] load pickups fail';
export const LOAD_PICKUPS_SUCCESS = '[App Pickup::Pickup] load pickups success';

// Load more info for a specific pickup: Not used anymore
export const LOAD_SPECIFIC_PICKUP = '[App Pickup::Pickup] load specific pickup';
export const LOAD_SPECIFIC_PICKUP_SUCCESS =
  '[App Pickup::Pickup] load specific pickup success';
export const LOAD_SPECIFIC_PICKUP_FAIL =
  '[App Pickup::Pickup] load specific pickup fail';

// Simulate pickup creation
export const SIMULATE_PICKUP_CREATION =
  '[App Pickup::Pickup] Simulate Pickup Creation';
export const SIMULATE_PICKUP_CREATION_SUCCESS =
  '[App Pickup::Pickup] Simulate Pickup Creation Success';
export const SIMULATE_PICKUP_CREATION_FAIL =
  '[App Pickup::Pickup] Simulate Pickup Creation Fail';

// Pickup package change
export const PICKUP_PACKAGE_CHANGED =
  '[App Pickup::Pickup] pickup package changed';

// Pickup Creation
export const CREATE_PICKUP = '[App Pickup::Pickup] create pickup';
export const CREATE_PICKUP_SUCCESS =
  '[App Pickup::Pickup] create pickup success';
export const CREATE_PICKUP_FAIL = '[App Pickup::Pickup] create pickup fail';

// Assign pickup to an agent
export const ASSIGN_PICKUPS = '[App Pickup::Pickup] assign pickup to an agent';
export const ASSIGN_PICKUPS_SUCCESS =
  '[App Pickup::Pickup] assign pickup to an agent success';
export const ASSIGN_PICKUPS_FAIL =
  '[App Pickup::Pickup] assign pickup to an agent fail';

// Validate Pickup
export const VALIDATE_PICKUP = '[App Pickup::Pickup] validate pickup';
export const VALIDATE_PICKUP_SUCCESS =
  '[App Pickup::Pickup] validate pickup success';
export const VALIDATE_PICKUP_FAIL = '[App Pickup::Pickup] validate pickup fail';

// Send Code
export const SEND_CODE = '[App Pickup::Pickup] send code';
export const SEND_CODE_SUCCESS = '[App Pickup::Pickup] send code success';
export const SEND_CODE_FAIL = '[App Pickup::Pickup] send code fail';

// Validate code
export const VALIDATE_CODE = '[App Pickup::Pickup] validate code';
export const VALIDATE_CODE_SUCCESS =
  '[App Pickup::Pickup] validate code success';
export const VALIDATE_CODE_FAIL = '[App Pickup::Pickup] validate code fail';

// Reinitialize pickup store (after log out)
export const REINITIALIZE_STORE = '[APP Pickup::Pickup] reinitialize store';

export class LoadPickups implements Action {
  readonly type = LOAD_PICKUPS;
}

export class LoadPickupsFail implements Action {
  readonly type = LOAD_PICKUPS_FAIL;
  constructor(public payload: any) {}
}

export class LoadPickupsSuccess implements Action {
  readonly type = LOAD_PICKUPS_SUCCESS;
  constructor(public payload: PickupLoadInterface) {}
}

export class LoadSpecificPickup implements Action {
  readonly type = LOAD_SPECIFIC_PICKUP;
  constructor(public payload: string) {}
}

export class LoadSpecificPickupSuccess implements Action {
  readonly type = LOAD_SPECIFIC_PICKUP_SUCCESS;
  constructor(public payload: SpecificPickupLoadInterface) {}
}

export class LoadSpecificPickupFail implements Action {
  readonly type = LOAD_SPECIFIC_PICKUP_FAIL;
  constructor(public payload: any) {}
}

export class SimulatePickupCreation implements Action {
  readonly type = SIMULATE_PICKUP_CREATION;
  constructor(public payload: CreatePickupInterface) {}
}

export class SimulatePickupCreationSuccess implements Action {
  readonly type = SIMULATE_PICKUP_CREATION_SUCCESS;
  constructor(public payload: SimulatedPickupResponseInterface) {}
}

export class SimulatePickupCreationFail implements Action {
  readonly type = SIMULATE_PICKUP_CREATION_FAIL;
  constructor(public payload: any) {}
}

export class PickupPackageChanged implements Action {
  readonly type = PICKUP_PACKAGE_CHANGED;
}

export class CreatePickup implements Action {
  readonly type = CREATE_PICKUP;
  constructor(public payload: CreatePickupInterface) {}
}

export class CreatePickupSuccess implements Action {
  readonly type = CREATE_PICKUP_SUCCESS;
  constructor(public payload: CreatePickupResponseInterface) {}
}

export class CreatePickupFail implements Action {
  readonly type = CREATE_PICKUP_FAIL;
  constructor(public payload: any) {}
}

export class AssignPickups implements Action {
  readonly type = ASSIGN_PICKUPS;
  constructor(public payload: PickupAssignationInterface) {}
}

export class AssignPickupsSuccess implements Action {
  readonly type = ASSIGN_PICKUPS_SUCCESS;
  constructor(public payload: PickupAssignationResponseInterface) {}
}

export class AssignPickupsFail implements Action {
  readonly type = ASSIGN_PICKUPS_FAIL;
  constructor(public payload: any) {}
}

export class ValidatePickup implements Action {
  readonly type = VALIDATE_PICKUP;
  constructor(public payload: string) {}
}

export class ValidatePickupSuccess implements Action {
  readonly type = VALIDATE_PICKUP_SUCCESS;
  constructor(public payload: PickupValidationResponseInterface) {}
}

export class ValidatePickupFail implements Action {
  readonly type = VALIDATE_PICKUP_FAIL;
  constructor(public payload: any) {}
}

export class SendCode implements Action {
  readonly type = SEND_CODE;
  constructor(public pickupId: string) {}
}

export class SendCodeSuccess implements Action {
  readonly type = SEND_CODE_SUCCESS;
}

export class SendCodeFail implements Action {
  readonly type = SEND_CODE_FAIL;
  constructor(public payload: any) {}
}

export class ValidateCode implements Action {
  readonly type = VALIDATE_CODE;
  constructor(public pickupId: string, public payload: string) {}
}

export class ValidateCodeSuccess implements Action {
  readonly type = VALIDATE_CODE_SUCCESS;
  constructor(public payload: SpecificPickupLoadInterface) {}
}

export class ValidateCodeFail implements Action {
  readonly type = VALIDATE_CODE_FAIL;
  constructor(public payload: any) {}
}

export class ReinitializeStore implements Action {
  readonly type = REINITIALIZE_STORE;
}

// export action type
export type PickupsAction =
  | LoadPickups
  | LoadPickupsFail
  | LoadPickupsSuccess
  | LoadSpecificPickup
  | LoadSpecificPickupSuccess
  | LoadSpecificPickupFail
  | SimulatePickupCreation
  | SimulatePickupCreationSuccess
  | SimulatePickupCreationFail
  | PickupPackageChanged
  | CreatePickup
  | CreatePickupSuccess
  | CreatePickupFail
  | AssignPickups
  | AssignPickupsSuccess
  | AssignPickupsFail
  | ValidatePickup
  | ValidatePickupSuccess
  | ValidatePickupFail
  | SendCode
  | SendCodeSuccess
  | SendCodeFail
  | ValidateCode
  | ValidateCodeSuccess
  | ValidateCodeFail
  | ReinitializeStore;
