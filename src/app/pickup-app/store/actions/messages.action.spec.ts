import * as fromMessages from './messages.action';

describe('Messages Action', () => {
  describe('Load Messages Actions', () => {
    describe('Load Messages', () => {
      it('should create an action', () => {
        const action = new fromMessages.LoadMessages();
        expect({ ...action }).toEqual({
          type: fromMessages.LOAD_MESSAGES
        });
      });
    });

    describe('Load Messages Success', () => {
      it('should create an action', () => {
        const payload = {
          success: {
            description: 'Load Messages Success',
            message: 'load.message.success'
          },
          data: {
            messages: [
              {
                createdAt: '2017-11-06T22:58:40.716Z',
                from: {
                  lastname: 'Test last',
                  firstname: 'Test first'
                },
                content: 'Test msg content'
              }
            ]
          }
        };
        const action = new fromMessages.LoadMessagesSuccess(payload);
        expect({ ...action }).toEqual({
          type: fromMessages.LOAD_MESSAGES_SUCCESS,
          payload
        });
      });
    });

    describe('Load Messages Fail', () => {
      it('should create an action', () => {
        const payload = {
          error: {
            description: 'Load Messages Fail',
            message: 'load.message.error'
          }
        };
        const action = new fromMessages.LoadMessagesFail(payload);
        expect({ ...action }).toEqual({
          type: fromMessages.LOAD_MESSAGES_FAIL,
          payload
        });
      });
    });

    describe('Reinitialze Message store', () => {
      it('should create an action', () => {
        const action = new fromMessages.ReinitializeMessagesStore();
        expect({ ...action }).toEqual({
          type: fromMessages.REINITIALIZE_MESSAGES_STORE
        });
      });
    });
  });
});
