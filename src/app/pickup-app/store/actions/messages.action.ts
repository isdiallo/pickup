import { Action } from '@ngrx/store';
import { MessageResponseInterface } from '../../models';

// Load messages
export const LOAD_MESSAGES = '[App Pickup::Message] load message';
export const LOAD_MESSAGES_SUCCESS =
  '[App Pickup::Message] load message success';
export const LOAD_MESSAGES_FAIL = '[App Pickup::Message] load message fail';

// Reinitialize messages
export const REINITIALIZE_MESSAGES_STORE =
  '[App Pickup::Message] reinitialize message store';

export class LoadMessages implements Action {
  readonly type = LOAD_MESSAGES;
}

export class LoadMessagesSuccess implements Action {
  readonly type = LOAD_MESSAGES_SUCCESS;
  constructor(public payload: MessageResponseInterface) {}
}

export class LoadMessagesFail implements Action {
  readonly type = LOAD_MESSAGES_FAIL;
  constructor(public payload: any) {}
}

export class ReinitializeMessagesStore implements Action {
  readonly type = REINITIALIZE_MESSAGES_STORE;
}

// Export the message type
export type MessageActionType =
  | LoadMessages
  | LoadMessagesSuccess
  | LoadMessagesFail
  | ReinitializeMessagesStore;
