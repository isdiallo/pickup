import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import * as fromPickups from './pickups.reducer';
import * as fromUsers from './users.reducers';
import * as fromMessages from './message.reducer';

export interface AppPickupState {
  pickups: fromPickups.PickupState;
  users: fromUsers.UserState;
  messages: fromMessages.MessageState;
}

export const reducers: ActionReducerMap<AppPickupState> = {
  pickups: fromPickups.reducer,
  users: fromUsers.reducer,
  messages: fromMessages.reducer
};

export const getAppPickupState = createFeatureSelector<AppPickupState>(
  'appPickup'
);
