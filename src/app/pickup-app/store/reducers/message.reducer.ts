import { reduce } from 'rxjs/operators';

import * as fromMessages from '../actions/messages.action';
import { MessageInterface } from '../../models';

export interface MessageState {
  messages: MessageInterface[];
  loading: boolean;
  loaded: boolean;
}

export const initialState: MessageState = {
  messages: [],
  loaded: false,
  loading: false,
};

export function reducer(
  state = initialState,
  action: fromMessages.MessageActionType
): MessageState {
  switch (action.type) {
    case fromMessages.LOAD_MESSAGES: {
      return {
        ...state,
        loaded: false,
        loading: true,
      };
    }

    case fromMessages.LOAD_MESSAGES_SUCCESS: {
      const messages = action.payload.data.messages;
      return {
        ...state,
        loaded: true,
        loading: false,
        messages,
      };
    }

    case fromMessages.LOAD_MESSAGES_FAIL: {
      return {
        ...state,
        loaded: false,
        loading: false,
      };
    }
    case fromMessages.REINITIALIZE_MESSAGES_STORE: {
      return initialState;
    }
  }
  return state;
}

// Message selectors
export const getMessages = (state: MessageState) => state.messages;
export const getMessageLoading = (state: MessageState) => state.loading;
export const getMessagesLoaded = (state: MessageState) => state.loaded;
