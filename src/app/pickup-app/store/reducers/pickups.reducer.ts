import { reduce } from 'rxjs/operators';

import * as fromPickups from '../actions/pickups.action';
import { PickupInterface } from '../../../pickup-shared/model/pickup.model';

export interface PickupState {
  entities: { [id: string]: PickupInterface };
  loaded: boolean;
  loading: boolean;
  simulationCharge: number;
}

export const initialState: PickupState = {
  entities: {},
  loaded: false,
  loading: false,
  simulationCharge: null,
};

export function reducer(
  state = initialState,
  action: fromPickups.PickupsAction
): PickupState {
  switch (action.type) {
    case fromPickups.CREATE_PICKUP:
    case fromPickups.VALIDATE_CODE:
    case fromPickups.SEND_CODE:
    case fromPickups.VALIDATE_PICKUP:
    case fromPickups.ASSIGN_PICKUPS:
    case fromPickups.SIMULATE_PICKUP_CREATION:
    case fromPickups.LOAD_SPECIFIC_PICKUP:
    case fromPickups.LOAD_PICKUPS: {
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    }

    case fromPickups.LOAD_PICKUPS_SUCCESS: {
      const pickups = action.payload;
      const entities = pickups.data.reduce(
        (retEntities: { [key: string]: PickupInterface }, pickup) => {
          return {
            ...retEntities,
            [pickup._id]: pickup,
          };
        },
        {
          ...state.entities,
        }
      );
      return {
        ...state,
        loaded: true,
        loading: false,
        entities,
      };
    }

    case fromPickups.SEND_CODE_FAIL:
    case fromPickups.VALIDATE_CODE_FAIL:
    case fromPickups.LOAD_PICKUPS_FAIL:
    case fromPickups.VALIDATE_PICKUP_FAIL:
    case fromPickups.ASSIGN_PICKUPS_FAIL:
    case fromPickups.LOAD_SPECIFIC_PICKUP_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false,
      };
    }

    case fromPickups.VALIDATE_CODE_SUCCESS:
    case fromPickups.LOAD_SPECIFIC_PICKUP_SUCCESS: {
      const pickup: PickupInterface = action.payload.data;
      const entities = {
        ...state.entities,
        [pickup._id]: pickup,
      };

      return {
        ...state,
        loading: false,
        loaded: true,
        entities,
      };
    }
    case fromPickups.VALIDATE_PICKUP_SUCCESS: {
      const validateId = action.payload.data._id;
      const validatedPickup = {
        ...state.entities[validateId],
        state: {
          ...state.entities[validateId].state,
          name: 'VALIDATED',
        },
      };
      const entities = {
        ...state.entities,
        [validateId]: validatedPickup,
      };
      return {
        ...state,
        loaded: true,
        loading: false,
        entities,
      };
    }

    case fromPickups.ASSIGN_PICKUPS_SUCCESS: {
      let entities = state.entities;
      for (const pickup of action.payload.data.success) {
        const { [pickup]: removed, ...newEntities } = entities;
        entities = newEntities;
      }
      return {
        ...state,
        loaded: true,
        loading: false,
        entities,
      };
    }

    case fromPickups.SIMULATE_PICKUP_CREATION_SUCCESS: {
      const simulationCharge = action.payload.data.charge;
      return {
        ...state,
        loaded: true,
        loading: false,
        simulationCharge,
      };
    }

    case fromPickups.SIMULATE_PICKUP_CREATION_FAIL: {
      const simulationCharge = null;
      return {
        ...state,
        loaded: false,
        loading: false,
        simulationCharge,
      };
    }

    case fromPickups.PICKUP_PACKAGE_CHANGED: {
      const simulationCharge = null;
      return {
        ...state,
        simulationCharge,
      };
    }

    case fromPickups.CREATE_PICKUP_SUCCESS: {
      const simulationCharge = null;
      const pickup = {
        ...action.payload.data,
      };
      const entities = {
        ...state.entities,
        [pickup._id]: pickup,
      };
      return {
        ...state,
        loading: false,
        loaded: true,
        entities,
        simulationCharge,
      };
    }

    case fromPickups.CREATE_PICKUP_FAIL: {
      const simulationCharge = null;
      return {
        ...state,
        loaded: false,
        loading: false,
        simulationCharge,
      };
    }

    case fromPickups.SEND_CODE_SUCCESS: {
      return {
        ...state,
        loaded: true,
        loading: false,
      };
    }

    case fromPickups.REINITIALIZE_STORE: {
      return initialState;
    }
  }
  return state;
}

// Pickups Selectors
export const getPickupsEntities = (state: PickupState) => state.entities;
export const getPickupsLoading = (state: PickupState) => state.loading;
export const getPickupsLoaded = (state: PickupState) => state.loaded;
export const getPickupsSimulationCharge = (state: PickupState) =>
  state.simulationCharge;
