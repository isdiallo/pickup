import * as fromPickups from './pickups.reducer';
import * as fromActions from '../actions/pickups.action';
import { PickupInterface } from '../../../pickup-shared/model/pickup.model';
import {
  PICKUPS,
  PickupCreationResponse
} from '../../../pickup-shared/testing/pickups.data';

describe('Pickups Reducers', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const { initialState } = fromPickups;
      const action = {} as any;
      const state = fromPickups.reducer(undefined, action);
      expect(state).toBe(initialState);
    });
  });

  describe('Load Pickup action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.LoadPickups();
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
    });
  });

  describe('Load Pickup success action', () => {
    it('should map array to entities', () => {
      const pickups: PickupInterface[] = PICKUPS;
      const entities = {
        '5a86067442de7a002093fe5f': pickups[0],
        '5a8dccc3aec7ea08284104a2': pickups[1]
      };
      const { initialState } = fromPickups;
      const action = new fromActions.LoadPickupsSuccess({
        data: PICKUPS,
        success: {
          description: 'load pickups success',
          message: 'load.pickups.success'
        }
      });
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual(entities);
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Load Pickup fail action', () => {
    it('should return loading and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.LoadPickupsFail({});
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Create Pickup action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.CreatePickup({
        packages: {},
        endUser: {
          firstname: 'End user first',
          lastname: 'End User last'
        },
        fromId: '1254sadf568sdf',
        isFree: false,
        useRelay: false
      });
      const state = fromPickups.reducer(initialState, action);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
    });
  });

  describe('Create Pickup success action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.CreatePickupSuccess(
        PickupCreationResponse
      );
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({
        '5af340c90984340fe845611c': PickupCreationResponse.data
      });
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Create Pickup fail action', () => {
    it('should set loading and loaded to false [with default state]', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.CreatePickupFail({});
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
    });

    it('should set loading and loaded to false', () => {
      const entities: { [key: string]: PickupInterface } = {
        '5a86067442de7a002093fe5f': PICKUPS[0],
        '5a8dccc3aec7ea08284104a2': PICKUPS[1]
      };
      const { initialState } = fromPickups;
      const previousState = { ...initialState, entities };
      const action = new fromActions.CreatePickupFail({});
      const state = fromPickups.reducer(previousState, action);
      expect(state.entities).toEqual(entities);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Send Code action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.SendCode('1256sadf5');
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
    });
  });

  describe('Send Code success action', () => {
    it('should set loading to false and loaded to true', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.SendCodeSuccess();
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Send Code fail action', () => {
    it('should return loading and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.SendCodeFail({});
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Validate Code action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.ValidateCode('125545sadfs24', '145223');
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
    });
  });

  describe('Validate Code success action', () => {
    it('should set loading to false and loaded to true and update the entities', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.ValidateCodeSuccess({
        success: {
          message: 'validate.code.success',
          description: 'validate code success'
        },
        data: PICKUPS[0]
      });
      const entities: { [key: string]: PickupInterface } = {
        '5a86067442de7a002093fe5f': PICKUPS[0]
      };
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual(entities);
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Validate Code fail action', () => {
    it('should return loading and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.ValidateCodeFail({});
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Validate pickup action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.ValidatePickup('5a86067442de7a002093fe5f');
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
    });
  });

  describe('Validate Pickup success action', () => {
    it('should set loading to false and loaded to true and update the entities', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.ValidatePickupSuccess({
        success: {
          message: 'validate.code.success',
          description: 'validate code success'
        },
        data: PICKUPS[0]
      });
      const entities: { [key: string]: PickupInterface } = {
        '5a86067442de7a002093fe5f': PICKUPS[0]
      };
      const previousState = { ...initialState, entities };
      const state = fromPickups.reducer(previousState, action);
      expect(state.entities).toEqual(entities);
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Validate Pickup fail action', () => {
    it('should return loading and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.ValidatePickupFail({});
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Assign pickup action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.AssignPickups({
        pickups: ['454sadf'],
        to: 'asdfsad5645'
      });
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
    });
  });

  describe('Assign Pickup success action', () => {
    it('should set loading to false and loaded to true and update the entities', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.AssignPickupsSuccess({
        success: {
          message: 'assign.pickup.success',
          description: 'Assign pickup success'
        },
        data: {
          success: ['5a8dccc3aec7ea08284104a2'],
          error: []
        }
      });
      const previousEntities: { [key: string]: PickupInterface } = {
        '5a86067442de7a002093fe5f': PICKUPS[0],
        '5a8dccc3aec7ea08284104a2': PICKUPS[1]
      };
      const entities: { [key: string]: PickupInterface } = {
        '5a86067442de7a002093fe5f': PICKUPS[0]
      };
      const previousState = { ...initialState, entities: previousEntities };
      const state = fromPickups.reducer(previousState, action);
      expect(state.entities).toEqual(entities);
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Assign Pickup fail action', () => {
    it('should return loading and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.AssignPickupsFail({});
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Similate pickup creation action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.SimulatePickupCreation({
        packages: {},
        endUser: {
          firstname: 'End user first',
          lastname: 'End User last'
        },
        fromId: '1254sadf568sdf',
        isFree: false,
        useRelay: false
      });
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
    });
  });

  describe('Simulate Pickup creation success action', () => {
    it('should set loading to false and loaded to true and update simulation charge', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.SimulatePickupCreationSuccess({
        success: {
          description: 'simulate pickup creation success',
          message: 'simulate.pickup.creation.success'
        },
        data: { charge: 1500 }
      });
      const entities: { [key: string]: PickupInterface } = {
        '5a86067442de7a002093fe5f': PICKUPS[0],
        '5a8dccc3aec7ea08284104a2': PICKUPS[1]
      };
      const previousState = { ...initialState, entities };
      const state = fromPickups.reducer(previousState, action);
      expect(state.entities).toEqual(entities);
      expect(state.simulationCharge).toEqual(1500);
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Simulate Pickup creation fail action', () => {
    it('should return loading and loaded to false and put simulation charge to null', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.SimulatePickupCreationFail({});
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.simulationCharge).toEqual(null);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Load Specific pickup action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.LoadSpecificPickup(
        '5a86067442de7a002093fe5f'
      );
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
    });
  });

  describe('Load Specific Pickup success action', () => {
    it('should set loading to false and loaded to true and update the entities', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.LoadSpecificPickupSuccess({
        success: {
          description: 'load specific pickup success',
          message: 'load.specific.pickup.success'
        },
        data: PICKUPS[0]
      });
      const entities: { [key: string]: PickupInterface } = {
        '5a86067442de7a002093fe5f': PICKUPS[0]
      };
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual(entities);
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Load specific Pickup fail action', () => {
    it('should return loading and loaded to false', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.LoadSpecificPickupFail({});
      const state = fromPickups.reducer(initialState, action);
      expect(state.entities).toEqual({});
      expect(state.simulationCharge).toEqual(null);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Reload pickup store action', () => {
    it('should return the initial store', () => {
      const { initialState } = fromPickups;
      const action = new fromActions.ReinitializeStore();
      const entities: { [key: string]: PickupInterface } = {
        '5a86067442de7a002093fe5f': PICKUPS[0],
        '5a8dccc3aec7ea08284104a2': PICKUPS[1]
      };
      const previousState = { ...initialState, entities };
      const state = fromPickups.reducer(previousState, action);
      expect(state.entities).toEqual({});
      expect(state.simulationCharge).toEqual(null);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
    });
  });

  // Test of reducers selectors

  describe('Pickup Reducer Selectors', () => {
    describe('getPickupsEntities', () => {
      it('should return entities', () => {
        const entities: { [key: string]: PickupInterface } = {
          '5a86067442de7a002093fe5f': PICKUPS[0],
          '5a8dccc3aec7ea08284104a2': PICKUPS[1]
        };
        const { initialState } = fromPickups;
        const previousState = { ...initialState, entities };
        const slice = fromPickups.getPickupsEntities(previousState);
        expect(slice).toEqual(entities);
      });
    });

    describe('getPickupsLoading', () => {
      it('should return loading', () => {
        const { initialState } = fromPickups;
        const previousState = { ...initialState, loading: true };
        const slice = fromPickups.getPickupsLoading(previousState);
        expect(slice).toEqual(true);
      });
    });

    describe('getPickupsLoaded', () => {
      it('should return loaded', () => {
        const { initialState } = fromPickups;
        const previousState = { ...initialState, loaded: true };
        const slice = fromPickups.getPickupsLoaded(previousState);
        expect(slice).toEqual(true);
      });
    });

    describe('getPickupsSimulationCharge', () => {
      it('should return simulation charge', () => {
        const { initialState } = fromPickups;
        const previousState = { ...initialState, simulationCharge: 150 };
        const slice = fromPickups.getPickupsSimulationCharge(previousState);
        expect(slice).toEqual(150);
      });
    });
  });
});
