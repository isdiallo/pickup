import * as fromUsers from './users.reducers';
import * as fromActions from '../actions/users.action';
import { AGENTS, USERSRELAY } from '../../../pickup-shared/testing/users.data';

describe('Users Reducers', () => {
  describe('Undefined action', () => {
    it('should return default state', () => {
      const { initialState } = fromUsers;
      const action = {} as any;
      const state = fromUsers.reducer(undefined, action);
      expect(state).toBe(initialState);
    });
  });

  describe('Load users agent action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromUsers;
      const action = new fromActions.LoadUsersAgent();
      const state = fromUsers.reducer(initialState, action);
      expect(state.usersAgent).toEqual([]);
      expect(state.agentsLoaded).toEqual(false);
      expect(state.agentsLoading).toEqual(true);
    });
  });

  describe('Load users relay action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromUsers;
      const action = new fromActions.LoadUsersRelay();
      const state = fromUsers.reducer(initialState, action);
      expect(state.usersRelay).toEqual([]);
      expect(state.relaysLoaded).toEqual(false);
      expect(state.relaysLoading).toEqual(true);
    });
  });

  describe('Load users agent success action', () => {
    it('should update users agent list, set loading to false and loaded to true', () => {
      const { initialState } = fromUsers;
      const action = new fromActions.LoadUsersAgentSuccess({
        success: {
          message: 'load.users.agents.success',
          description: 'load users agent success'
        },
        data: AGENTS
      });
      const state = fromUsers.reducer(initialState, action);
      expect(state.usersAgent).toEqual(AGENTS);
      expect(state.agentsLoaded).toEqual(true);
      expect(state.agentsLoading).toEqual(false);
    });
  });

  describe('Load users relay success action', () => {
    it('should update users relay list, set loading to false and loaded to true', () => {
      const { initialState } = fromUsers;
      const action = new fromActions.LoadUsersRelaySuccess({
        success: {
          message: 'load.users.relay.success',
          description: 'load users relay success'
        },
        data: USERSRELAY
      });
      const state = fromUsers.reducer(initialState, action);
      expect(state.usersRelay).toEqual(USERSRELAY);
      expect(state.relaysLoaded).toEqual(true);
      expect(state.relaysLoading).toEqual(false);
    });
  });

  describe('Load users agent fail action', () => {
    it('should set loading to false and loaded to false', () => {
      const { initialState } = fromUsers;
      const action = new fromActions.LoadUsersAgentFail({});
      const state = fromUsers.reducer(initialState, action);
      expect(state.usersAgent).toEqual([]);
      expect(state.agentsLoaded).toEqual(false);
      expect(state.agentsLoading).toEqual(false);
    });
  });

  describe('Load users relay fail action', () => {
    it('should set loading to false and loaded to false', () => {
      const { initialState } = fromUsers;
      const action = new fromActions.LoadUsersRelayFail({});
      const state = fromUsers.reducer(initialState, action);
      expect(state.usersRelay).toEqual([]);
      expect(state.relaysLoaded).toEqual(false);
      expect(state.relaysLoading).toEqual(false);
    });
  });

  // Test reducers selector

  describe('Users Reducer Selectors', () => {
    describe('getUsersAgent', () => {
      it('should return users agent', () => {
        const usersAgent = AGENTS;
        const { initialState } = fromUsers;
        const previousState = { ...initialState, usersAgent };
        const slice = fromUsers.getUsersAgent(previousState);
        expect(slice).toEqual(usersAgent);
      });
    });

    describe('getUsersAgentLoading', () => {
      it('should return loading', () => {
        const { initialState } = fromUsers;
        const previousState = { ...initialState, agentsLoading: true };
        const slice = fromUsers.getUsersAgentLoading(previousState);
        expect(slice).toEqual(true);
      });
    });

    describe('getUsersAgentLoaded', () => {
      it('should return loaded', () => {
        const { initialState } = fromUsers;
        const previousState = { ...initialState, agentsLoaded: true };
        const slice = fromUsers.getUsersAgentLoaded(previousState);
        expect(slice).toEqual(true);
      });
    });

    describe('getUsersRelay', () => {
      it('should return users relay', () => {
        const usersRelay = USERSRELAY;
        const { initialState } = fromUsers;
        const previousState = { ...initialState, usersRelay };
        const slice = fromUsers.getUsersRelay(previousState);
        expect(slice).toEqual(usersRelay);
      });
    });

    describe('getUsersRelayLoading', () => {
      it('should return loading', () => {
        const { initialState } = fromUsers;
        const previousState = { ...initialState, relaysLoading: true };
        const slice = fromUsers.getUsersRelayLoading(previousState);
        expect(slice).toEqual(true);
      });
    });

    describe('getUsersRelayLoaded', () => {
      it('should return loaded', () => {
        const { initialState } = fromUsers;
        const previousState = { ...initialState, relaysLoaded: true };
        const slice = fromUsers.getUsersRelayLoaded(previousState);
        expect(slice).toEqual(true);
      });
    });
  });
});
