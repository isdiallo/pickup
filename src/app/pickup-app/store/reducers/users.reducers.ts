import { reduce } from 'rxjs/operators';

import * as fromUsers from '../actions/users.action';
import { UserRelayInterface, UserInterface } from '../../models';

export interface UserState {
  usersRelay: UserRelayInterface[];
  usersAgent: UserInterface[];
  relaysLoaded: boolean;
  relaysLoading: boolean;
  agentsLoaded: boolean;
  agentsLoading: boolean;
}

export const initialState = {
  usersRelay: [],
  usersAgent: [],
  relaysLoaded: false,
  relaysLoading: false,
  agentsLoaded: false,
  agentsLoading: false,
};

export function reducer(
  state = initialState,
  action: fromUsers.UsersAction
): UserState {
  switch (action.type) {
    case fromUsers.LOAD_USERS_AGENT: {
      return {
        ...state,
        agentsLoading: true,
        agentsLoaded: false,
      };
    }
    case fromUsers.LOAD_USERS_AGENT_SUCCESS: {
      const usersAgent = action.payload.data;
      return {
        ...state,
        usersAgent,
        agentsLoaded: true,
        agentsLoading: false,
      };
    }
    case fromUsers.LOAD_USERS_AGENT_FAIL: {
      return {
        ...state,
        agentsLoaded: false,
        agentsLoading: false,
      };
    }
    case fromUsers.LOAD_USERS_RELAY: {
      return {
        ...state,
        relaysLoading: true,
        relaysLoaded: false,
      };
    }
    case fromUsers.LOAD_USERS_RELAY_SUCCESS: {
      const usersRelay = action.payload.data;

      return {
        ...state,
        usersRelay,
        relaysLoaded: true,
        relaysLoading: false,
      };
    }
    case fromUsers.LOAD_USERS_RELAY_FAIL: {
      return {
        ...state,
        relaysLoading: false,
        relaysLoaded: false,
      };
    }
  }
  return state;
}

// Direct selectors
export const getUsersAgent = (state: UserState) => state.usersAgent;
export const getUsersAgentLoaded = (state: UserState) => state.agentsLoaded;
export const getUsersAgentLoading = (state: UserState) => state.agentsLoading;

export const getUsersRelay = (state: UserState) => state.usersRelay;
export const getUsersRelayLoaded = (state: UserState) => state.relaysLoaded;
export const getUsersRelayLoading = (state: UserState) => state.relaysLoading;
