import * as fromMessages from './message.reducer';
import * as fromActions from '../actions/messages.action';
import { MSGs } from '../../../pickup-shared/testing/messages.data';

describe('Messages Reducers', () => {
  describe('Undefined action', () => {
    it('should return default state', () => {
      const { initialState } = fromMessages;
      const action = {} as any;
      const state = fromMessages.reducer(undefined, action);
      expect(state).toBe(initialState);
    });
  });

  describe('Load messages action', () => {
    it('should set loading to true and loaded to false', () => {
      const { initialState } = fromMessages;
      const action = new fromActions.LoadMessages();
      const state = fromMessages.reducer(initialState, action);
      expect(state.messages).toEqual([]);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(true);
    });
  });

  describe('Load messages success action', () => {
    it('should load messages, set loading to false and loaded to true', () => {
      const { initialState } = fromMessages;
      const action = new fromActions.LoadMessagesSuccess({
        success: {
          message: 'load.msg.success',
          description: 'load msg success'
        },
        data: { messages: MSGs }
      });
      const state = fromMessages.reducer(initialState, action);
      expect(state.messages).toEqual(MSGs);
      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Load messages fail action', () => {
    it('should set loading to false and loaded to false', () => {
      const { initialState } = fromMessages;
      const action = new fromActions.LoadMessagesFail({});
      const state = fromMessages.reducer(initialState, action);
      expect(state.messages).toEqual([]);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
    });
  });

  describe('Reinitialize message store action', () => {
    it('should set state to default', () => {
      const { initialState } = fromMessages;
      const action = new fromActions.ReinitializeMessagesStore();
      const messages = MSGs;
      const previousState = { ...initialState, messages };
      const state = fromMessages.reducer(previousState, action);
      expect(state.messages).toEqual([]);
      expect(state.loaded).toEqual(false);
      expect(state.loading).toEqual(false);
    });
  });

  // Test of reducers selector

  describe('Message Reducer Selectors', () => {
    describe('getMessages', () => {
      it('should return messages', () => {
        const messages = MSGs;
        const { initialState } = fromMessages;
        const previousState = { ...initialState, messages };
        const slice = fromMessages.getMessages(previousState);
        expect(slice).toEqual(messages);
      });
    });

    describe('getMessageLoading', () => {
      it('should return loading equal true', () => {
        const { initialState } = fromMessages;
        const previousState = { ...initialState, loading: true };
        const slice = fromMessages.getMessageLoading(previousState);
        expect(slice).toEqual(true);
      });
    });

    describe('getMessageLoaded', () => {
      it('should return loaded equal true', () => {
        const { initialState } = fromMessages;
        const previousState = { ...initialState, loaded: true };
        const slice = fromMessages.getMessagesLoaded(previousState);
        expect(slice).toEqual(true);
      });
    });
  });
});
