import { PickupsEffect } from './pickups.effect';
import { UsersEffect } from './users.effect';
import { MessagesEffect } from './messages.effect';

export const effects: any[] = [PickupsEffect, UsersEffect, MessagesEffect];

export * from './pickups.effect';
export * from './users.effect';
export * from './messages.effect';
