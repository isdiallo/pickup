import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';

import { switchMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import * as fromPickupsAction from '../actions';
import * as fromServices from '../../services';

import * as fromRoot from '../../../store';

@Injectable()
export class PickupsEffect {
  constructor(
    private actions$: Actions,
    private PickupsService: fromServices.PickupService
  ) {}

  @Effect()
  loadPickups$ = this.actions$.pipe(
    ofType(fromPickupsAction.LOAD_PICKUPS),
    switchMap(() => {
      return this.PickupsService.getPickups().pipe(
        map((pickups) => new fromPickupsAction.LoadPickupsSuccess(pickups)),
        catchError((error) => of(new fromPickupsAction.LoadPickupsFail(error)))
      );
    })
  );

  @Effect()
  loadSpecificPickup$ = this.actions$.pipe(
    ofType(fromPickupsAction.LOAD_SPECIFIC_PICKUP),
    map((action: fromPickupsAction.LoadSpecificPickup) => action.payload),
    switchMap((pickupId) => {
      return this.PickupsService.getSpecificPickup(pickupId).pipe(
        map(
          (pickup) => new fromPickupsAction.LoadSpecificPickupSuccess(pickup)
        ),
        catchError((error) =>
          of(new fromPickupsAction.LoadSpecificPickupFail(error))
        )
      );
    })
  );

  @Effect()
  simulatePickupCreation$ = this.actions$.pipe(
    ofType(fromPickupsAction.SIMULATE_PICKUP_CREATION),
    map((action: fromPickupsAction.SimulatePickupCreation) => action.payload),
    switchMap((simulatedPickup) =>
      this.PickupsService.simulatePickupCreation(simulatedPickup).pipe(
        map(
          (pickupCharge) =>
            new fromPickupsAction.SimulatePickupCreationSuccess(pickupCharge)
        ),
        catchError((error) =>
          of(new fromPickupsAction.SimulatePickupCreationFail(error))
        )
      )
    )
  );

  @Effect()
  createPickup$ = this.actions$.pipe(
    ofType(fromPickupsAction.CREATE_PICKUP),
    map((action: fromPickupsAction.CreatePickup) => action.payload),
    switchMap((pickup) =>
      this.PickupsService.createPickup(pickup).pipe(
        map(
          (createdPickup) =>
            new fromPickupsAction.CreatePickupSuccess(createdPickup)
        ),
        catchError((error) => of(new fromPickupsAction.CreatePickupFail(error)))
      )
    )
  );

  @Effect()
  createPickupSuccess$ = this.actions$.pipe(
    ofType(fromPickupsAction.CREATE_PICKUP_SUCCESS),
    map((action: fromPickupsAction.CreatePickupSuccess) => action.payload.data),
    map((pickup) => {
      return new fromRoot.Go({
        path: ['/app/pickup', pickup._id],
      });
    })
  );

  @Effect()
  assignPickup$ = this.actions$.pipe(
    ofType(fromPickupsAction.ASSIGN_PICKUPS),
    map((action: fromPickupsAction.AssignPickups) => action.payload),
    switchMap((assignPick) =>
      this.PickupsService.assignPickup(assignPick).pipe(
        map(
          (assignedPick) =>
            new fromPickupsAction.AssignPickupsSuccess(assignedPick)
        ),
        catchError((error) =>
          of(new fromPickupsAction.AssignPickupsFail(error))
        )
      )
    )
  );

  @Effect()
  validatePickup$ = this.actions$.pipe(
    ofType(fromPickupsAction.VALIDATE_PICKUP),

    map((action: fromPickupsAction.ValidatePickup) => action.payload),
    switchMap((pickupId: string) =>
      this.PickupsService.validatePickup(pickupId).pipe(
        map(
          (validatedId) =>
            new fromPickupsAction.ValidatePickupSuccess(validatedId)
        ),
        catchError((error) =>
          of(new fromPickupsAction.ValidatePickupFail(error))
        )
      )
    )
  );

  @Effect()
  sendCode$ = this.actions$.pipe(
    ofType(fromPickupsAction.SEND_CODE),
    map((action: fromPickupsAction.SendCode) => action.pickupId),
    switchMap((pickupId: string) =>
      this.PickupsService.sendCode(pickupId).pipe(
        map((_) => new fromPickupsAction.SendCodeSuccess()),
        catchError((error) => of(new fromPickupsAction.SendCodeFail(error)))
      )
    )
  );

  @Effect()
  validateCode$ = this.actions$.pipe(
    ofType(fromPickupsAction.VALIDATE_CODE),
    switchMap((action: fromPickupsAction.ValidateCode) =>
      this.PickupsService.validateCode(action.pickupId, action.payload).pipe(
        map((pickup) => new fromPickupsAction.ValidateCodeSuccess(pickup)),
        catchError((error) => of(new fromPickupsAction.ValidateCodeFail(error)))
      )
    )
  );
}
