import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';

import * as fromMessages from '../actions/messages.action';
import { map, switchMap, catchError } from 'rxjs/operators';
import { MessagesService } from '../../services';
import { of } from 'rxjs';

@Injectable()
export class MessagesEffect {
  constructor(
    private actions$: Actions,
    private MessageService: MessagesService
  ) {}

  @Effect()
  loadMessages$ = this.actions$.pipe(
    ofType(fromMessages.LOAD_MESSAGES),
    switchMap(() =>
      this.MessageService.loadMessagess().pipe(
        map((messages) => new fromMessages.LoadMessagesSuccess(messages)),
        catchError((error) => of(new fromMessages.LoadMessagesFail(error)))
      )
    )
  );
}
