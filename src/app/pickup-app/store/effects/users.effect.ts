import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';

import { switchMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import * as fromUsersAction from '../actions/users.action';

import { UsersService } from '../../services/users.service';

@Injectable()
export class UsersEffect {
  constructor(private actions$: Actions, private UserService: UsersService) {}

  @Effect()
  loadAgents$ = this.actions$.pipe(
    ofType(fromUsersAction.LOAD_USERS_AGENT),
    switchMap(() => {
      return this.UserService.getAgents().pipe(
        map((agents) => new fromUsersAction.LoadUsersAgentSuccess(agents)),
        catchError((error) => of(new fromUsersAction.LoadUsersAgentFail(error)))
      );
    })
  );

  @Effect()
  loadUsersRelay$ = this.actions$.pipe(
    ofType(fromUsersAction.LOAD_USERS_RELAY),
    switchMap(() => {
      return this.UserService.getUsersRelay().pipe(
        map(
          (usersRelay) => new fromUsersAction.LoadUsersRelaySuccess(usersRelay)
        ),
        catchError((error) => of(new fromUsersAction.LoadUsersRelayFail(error)))
      );
    })
  );
}
