import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-info-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class InfoNavbarComponent implements OnInit {
  constructor() {}

  @Input() isAuthenticated: boolean;
  @Output() login = new EventEmitter<any>();
  @Output() logout = new EventEmitter<any>();

  doLogin(event: any) {
    this.login.emit(event);
  }

  doLogout(event: any) {
    this.logout.emit(event);
  }
  ngOnInit() {}
}
