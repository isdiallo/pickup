import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoNavbarComponent } from './navbar.component';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';

import { HasAuthorityStubDirective } from '../../../pickup-shared/testing/HasAuthorityDirective.stub';

describe('NavbarComponent', () => {
  let component: InfoNavbarComponent;
  let fixture: ComponentFixture<InfoNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InfoNavbarComponent, HasAuthorityStubDirective],
      imports: [MatIconModule, MatMenuModule, MatToolbarModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
