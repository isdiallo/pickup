import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';

import { ReactiveFormsModule } from '@angular/forms';
import { AlertListComponent } from '../../../pickup-shared/components/alert-list/alert-list.component';
import { AlertComponent } from '../../../pickup-shared/components/alert/alert.component';
import * as fromSharedStore from '../../../pickup-shared/store';
import * as fromRoot from '../../../store/reducers';

import { Store, StoreModule, combineReducers } from '@ngrx/store';
import { BroadcasterService } from '../../../pickup-shared/services';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Page } from '../../../pickup-shared/testing/page';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { RouterLinkStubDirective } from '../../../pickup-shared/testing/RouterLink.stub';
import { RegisterUserType } from '../../../pickup-shared/model';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let sharedStore: Store<fromSharedStore.AppSharedState>;
  let registerPage: Page<RegisterComponent>;
  let linkDes: DebugElement[];
  let routerLinks: any[];

  function updateRegisterForm(key: string, value: string | boolean) {
    component.registerForm.controls[key].setValue(value);
  }

  function prefillRegisterForm() {
    updateRegisterForm('firstname', 'Firstname test');
    updateRegisterForm('lastname', 'Lastname test');
    updateRegisterForm('type', 'RELAY');
    updateRegisterForm('city', 'City test');
    updateRegisterForm('town', 'Town test');
    updateRegisterForm('address', 'Address test');
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RegisterComponent,
        AlertListComponent,
        AlertComponent,
        RouterLinkStubDirective,
      ],
      imports: [
        StoreModule.forRoot({
          ...fromRoot.reducers,
          features: combineReducers(fromSharedStore.reducers),
        }),
        BrowserAnimationsModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatCheckboxModule,
        ReactiveFormsModule,
        MatCardModule,
      ],
      providers: [BroadcasterService],
    }).compileComponents();
  }));

  beforeEach(() => {
    sharedStore = TestBed.get(Store);
    spyOn(sharedStore, 'dispatch').and.callThrough();

    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;

    registerPage = new Page(fixture);
    fixture.detectChanges();
    linkDes = fixture.debugElement.queryAll(
      By.directive(RouterLinkStubDirective)
    );
    routerLinks = linkDes.map((de) => de.injector.get(RouterLinkStubDirective));
  });

  it('should create and instanciate default properties', () => {
    expect(component).toBeTruthy();
    expect(component.hide).toBe(true);
    expect(component.hideConfirm).toBe(true);
    expect(component.accountTypes).toEqual([
      {
        type: 'RELAY',
        value: 'Relay User Account',
      },
      {
        type: 'MERCHANT',
        value: 'Business User Account',
      },
      {
        type: 'AGENT',
        value: 'Agent User Account',
      },
    ]);
  });

  it('should get all routerLinks in the template', () => {
    expect(linkDes.length).toBe(1, 'should have 1 link debug element');
    expect(routerLinks.length).toBe(1, 'should have 1 routerLinks');
    expect(routerLinks[0].routerLink).toBe('/terms-conditions');
  });

  it('can click on terms and condition link', () => {
    const termsLinkDe = linkDes[0];
    const termsLink = routerLinks[0];
    expect(termsLink.navigatedTo).toBeNull('should not have navigated yet');
    termsLinkDe.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(termsLink.navigatedTo).toBe('/terms-conditions');
  });

  it('should deactivate the register button just after init', () => {
    expect(
      registerPage.query<HTMLButtonElement>('.register-btn').disabled
    ).toBe(true);
  });

  it('should keep the register button deactivated if only username is entered', () => {
    updateRegisterForm('username', 'testusername');
    expect(
      registerPage.query<HTMLButtonElement>('.register-btn').disabled
    ).toBe(true);
  });

  it('should keep the register btn disabled if username is not valid', () => {
    prefillRegisterForm();
    updateRegisterForm('cgu', true);
    updateRegisterForm('password', 'testpwd');
    updateRegisterForm('confirmPassword', 'testpwd');
    updateRegisterForm('username', 'not valid');
    expect(
      registerPage.query<HTMLButtonElement>('.register-btn').disabled
    ).toBe(true);
  });

  it('should keep the register btn disabled if password confimation does not match', () => {
    prefillRegisterForm();
    const username: HTMLInputElement = fixture.debugElement.nativeElement.querySelector(
      '.username input'
    );
    username.value = 'test@test.fr';
    username.dispatchEvent(new Event('input'));
    updateRegisterForm('cgu', true);
    updateRegisterForm('phone', '012365478');
    updateRegisterForm('password', 'testpwd');
    updateRegisterForm('confirmPassword', 'testpwd not match');

    fixture.detectChanges();
    expect(
      registerPage.query<HTMLButtonElement>('.register-btn').disabled
    ).toBe(true);
  });

  it('should enable the register when all info are entered', () => {
    prefillRegisterForm();
    const username: HTMLInputElement = fixture.debugElement.nativeElement.querySelector(
      '.username input'
    );
    username.value = 'test@test.fr';
    username.dispatchEvent(new Event('input'));
    updateRegisterForm('cgu', true);
    updateRegisterForm('phone', '012365478');
    updateRegisterForm('password', 'testpwd');
    updateRegisterForm('confirmPassword', 'testpwd');

    fixture.detectChanges();
    expect(
      registerPage.query<HTMLButtonElement>('.register-btn').disabled
    ).toBe(false);
  });

  it('should show phone input when username is an mail', () => {
    const username: HTMLInputElement = registerPage.query<HTMLInputElement>(
      '.username input'
    );
    username.value = 'test@test.fr';
    username.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(registerPage.query<HTMLElement>('.mail')).toBeFalsy();
    expect(registerPage.query<HTMLElement>('.phone')).toBeTruthy();
  });

  it('should show mail input when username is a phone', () => {
    const username: HTMLInputElement = registerPage.query<HTMLInputElement>(
      '.username input'
    );
    username.value = '123654789';
    username.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(registerPage.query<HTMLElement>('.mail')).toBeTruthy();
    expect(registerPage.query<HTMLElement>('.phone')).toBeFalsy();
  });

  it('should dispatch an action to register when register button is clicked', () => {
    prefillRegisterForm();
    const username: HTMLInputElement = fixture.debugElement.nativeElement.querySelector(
      '.username input'
    );
    username.value = 'test@test.fr';
    username.dispatchEvent(new Event('input'));
    updateRegisterForm('cgu', true);
    updateRegisterForm('phone', '012365478');
    updateRegisterForm('password', 'testpwd');
    updateRegisterForm('confirmPassword', 'testpwd');
    const registerAction = new fromSharedStore.DoRegister({
      username: 'test@test.fr',
      firstname: 'Firstname test',
      lastname: 'Lastname test',
      type: RegisterUserType.Relay,
      city: 'City test',
      town: 'Town test',
      address: 'Address test',
      password: 'testpwd',
      phone: '012365478',
      mail: 'test@test.fr',
    });

    fixture.detectChanges();
    registerPage.query<HTMLButtonElement>('.register-btn').click();
    expect(sharedStore.dispatch).toHaveBeenCalledWith(registerAction);
  });
});
