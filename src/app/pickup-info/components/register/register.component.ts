import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';

import { filter } from 'rxjs/operators';

import { Store } from '@ngrx/store';

import * as fromSharedStore from '../../../pickup-shared/store';
import {
  USER_TYPES,
  PHONE_REGEX,
  MAIL_REGEX
} from '../../../pickup-shared/pickup-shared.constants';
import {
  RegisterInterface,
  PickupEvent
} from '../../../pickup-shared/model/index';
import { BroadcasterService } from '../../../pickup-shared/services';
import { duplicateInputValidator } from '../../../pickup-shared/validators/duplicate-input.validator';
import {
  usernameValidator,
  emailValidator,
  phoneValidator
} from '../../../pickup-shared/validators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  hide = true;
  hideConfirm = true;
  accountTypes = USER_TYPES;
  registerForm: FormGroup;
  messages: PickupEvent[] = [];

  constructor(
    private formBuiler: FormBuilder,
    private sharedStore: Store<fromSharedStore.AppSharedState>,
    private broadcastService: BroadcasterService
  ) {
    this.createForm();
  }

  messages$ = this.broadcastService._eventBus
    .pipe(filter((event: PickupEvent) => event.type === 'error'))
    .subscribe(event => {
      this.messages.push(event);
    });

  createForm() {
    this.registerForm = this.formBuiler.group(
      {
        username: ['', [Validators.required, usernameValidator()]],
        firstname: ['test first', Validators.required],
        lastname: ['last tester', Validators.required],
        type: ['', Validators.required],
        password: ['test', Validators.required],
        confirmPassword: [
          'test',
          [
            Validators.required,
            duplicateInputValidator('password', 'confirmPassword')
          ]
        ],
        address: ['test address', Validators.required],
        city: ['test city', Validators.required],
        town: ['test town', Validators.required],
        cgu: [true, Validators.requiredTrue]
      },
      { validator: duplicateInputValidator('password', 'confirmPassword') }
    );
  }

  usernameChanged(event: any) {
    if (
      event.target &&
      event.target.value &&
      PHONE_REGEX.test(event.target.value)
    ) {
      this.registerForm.addControl(
        'mail',
        new FormControl('', emailValidator())
      );
    } else {
      this.registerForm.removeControl('mail');
    }

    if (
      event.target &&
      event.target.value &&
      MAIL_REGEX.test(event.target.value)
    ) {
      this.registerForm.addControl(
        'phone',
        new FormControl('', phoneValidator())
      );
    } else {
      this.registerForm.removeControl('phone');
    }
  }

  showInput(username: string, type: string): boolean {
    if (type === 'mail') {
      return PHONE_REGEX.test(username);
    } else {
      return MAIL_REGEX.test(username);
    }
  }

  onSubmit() {
    this.sharedStore.dispatch(new fromSharedStore.DoRegister(this.preSubmit()));
  }

  preSubmit(): RegisterInterface {
    // If the username is phone number then assign it to user phone
    if (PHONE_REGEX.test(this.registerForm.value.username)) {
      this.registerForm.value.phone = this.registerForm.value.username;
    }
    // If the username is email then assign it to user email
    if (MAIL_REGEX.test(this.registerForm.value.username)) {
      this.registerForm.value.mail = this.registerForm.value.username;
    }
    const formModel = this.registerForm.value;
    const {
      ['cgu']: cgu,
      ['confirmPassword']: confirnPwd,
      ...registerModel
    } = formModel;
    return registerModel;
  }
  ngOnInit() {}

  ngOnDestroy() {
    this.messages$.unsubscribe();
  }
}
