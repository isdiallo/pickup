import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(private formBuilder: FormBuilder) {
    this.createTrackingForm();
  }
  trackingForm: FormGroup;

  createTrackingForm() {
    this.trackingForm = this.formBuilder.group({
      pickupId: ['', Validators.required]
    });
  }

  onTrack() {}
  ngOnInit() {}
}
