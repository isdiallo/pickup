import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';

import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Page } from '../../../pickup-shared/testing/page';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let homePage: Page<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent],
      imports: [
        MatFormFieldModule,
        MatCardModule,
        ReactiveFormsModule,
        MatInputModule,
        BrowserAnimationsModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    homePage = new Page(fixture);
  });

  it('should create with tracking btn disabled', () => {
    expect(component).toBeTruthy();
    expect(homePage.query<HTMLButtonElement>('.tracking-btn').disabled).toBe(
      true
    );
  });

  it('should enable tracking btn when something entered', () => {
    component.trackingForm.controls['pickupId'].setValue('test pickup id');
    fixture.detectChanges();
    expect(homePage.query<HTMLButtonElement>('.tracking-btn').disabled).toBe(
      false
    );
  });
});
