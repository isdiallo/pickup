import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';

import { ReactiveFormsModule } from '@angular/forms';
import { AlertListComponent } from '../../../pickup-shared/components/alert-list/alert-list.component';
import { AlertComponent } from '../../../pickup-shared/components/alert/alert.component';
import { Store, StoreModule, combineReducers } from '@ngrx/store';
import * as fromSharedStore from '../../../pickup-shared/store';
import * as fromRoot from '../../../store/reducers';
import { BroadcasterService } from '../../../pickup-shared/services';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Page } from '../../../pickup-shared/testing/page';
import { DebugElement } from '@angular/core';
import { RouterLinkStubDirective } from '../../../pickup-shared/testing/RouterLink.stub';
import { By } from '@angular/platform-browser';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let sharedStore: Store<fromSharedStore.AppSharedState>;
  let loginPage: Page<LoginComponent>;
  let linkDes: DebugElement[];
  let routerLinks: any[];

  function updateLoginForm(key: string, value: string) {
    component.loginForm.controls[key].setValue(value);
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [BroadcasterService],
      declarations: [
        LoginComponent,
        AlertListComponent,
        AlertComponent,
        RouterLinkStubDirective,
      ],
      imports: [
        StoreModule.forRoot({
          ...fromRoot.reducers,
          features: combineReducers(fromSharedStore.reducers),
        }),
        BrowserAnimationsModule,
        MatCardModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        ReactiveFormsModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    sharedStore = TestBed.get(Store);
    spyOn(sharedStore, 'dispatch').and.callThrough();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    loginPage = new Page(fixture);
    fixture.detectChanges();
    linkDes = fixture.debugElement.queryAll(
      By.directive(RouterLinkStubDirective)
    );
    routerLinks = linkDes.map((de) => de.injector.get(RouterLinkStubDirective));
  });

  it('should create and instanciate hide property to true', () => {
    expect(component).toBeTruthy();
    expect(component.hide).toEqual(true);
  });

  it('should get all routerLinks in the template', () => {
    expect(linkDes.length).toBe(2, 'should have 2 link debug element');
    expect(routerLinks.length).toBe(2, 'should have 2 routerLinks');
    expect(routerLinks[0].routerLink).toBe('.');
    expect(routerLinks[1].routerLink).toBe('/register');
  });

  it('can click on register link', () => {
    const registerLinkDe = linkDes[1];
    const registerLink = routerLinks[1];
    expect(registerLink.navigatedTo).toBeNull('should not have navigated yet');
    registerLinkDe.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(registerLink.navigatedTo).toBe('/register');
  });

  it('should deactivate the login button just after init', () => {
    expect(loginPage.query<HTMLButtonElement>('.login-btn').disabled).toBe(
      true
    );
  });

  it('should keep the login button deactivated if only username is entered', () => {
    updateLoginForm('username', 'testusername');
    fixture.detectChanges();
    expect(loginPage.query<HTMLButtonElement>('.login-btn').disabled).toBe(
      true
    );
  });

  it('should keep the login button deactivated if only password is entered', () => {
    updateLoginForm('password', 'testpwd');
    fixture.detectChanges();
    expect(loginPage.query<HTMLButtonElement>('.login-btn').disabled).toBe(
      true
    );
  });

  it('should keep the login button deactivated if username is not an email', () => {
    updateLoginForm('username', 'testusername');
    updateLoginForm('password', 'testpwd');
    fixture.detectChanges();
    expect(loginPage.query<HTMLButtonElement>('.login-btn').disabled).toBe(
      true
    );
  });

  it('should keep the login button deactivated if username is a false phone number (not a 9 digits)', () => {
    updateLoginForm('username', '01234567');
    updateLoginForm('password', 'testpwd');
    fixture.detectChanges();
    expect(loginPage.query<HTMLButtonElement>('.login-btn').disabled).toBe(
      true
    );
  });

  it('should keep the login button deactivated if username is a false mail', () => {
    updateLoginForm('username', 'test@test.');
    updateLoginForm('password', 'testpwd');
    fixture.detectChanges();
    expect(loginPage.query<HTMLButtonElement>('.login-btn').disabled).toBe(
      true
    );
  });

  it('should enable the login button if username is valid and password is given', () => {
    updateLoginForm('username', 'test@test.fr');
    updateLoginForm('password', 'testpwd');
    fixture.detectChanges();
    expect(loginPage.query<HTMLButtonElement>('.login-btn').disabled).toBe(
      false
    );
  });

  it('should dispatch an action to login when login button is clicked', () => {
    updateLoginForm('username', 'test@test.fr');
    updateLoginForm('password', 'testpwd');
    const loginAction = new fromSharedStore.DoLogin({
      username: 'test@test.fr',
      password: 'testpwd',
      rememberMe: false,
    });
    fixture.detectChanges();
    loginPage.query<HTMLButtonElement>('.login-btn').click();
    expect(sharedStore.dispatch).toHaveBeenCalledWith(loginAction);
  });
});
