import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import * as fromSharedStore from '../../../pickup-shared/store';
import { usernameValidator } from '../../../pickup-shared/validators/index';
import { BroadcasterService } from '../../../pickup-shared/services';
import { PickupEvent } from '../../../pickup-shared/model';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  hide: boolean;

  messages: PickupEvent[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private sharedStore: Store<fromSharedStore.AppSharedState>,
    private broadcastService: BroadcasterService
  ) {
    this.createLoginForm();
  }

  messages$ = this.broadcastService._eventBus
    .pipe(filter((event: PickupEvent) => event.type === 'error'))
    .subscribe(event => {
      this.messages.push(event);
    });

  createLoginForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', usernameValidator()],
      password: ['', Validators.required],
      rememberMe: false
    });
  }

  onSubmit() {
    this.sharedStore.dispatch(
      new fromSharedStore.DoLogin(this.loginForm.value)
    );
  }

  ngOnInit() {
    this.hide = true;
  }

  ngOnDestroy() {
    this.messages$.unsubscribe();
  }
}
