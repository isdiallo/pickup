import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromSharedStore from '../pickup-shared/store';
import { Router } from '@angular/router';
import { WindowRefService } from '../pickup-shared/services';

@Component({
  selector: 'app-pickup-info',
  templateUrl: './pickup-info.component.html',
  styleUrls: ['./pickup-info.component.scss'],
})
export class PickupInfoComponent implements OnInit {
  constructor(
    private sharedStore: Store<fromSharedStore.AppSharedState>,
    private route: Router,
    private windowRef: WindowRefService
  ) {}

  isAuthenticated$: Observable<boolean>;

  doLogin(event: any) {
    this.route.navigate(['/login']);
  }

  doLogout(event: any) {
    this.sharedStore.dispatch(new fromSharedStore.DoLogout());
    this.windowRef.getNativeWindow().location.reload();
  }

  ngOnInit() {
    this.isAuthenticated$ = this.sharedStore.select(
      fromSharedStore.isAuthenticated
    );
  }
}
