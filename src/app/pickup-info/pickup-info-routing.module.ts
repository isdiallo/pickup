import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PickupInfoComponent } from './pickup-info.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { ServicesComponent } from './components/services/services.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { TermsComponent } from './components/terms/terms.component';

const pickupInfoRoutes: Routes = [
  {
    path: '',
    component: PickupInfoComponent,
    children: [
      {
        path: '',
        component: HomeComponent,
        data: {
          name: 'home'
        }
      },
      {
        path: 'login',
        component: LoginComponent,
        data: {
          name: 'login'
        }
      },
      {
        path: 'register',
        component: RegisterComponent,
        data: {
          name: 'register'
        }
      },
      {
        path: 'contact-us',
        component: ContactUsComponent,
        data: {
          name: 'contact-us'
        }
      },
      {
        path: 'about-us',
        component: AboutUsComponent,
        data: {
          name: 'about-us'
        }
      },
      {
        path: 'services',
        component: ServicesComponent,
        data: {
          name: 'services'
        }
      },
      {
        path: 'privacy-policy',
        component: PrivacyPolicyComponent,
        data: {
          name: 'privacy-policy'
        }
      },
      {
        path: 'terms-conditions',
        component: TermsComponent,
        data: {
          name: 'terms-conditions'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(pickupInfoRoutes)],
  exports: [RouterModule]
})
export class PickupInfoRoutingModule {}
