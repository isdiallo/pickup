import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickupInfoComponent } from './pickup-info.component';
import { InfoNavbarComponent } from './components/info-navbar/navbar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FooterComponent } from '../pickup-shared/components/footer/footer.component';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';

describe('PickupInfoComponent', () => {
  let component: PickupInfoComponent;
  let fixture: ComponentFixture<PickupInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PickupInfoComponent, InfoNavbarComponent, FooterComponent],
      imports: [
        RouterTestingModule,
        MatIconModule,
        MatMenuModule,
        MatToolbarModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickupInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
