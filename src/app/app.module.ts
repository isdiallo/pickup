import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MediaMatcher } from '@angular/cdk/layout';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { StoreModule, MetaReducer } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  StoreRouterConnectingModule,
  RouterStateSerializer,
} from '@ngrx/router-store';

import { Ng2Webstorage } from 'ngx-webstorage';

import { reducers, effects, CustomSerializer } from './store';

// for dev env only!!!
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PickupInfoModule } from './pickup-info/pickup-info.module';
import { PickupSharedModule } from './pickup-shared/pickup-shared.module';

import { environment } from '../environments/environment';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    Ng2Webstorage.forRoot({ prefix: 'pickup-Auth', separator: '-' }),
    PickupInfoModule,
    AppRoutingModule,

    StoreModule.forRoot(reducers),
    EffectsModule.forRoot(effects),
    PickupSharedModule,
    StoreRouterConnectingModule.forRoot(),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  exports: [HttpClientModule, PickupSharedModule],
  providers: [
    MediaMatcher,
    { provide: RouterStateSerializer, useClass: CustomSerializer },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
